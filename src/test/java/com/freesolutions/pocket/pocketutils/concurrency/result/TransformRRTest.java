package com.freesolutions.pocket.pocketutils.concurrency.result;

import com.freesolutions.pocket.pocketutils.concurrency.processor.FixedAsyncProcessor;
import com.freesolutions.pocket.pocketutils.concurrency.queue.SyncableLinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class TransformRRTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransformRRTest.class);

    @Test
    public void test00() throws Exception {

        var asyncProcessor = new FixedAsyncProcessor<>("test", 1, new SyncableLinkedNLQueue<>());

        NonnullResult<String> source1 = NonnullFuture.of();
        NonnullResult<String> source2 = NonnullFuture.of();

        NonnullResult<String> result = NonnullFuture.<String>of().ofTransformR2R(asyncProcessor,
            source1,
            r1 -> {
                checkState(r1.status().present);
                LOGGER.info("r1: {}", r1.value());
                return NonnullFuture.<String>of().ofTransformR2R(asyncProcessor,
                    source2,
                    r2 -> {
                        checkState(r2.status().present);
                        LOGGER.info("r2: {}", r2.value());
                        return NonnullImmutableResult.ofPresent("output");
                    }
                );
            }
        );

        result.whenDone(asyncProcessor, () -> {
            checkState(result.status().present);
            LOGGER.info("result: {}", result.value());
        });

        asyncProcessor.start();

        ClockUtils.waitTimeout(5000L);
        source1.present("input1");
        source2.present("input2");
        ClockUtils.waitTimeout(5000L);

        asyncProcessor.stop();
    }
}
