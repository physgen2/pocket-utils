package com.freesolutions.pocket.pocketutils.concurrency.queue;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class LinkedNLQueueTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LinkedNLQueueTest.class);

    //    @Test
    public void test01() throws Exception {
        LinkedNLQueue<String> queue = new LinkedNLQueue<>();

        //submit
        final int outputThreadCount = 25;
        AtomicInteger[] outputValue = new AtomicInteger[outputThreadCount];
        AtomicInteger[] checkValue = new AtomicInteger[outputThreadCount];
        Thread[] outputThreads = new Thread[outputThreadCount];
        for (int i = 0; i < outputThreadCount; i++) {
            int outputThreadIdx = i;
            outputValue[i] = new AtomicInteger(0);
            checkValue[i] = new AtomicInteger(0);
            outputThreads[i] = new Thread(() -> {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        String item = itemValue(outputThreadIdx, outputValue[outputThreadIdx].incrementAndGet());
                        queue.add(item);
                        if (ThreadLocalRandom.current().nextDouble() < 0.8) {
                            ClockUtils.waitTimeout(1L);
                        }
                    } catch (InterruptedException ignored) {
                        break;
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                    }
                }
            });
        }
        Thread checkThread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {

                    //poll
                    String actualItem = queue.poll();
                    if (actualItem == null) {
                        LOGGER.info("poll returns null, wait some");
                        ClockUtils.waitTimeout(1L);
                        continue;
                    }

                    //check item
                    int actualThreadIdx = threadNumber(actualItem);
                    String expectedItem = itemValue(actualThreadIdx, checkValue[actualThreadIdx].incrementAndGet());
                    checkState(expectedItem.equals(actualItem));

                    //print queue
//                    LOGGER.info("item: {}, size: {}, collisions: {}",
//                            actualItem, queue.size(), queue.getCollisions());
                } catch (InterruptedException ignored) {
                    break;
                } catch (Throwable t) {
                    LOGGER.error("", t);
                }
            }
        });

        //start
        checkThread.start();
        for (Thread outputThread : outputThreads) {
            outputThread.start();
        }

        //wait
        Thread.sleep(5000);

        //interrupt
        for (Thread outputThread : outputThreads) {
            outputThread.interrupt();
        }
        checkThread.interrupt();

        //join
        for (Thread outputThread : outputThreads) {
            outputThread.join(1000);
        }
        checkThread.join(1000);

        LOGGER.info("size: {}, collisions: {}", queue.size(), queue.getCollisions());
    }

    private static String itemValue(int threadNumber, int valueNumber) {
        return threadNumber + "_" + valueNumber;
    }

    private static int threadNumber(String itemValue) {
        return Integer.parseInt(itemValue.split("_")[0]);
    }
}
