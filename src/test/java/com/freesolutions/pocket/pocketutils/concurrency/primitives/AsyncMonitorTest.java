package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import com.freesolutions.pocket.pocketutils.concurrency.processor.FixedAsyncProcessor;
import com.freesolutions.pocket.pocketutils.concurrency.queue.SyncableLinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class AsyncMonitorTest {

    @Test
    public void test00() throws Exception {

        List<Integer> inputs = new ArrayList<>();
        for (int i = 0; i < 1000L; i++) {
            inputs.add(i);
        }
        Collections.shuffle(inputs);

        AtomicReference<Integer> selection = new AtomicReference<>();
        AsyncMonitor asyncMonitor = new AsyncMonitor();

        List<Integer> outputs = new ArrayList<>();

        var asyncProcessor = new FixedAsyncProcessor<>("test", 4, new SyncableLinkedNLQueue<>());
        asyncProcessor.start();

        //setup waiters
        for (int i = 0; i < inputs.size(); i++) {
            int ii = i;
            asyncMonitor.fetchR(asyncProcessor, null, () -> {
                Integer s = selection.get();
                return (s != null && s == ii) ?
                    NonnullImmutableResult.ofPresent(ii) :
                    NonnullImmutableResult.ofPending();
            }).submitX(asyncProcessor, x -> {
                synchronized (outputs) {
                    outputs.add(x);
                    selection.set(null);
                    outputs.notifyAll();
                }
            });
        }

        //play
        for (int i : inputs) {
            selection.set(i);
            asyncMonitor.trigger();
            synchronized (outputs) {
                ClockUtils.waitTimeout(TimeUnit.SECONDS.toMillis(1L), outputs, ignored -> selection.get() == null);
            }
        }

        asyncProcessor.stop();

        assertEquals(inputs, outputs);
    }
}
