package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import com.freesolutions.pocket.pocketutils.concurrency.processor.ExplicitAsyncProcessor;
import com.freesolutions.pocket.pocketutils.concurrency.queue.LinkedActionQueue;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import org.junit.jupiter.api.Test;

import javax.annotation.ParametersAreNonnullByDefault;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertSame;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class GlobalTrackerTest {

    private static final Random RANDOM = new SecureRandom();

    @ParametersAreNonnullByDefault
    private static class QueryItem {
        public final long timeoutMs;

        public QueryItem(long timeoutMs) {
            this.timeoutMs = timeoutMs;
        }
    }

    @Test
    public void test00() throws Exception {
        for (int i = 0; i < 10; i++) {
            doTest00();
        }
    }

    private void doTest00() throws Exception {

        AtomicInteger queryItemsCount = new AtomicInteger(100);
        var asyncProcessor = new ExplicitAsyncProcessor<>(new LinkedActionQueue());

        //prepare unsorted query items
        long prevDeadlineMs = 10L;
        List<QueryItem> queryItems = new ArrayList<>();
        for (int i = 0; i < queryItemsCount.get(); i++) {
            prevDeadlineMs += (1L + RANDOM.nextInt(5));
            queryItems.add(new QueryItem(prevDeadlineMs));
        }

        //setup callbacks
        for (QueryItem queryItem : queryItems) {
            GlobalTracker.instance().query(Query.of(asyncProcessor, queryItem.timeoutMs, () -> {
                //don't use synchronization, due to usage of explicit async processor, which calls within test thread, ok
                queryItemsCount.decrementAndGet();
                QueryItem qi = queryItems.remove(0);
                assertSame(queryItem, qi);//callbacks must come in order of sorted query items array, ok
            }));
        }

        //sort
        queryItems.sort(Comparator.comparingLong(queryItem -> queryItem.timeoutMs));

        //process callbacks
        while (queryItemsCount.get() > 0) {
            asyncProcessor.sync(SyncQuery.timeout(TimeUnit.SECONDS.toMillis(5L))).mustOk();
        }
    }
}
