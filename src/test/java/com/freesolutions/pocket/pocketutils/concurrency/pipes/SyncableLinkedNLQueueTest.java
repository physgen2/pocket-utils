package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import com.freesolutions.pocket.pocketutils.concurrency.queue.SyncableLinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.queue.SyncableNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.threads.IntervalStrategy;
import com.freesolutions.pocket.pocketutils.concurrency.threads.LoopRunnable;
import com.freesolutions.pocket.pocketutils.concurrency.threads.ThreadBucket;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class SyncableLinkedNLQueueTest {

    private static final Random RANDOM = new Random();
    private static final String ALPHANUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    @Test
    public void testPoll_fromFilled() throws Exception {
        SyncableNLQueue<String> inQueue = new SyncableLinkedNLQueue<>();
        SyncableNLQueue<String> outQueue = new SyncableLinkedNLQueue<>();

        final int threadCount = 10;

        ThreadBucket threads = new ThreadBucket("test", threadCount);
        threads.submitAll(new LoopRunnable(
            IntervalStrategy.createPerturbated(5L, 2L),
            threads::isStopped,
            () -> {
                String value = inQueue.poll();
                if (value != null) {
                    outQueue.add(value);
                }
            }
        ));

        Set<String> inSet = new HashSet<>();
        for (int i = 0; i < 1000; i++) {
            inSet.add(randomAlphanumeric(10));
        }
        inSet.forEach(inQueue::add);

        threads.start();
        while (inQueue.peek() != null) {
            ClockUtils.waitTimeout(100L);
        }
        threads.stop();

        Set<String> outSet = new HashSet<>();
        outQueue.consumingIterable().forEach(outSet::add);

        assertEquals(inSet, outSet);
    }

    @Test
    public void testTake_oneValue() throws Exception {
        SyncableNLQueue<String> inQueue = new SyncableLinkedNLQueue<>();
        SyncableNLQueue<String> outQueue = new SyncableLinkedNLQueue<>();

        final long timeoutMs = 1000;
        final int threadCount = 10;
        String expectedValue = "123";

        AtomicInteger aliveCount = new AtomicInteger();
        ThreadBucket threads = new ThreadBucket("test", threadCount);
        threads.submitAll(() -> {
            try {
                String actualValue = inQueue.take(timeoutMs);
                if (actualValue != null) {
                    outQueue.add(actualValue);
                }
            } catch (Throwable t) {
                fail(t.getMessage());
            } finally {
                aliveCount.decrementAndGet();
            }
        });

        aliveCount.set(threadCount);
        threads.start();
        for (int i = 0; i < threadCount; i++) {
            inQueue.add(expectedValue);
        }
        while (aliveCount.get() > 0) {
            ClockUtils.waitTimeout(100L);
        }
        threads.stop();

        assertEquals(threadCount, outQueue.size());
        for (String actualValue : outQueue.consumingIterable()) {
            assertEquals(expectedValue, actualValue);
        }
    }

    @Test
    public void testTake_live() throws Exception {
        SyncableNLQueue<String> inQueue = new SyncableLinkedNLQueue<>();
        SyncableNLQueue<String> outQueue = new SyncableLinkedNLQueue<>();

        final int threadCount = 10;

        ThreadBucket threads = new ThreadBucket("test", threadCount);
        threads.submitAll(new LoopRunnable(
            IntervalStrategy.NO_WAIT,
            threads::isStopped,
            () -> outQueue.add(inQueue.take())
        ));

        Set<String> inSet = new HashSet<>();
        for (int i = 0; i < 100000; i++) {
            inSet.add(randomAlphanumeric(10));
        }

        threads.start();
        ClockUtils.waitTimeout(100L);
        inSet.forEach(inQueue::add);
        while (inQueue.peek() != null) {
            ClockUtils.waitTimeout(100L);
        }
        threads.stop();

        Set<String> outSet = new HashSet<>();
        outQueue.consumingIterable().forEach(outSet::add);

        assertEquals(inSet, outSet);
    }

    @Test
    public void testTake_cancel() throws Exception {
        SyncableNLQueue<String> queue = new SyncableLinkedNLQueue<>();

        final int threadCount = 10;

        AtomicInteger aliveCount = new AtomicInteger();
        ThreadBucket threads = new ThreadBucket("test", threadCount);
        threads.submitAll(() -> {
            try {
                queue.takeR().cancel();
            } finally {
                aliveCount.decrementAndGet();
            }
        });

        aliveCount.set(threadCount);
        threads.start();
        while (aliveCount.get() > 0) {
            ClockUtils.waitTimeout(100L);
        }
        threads.stop();

        String expected = "123";
        queue.add(expected);
        String actual = queue.take(100L);
        assertEquals(expected, actual);
    }

    private static String randomAlphanumeric(int n) {
        StringBuilder b = new StringBuilder(n);
        for (; n > 0; n--) {
            b.append(randomAphanumericChar());
        }
        return b.toString();
    }

    private static char randomAphanumericChar() {
        return ALPHANUMERIC.charAt(RANDOM.nextInt(ALPHANUMERIC.length()));
    }
}
