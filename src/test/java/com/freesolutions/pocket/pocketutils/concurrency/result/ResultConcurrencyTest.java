package com.freesolutions.pocket.pocketutils.concurrency.result;

import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.processor.FixedAsyncProcessor;
import com.freesolutions.pocket.pocketutils.concurrency.queue.LinkedActionQueue;
import com.freesolutions.pocket.pocketutils.concurrency.queue.SyncableLinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.*;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.threads.IntervalStrategy;
import com.freesolutions.pocket.pocketutils.concurrency.threads.LoopRunnable;
import com.freesolutions.pocket.pocketutils.concurrency.threads.ThreadBucket;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class ResultConcurrencyTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultConcurrencyTest.class);

    @ParametersAreNonnullByDefault
    private static class Item {

        //initial data
        public final NonnullFuture<Integer> priority = NonnullFuture.of();
        public final NonnullFuture<Item> link = NonnullFuture.of();

        //output
        public final NullableFuture<Void> done = NullableFuture.of();

        //computing data
        public final NonnullFuture<Item> ownerOfIsland = NonnullFuture.of();
        public final NonnullFuture<Integer> island = NonnullFuture.of();

        public Item(Pipe pipe, AtomicInteger numberOfIslands, ConcurrentMap<Integer, AtomicInteger> islandSizes) {
//            findOwnerOfIslandInChainFrom_01(pipe, link);
            findOwnerOfIslandInChainFrom_02(pipe, link);
            island.ofTransformX2R(pipe, ownerOfIsland, vOwnerOfIsland ->
                vOwnerOfIsland != this ?
                    vOwnerOfIsland.island :
                    NonnullImmutableResult.ofPresent(numberOfIslands.incrementAndGet())
            ).submitX(pipe, islandId -> {
                AtomicInteger h = islandSizes.get(islandId);
                if (h == null) {
                    h = new AtomicInteger(0);
                    AtomicInteger tmp = islandSizes.putIfAbsent(islandId, h);
                    if (tmp != null) {
                        h = tmp;
                    }
                }
                h.incrementAndGet();
                done.presentNull();
            });
        }

        private NonnullResult<Item> findOwnerOfIslandInChainFrom_01(Pipe pipe, NonnullFuture<Item> from) {
            return ownerOfIsland.ofTransformR2R(pipe, from, ignored1 ->
                from.value() == this ?
                    NonnullImmutableResult.ofPresent(this) :
                    NonnullFuture.<Item>of().ofTransformNR2R(pipe,
                        NullableFuture.of().ofCompletion(pipe, from.value().priority, priority),
                        ignored2 -> NonnullFuture.<Item>of().ofTransformR2R(pipe, from.value().priority, ignored3 ->
                            from.value().priority.value() > priority.value() ?
                                from.value().ownerOfIsland :
                                findOwnerOfIslandInChainFrom_01(pipe, from.value().link)
                        )
                    )
            );
        }

        private void findOwnerOfIslandInChainFrom_02(Pipe pipe, NonnullFuture<Item> from) {
            from.whenDone(pipe, () -> {
                if (from.value() == this) {
                    ownerOfIsland.present(this);
                } else {
                    NullableFuture.of().ofCompletion(pipe, from.value().priority, priority).whenDone(pipe, () ->
                        from.value().priority.whenDone(pipe, () -> {
                            if (from.value().priority.value() > priority.value()) {
                                from.value().ownerOfIsland.delegateR2R(pipe, ownerOfIsland);
                            } else {
                                findOwnerOfIslandInChainFrom_02(pipe, from.value().link);
                            }
                        })
                    );
                }
            });
        }
    }

    @Test
    public void test() throws Exception {
        main(new String[]{"useNativeQueue"});
        main(new String[0]);
    }

    public static void main(String[] args) throws Exception {

        LOGGER.info("------------------ concurrency islands test launch -------------------------");
        boolean useNativeQueue = args.length > 0 && args[0].equals("useNativeQueue");
        LOGGER.info("useNativeQueue: {}", useNativeQueue);

        AtomicInteger numberOfIslands = new AtomicInteger(0);
        ConcurrentMap<Integer, AtomicInteger> islandSizes = new ConcurrentHashMap<>();

        ThreadBucket statsLogger = new ThreadBucket("test-stats", 1);
        statsLogger.submit(new LoopRunnable(
            IntervalStrategy.createDefaultPerturbated(TimeUnit.SECONDS.toMillis(1L)),
            statsLogger::isStopped,
            () -> {
                MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
                LOGGER.info(
                    "Instant: {}, future counter: {}, " +
//                                    "dataPoolCounter: {}, " +
                        "immutableResult counter: {}" +
                        ", heap.max {}, heap.used: {}, heap.committed: {}, non_heap.used: {}" +
                        ", non_heap.max: {}, non_heap.committed: {}",
                    ClockUtils.UTC.instant(),
                    Future.GLOBAL_COUNTER.get(),
//                            Future.DATA_POOL.CELLS_USED.get(),
                    ImmutableResult.GLOBAL_COUNTER.get(),
                    memoryMXBean.getHeapMemoryUsage().getMax() / 0x100000,
                    memoryMXBean.getHeapMemoryUsage().getUsed() / 0x100000,
                    memoryMXBean.getHeapMemoryUsage().getCommitted() / 0x100000,
                    memoryMXBean.getNonHeapMemoryUsage().getUsed() / 0x100000,
                    memoryMXBean.getNonHeapMemoryUsage().getMax() / 0x100000,
                    memoryMXBean.getNonHeapMemoryUsage().getCommitted() / 0x100000
                );
            }
        ));
        statsLogger.start();

        var asyncProcessor = new FixedAsyncProcessor<>(
            "test-processor",
            20,
            useNativeQueue ?
                new LinkedActionQueue() :
                new SyncableLinkedNLQueue<>()
        );
        asyncProcessor.start();

        //create items
        final int numberOfItems = 200000;
        List<Item> items = new ArrayList<>(numberOfItems);
        for (int i = 0; i < numberOfItems; i++) {
            items.add(new Item(asyncProcessor, numberOfIslands, islandSizes));
        }

        //wait for 1 sec
        ClockUtils.waitTimeout(TimeUnit.SECONDS.toMillis(1L));

        //items bucket
        List<Item> bucket = new ArrayList<>(items);
        Collections.shuffle(bucket, new SecureRandom(Long.toString(ClockUtils.UTC.millis()).getBytes(StandardCharsets.UTF_8)));

        //fill with initial data
        for (int i = 0; i < numberOfItems; i++) {
            final int itemIndex = i;
            asyncProcessor.add(() -> {
                items.get(itemIndex).priority.present(itemIndex);
                items.get(itemIndex).link.present(bucket.get(itemIndex));
            });
        }

        //wait for items completed
        NullableFuture.of().ofCompletion(
            asyncProcessor,
            (Iterable<NullableFuture<Void>>) (() -> items.stream().map(item -> checkNotNull(item).done).iterator())
        ).sync(SyncQuery.infinite()).mustOk();

        asyncProcessor.stop();
        statsLogger.stop();

        LOGGER.info("numberOfIslands: {}", numberOfIslands);
        LOGGER.info("islandSizes: {}", islandSizes);
        LOGGER.info("avgIslandSize: {}", islandSizes.values().stream().collect(Collectors.averagingInt(AtomicInteger::get)));
        LOGGER.info("----------------------------------------------------------------------------");
    }
}
