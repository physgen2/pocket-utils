package com.freesolutions.pocket.pocketutils.data;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class CombinationIteratorTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CombinationIteratorTest.class);

    @ParametersAreNonnullByDefault
    private static class FiboIterator implements Iterator<Integer> {
        private final int count;
        private int i = 0;
        private int a = 1;
        private int b = 0;

        public FiboIterator(int count) {
            this.count = count;
        }

        @Override
        public boolean hasNext() {
            return i < count;
        }

        @Override
        public Integer next() {
            if (i >= count) {
                throw new NoSuchElementException();
            }
            int r = a + b;
            a = b;
            b = r;
            i++;
            return r;
        }
    }

    @Test
    public void test01() throws Exception {

        List<Supplier<Iterator<Integer>>> suppliers = List.of(
            () -> new FiboIterator(10),
            () -> new FiboIterator(10),
            () -> new FiboIterator(10)
        );

        //actual
        Iterator<String> i = new CombinationIterator<>(suppliers, String::valueOf);
        StringBuilder actualSB = new StringBuilder();
        while (i.hasNext()) {
            actualSB.append(i.next());
        }

        //expected
        StringBuilder expectedSB = new StringBuilder();
        for (int i2 : (Iterable<Integer>) suppliers.get(2)::get) {
            for (int i1 : (Iterable<Integer>) suppliers.get(1)::get) {
                for (int i0 : (Iterable<Integer>) suppliers.get(0)::get) {
                    expectedSB.append(String.valueOf(List.of(i0, i1, i2)));
                }
            }
        }

        //compare
        assertEquals(expectedSB.toString(), actualSB.toString());
    }

    @Test
    public void test02() throws Exception {

        List<Supplier<Iterator<Integer>>> suppliers = List.of(
            () -> IntStream.range(0, 10).iterator(),
            () -> IntStream.range(0, 10).iterator(),
            () -> IntStream.range(0, 10).iterator(),
            () -> IntStream.range(0, 10).iterator(),
            () -> IntStream.range(0, 10).iterator(),
            () -> IntStream.range(0, 10).iterator()
        );

        int happyCount = 0;
        for (boolean isHappy : (Iterable<Boolean>) () -> new CombinationIterator<>(suppliers, v ->
            v.get(0) + v.get(1) + v.get(2) == v.get(3) + v.get(4) + v.get(5)
        )) {
            if (isHappy) {
                happyCount++;
            }
        }
        LOGGER.info("happyCount: {}", happyCount);
        assertEquals(55252, happyCount);
    }
}
