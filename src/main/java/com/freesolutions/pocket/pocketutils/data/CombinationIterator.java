package com.freesolutions.pocket.pocketutils.data;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@NotThreadSafe
public class CombinationIterator<R, T> implements Iterator<R> {

    private final int count;
    @Nonnull
    private final Function<List<T>, R> combiner;
    @Nonnull
    private final List<Supplier<Iterator<T>>> suppliers;
    @Nonnull
    private final Iterator<T>[] iterators;
    @Nullable
    private T[] values;//initially null
    private int remaining;

    public CombinationIterator(@Nonnull List<Supplier<Iterator<T>>> suppliers, @Nonnull Function<List<T>, R> combiner) {
        final int count = suppliers.size();
        @SuppressWarnings("unchecked")
        Iterator<T>[] iterators = new Iterator[count];
        int remaining = count;
        for (int i = 0; i < count; i++) {
            iterators[i] = suppliers.get(i).get();
            if (!iterators[i].hasNext()) {
                remaining = 0;
            }
        }
        this.count = count;
        this.combiner = combiner;
        this.suppliers = suppliers;
        this.iterators = iterators;
        this.remaining = remaining;
    }

    @Override
    public boolean hasNext() {
        return remaining > 0;
    }

    @Override
    public R next() {
        if (remaining == 0) {
            throw new NoSuchElementException();
        }
        if (values == null) {//first call of next
            @SuppressWarnings("unchecked")
            T[] values = (T[]) new Object[count];
            for (int i = 0; i < count; i++) {
                checkState(iterators[i].hasNext());
                values[i] = iterators[i].next();
                if (!iterators[i].hasNext()) {
                    remaining--;
                }
            }
            this.values = values;
            return combine(values);
        }
        for (int i = 0; i < count; i++) {
            boolean flip = !iterators[i].hasNext();
            if (flip) {
                iterators[i] = suppliers.get(i).get();
                checkArgument(iterators[i].hasNext());
                remaining++;
            }
            values[i] = iterators[i].next();
            if (!iterators[i].hasNext()) {
                remaining--;
            }
            if (!flip) {
                return combine(values);
            }
        }
        checkState(remaining == 0);
        throw new NoSuchElementException();
    }

    private R combine(@Nonnull T[] values) {
        return combiner.apply(Collections.unmodifiableList(Arrays.asList(values)));
    }
}
