package com.freesolutions.pocket.pocketutils.data;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Stanislau Mirzayeu
 */
public class ArrayIterator<T> implements Iterator<T> {

    @Nonnull
    private final T[] array;

    private int i;

    public ArrayIterator(@Nonnull T[] array) {
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return i < array.length;
    }

    @Override
    public T next() {
        int i = this.i;
        if (i >= array.length) {
            throw new NoSuchElementException();
        }
        this.i = i + 1;
        return array[i];
    }
}
