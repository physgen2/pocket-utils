package com.freesolutions.pocket.pocketutils.data;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@NotThreadSafe
public class MergeIterator<T> implements Iterator<T> {

    private final int size;
    private final Iterator<?>[] inputs;
    private final Comparator<T> comparator;

    private final boolean[] completed;
    private final Object[] forwards;
    private int remain;

    @Nullable
    private Iterator<?> lastInput;

    public MergeIterator(
        @Nonnull Collection<Iterator<T>> inputs,
        @Nonnull Comparator<T> comparator
    ) {
        this.size = inputs.size();
        this.inputs = inputs.toArray(new Iterator[0]);
        this.comparator = comparator;
        this.completed = new boolean[size];//false by default
        this.forwards = new Object[size];

        //initialize
        int i = 0;
        for (Iterator<T> input : inputs) {
            if (input.hasNext()) {
                forwards[i] = input.next();
                remain++;
            }
            i++;
        }
    }

    @Override
    public boolean hasNext() {
        return remain > 0;
    }

    //NOTE: nullability unknown
    @SuppressWarnings("unchecked")
    @Override
    public T next() {

        //all completed ?
        if (remain == 0) {
            throw new NoSuchElementException();
        }

        //short case of last single iterator
        if (lastInput != null) {
            T result = (T) lastInput.next();//hasNext() was checked before
            if (!lastInput.hasNext()) {
                remain = 0;
            }
            return result;
        }

        //choose winner
        int winner = -1;
        T result = null;
        for (int i = 0; i < size; i++) {
            if (!completed[i]) {
                T candidate = (T) forwards[i];
                if (winner < 0 || comparator.compare(candidate, result) < 0) {
                    winner = i;
                    result = candidate;
                }
            }
        }
        checkState(winner >= 0);

        //recalculate forward for winner
        Iterator<?> wInput = inputs[winner];
        if (!wInput.hasNext()) {
            completed[winner] = true;
            remain--;
        } else if (remain > 1) {
            forwards[winner] = wInput.next();
        } else { //remain == 1
            this.lastInput = wInput;//switch to last-input mode
        }

        //result
        return result;
    }
}
