package com.freesolutions.pocket.pocketutils.actions;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.freesolutions.pocket.pocketutils.utils.Utils.interruptionAwarePropagate;

/**
 * @author Stanislau Mirzayeu
 */
public interface DataAction<T> extends NonnullDataAction<T> {

    DataAction<?> IDENTITY = ignored -> {
        //empty
    };

    @Nonnull
    @SuppressWarnings("unchecked")
    static <T> DataAction<T> identity() {
        return (DataAction<T>) IDENTITY;
    }

    void execute(@Nullable T value) throws Exception;

    static <T> void executeQuietly(@Nonnull DataAction<T> dataAction, @Nullable T value) {
        try {
            dataAction.execute(value);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        } catch (Throwable ignored) {
            //empty
        }
    }

    static <T> void executeSafe(@Nonnull DataAction<T> dataAction, @Nullable T value) {
        try {
            dataAction.execute(value);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        } catch (Throwable t) {
            LoggerFactory.getLogger(DataAction.class).error("", t);
        }
    }

    static <T> void executeUnchecked(@Nonnull DataAction<T> dataAction, @Nullable T value) {
        try {
            dataAction.execute(value);
        } catch (Throwable t) {
            throw interruptionAwarePropagate(t);
        }
    }

    static <T> void executeWithRetries(@Nonnull DataAction<T> dataAction, @Nullable T value, int attempts, long failureIntervalMs, boolean continueOnInterruption) throws Exception {
        boolean interrupted = continueOnInterruption && Thread.interrupted();
        try {
            int remaining = Math.max(0, attempts);
            while (true) {
                remaining = Math.max(-1, remaining - 1);
                try {
                    dataAction.execute(value);
                    return;
                } catch (Throwable t) {
                    if (t instanceof InterruptedException) {
                        //NOTE: assume that thread interruption flag is clear
                        if (!continueOnInterruption || remaining == 0) {
                            interrupted = false;
                            throw t;
                        }
                        interrupted = true;
                    }
                    if (remaining == 0) {
                        throw t; //not InterruptedException
                    }
                    LoggerFactory.getLogger(DataAction.class).error(
                        "Failed attempt to execute, ignore" +
                            ", remaining attempts: " + (remaining < 0 ? "infinity" : remaining) +
                            ", repeat in " + failureIntervalMs + " ms",
                        t
                    );
                    if (failureIntervalMs > 0L) {
                        long deadlineTs = ClockUtils.toDeadlineTs(failureIntervalMs, ClockUtils.UNI_CLOCK);
                        while (true) {
                            try {
                                ClockUtils.waitDeadline(deadlineTs, ClockUtils.UNI_CLOCK);
                                break;
                            } catch (InterruptedException ie) {
                                //NOTE: assume that thread interruption flag is clear
                                if (!continueOnInterruption) {
                                    //checkState(!interrupted);
                                    throw ie;
                                }
                                interrupted = true;
                            }
                        }
                    }
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
