package com.freesolutions.pocket.pocketutils.actions;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static com.freesolutions.pocket.pocketutils.utils.Utils.interruptionAwarePropagate;

/**
 * @author Stanislau Mirzayeu
 */
public interface Action {

    Action IDENTITY = () -> {
        //empty
    };

    @Nonnull
    static Action identity() {
        return IDENTITY;
    }

    void execute() throws Exception;

    static void executeQuietly(@Nonnull Action action) {
        try {
            action.execute();
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        } catch (Throwable ignored) {
            //empty
        }
    }

    static void executeSafe(@Nonnull Action action) {
        try {
            action.execute();
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        } catch (Throwable t) {
            LoggerFactory.getLogger(Action.class).error("", t);
        }
    }

    static void executeUnchecked(@Nonnull Action action) {
        try {
            action.execute();
        } catch (Throwable t) {
            throw interruptionAwarePropagate(t);
        }
    }

    static boolean executeOrRepeatOnInterruption(@Nonnull Action action) throws Exception {
        boolean interrupted = Thread.interrupted();
        while (true) {
            try {
                action.execute();
                break;
            } catch (InterruptedException ignored) {
                //NOTE: assume that thread interruption flag is clear
                interrupted = true;
            } catch (Throwable t) {
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
                throw t;
            }
        }
        return interrupted;
    }

    static void executeWithRetries(@Nonnull Action action, int attempts, long failureIntervalMs, boolean continueOnInterruption) throws Exception {
        boolean interrupted = continueOnInterruption && Thread.interrupted();
        try {
            int remaining = Math.max(0, attempts);
            while (true) {
                remaining = Math.max(-1, remaining - 1);
                try {
                    action.execute();
                    return;
                } catch (Throwable t) {
                    if (t instanceof InterruptedException) {
                        //NOTE: assume that thread interruption flag is clear
                        if (!continueOnInterruption || remaining == 0) {
                            interrupted = false;
                            throw t;
                        }
                        interrupted = true;
                    }
                    if (remaining == 0) {
                        throw t; //not InterruptedException
                    }
                    LoggerFactory.getLogger(Action.class).error(
                        "Failed attempt to execute, ignore" +
                            ", remaining attempts: " + (remaining < 0 ? "infinity" : remaining) +
                            ", repeat in " + failureIntervalMs + " ms",
                        t
                    );
                    if (failureIntervalMs > 0L) {
                        long deadlineTs = ClockUtils.toDeadlineTs(failureIntervalMs, ClockUtils.UNI_CLOCK);
                        while (true) {
                            try {
                                ClockUtils.waitDeadline(deadlineTs, ClockUtils.UNI_CLOCK);
                                break;
                            } catch (InterruptedException ie) {
                                //NOTE: assume that thread interruption flag is clear
                                if (!continueOnInterruption) {
                                    //checkState(!interrupted);
                                    throw ie;
                                }
                                interrupted = true;
                            }
                        }
                    }
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
