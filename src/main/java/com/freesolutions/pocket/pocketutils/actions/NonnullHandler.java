package com.freesolutions.pocket.pocketutils.actions;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static com.freesolutions.pocket.pocketutils.utils.Utils.interruptionAwarePropagate;

/**
 * @author Stanislau Mirzayeu
 */
public interface NonnullHandler<R> extends Handler<R> {

    @Nonnull
    static <R> NonnullHandler<R> toResult(@Nonnull R result) {
        return () -> result;
    }

    @Nonnull
    R handle() throws Exception;

    @Nonnull
    static <R> R executeQuietly(@Nonnull NonnullHandler<R> handler, @Nonnull R resultIfError) {
        try {
            return handler.handle();
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return resultIfError;
        } catch (Throwable ignored) {
            return resultIfError;
        }
    }

    @Nonnull
    static <R> R executeSafe(@Nonnull NonnullHandler<R> handler, @Nonnull R resultIfError) {
        try {
            return handler.handle();
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return resultIfError;
        } catch (Throwable t) {
            LoggerFactory.getLogger(NonnullHandler.class).error("", t);
            return resultIfError;
        }
    }

    @Nonnull
    static <R> R executeUnchecked(@Nonnull NonnullHandler<R> handler) {
        try {
            return handler.handle();
        } catch (Throwable t) {
            throw interruptionAwarePropagate(t);
        }
    }

    @Nonnull
    static <R> R executeWithRetries(@Nonnull NonnullHandler<R> handler, int attempts, long failureIntervalMs, boolean continueOnInterruption) throws Exception {
        boolean interrupted = continueOnInterruption && Thread.interrupted();
        try {
            int remaining = Math.max(0, attempts);
            while (true) {
                remaining = Math.max(-1, remaining - 1);
                try {
                    return handler.handle();
                } catch (Throwable t) {
                    if (t instanceof InterruptedException) {
                        //NOTE: assume that thread interruption flag is clear
                        if (!continueOnInterruption || remaining == 0) {
                            interrupted = false;
                            throw t;
                        }
                        interrupted = true;
                    }
                    if (remaining == 0) {
                        throw t; //not InterruptedException
                    }
                    LoggerFactory.getLogger(NonnullHandler.class).error(
                        "Failed attempt to execute, ignore" +
                            ", remaining attempts: " + (remaining < 0 ? "infinity" : remaining) +
                            ", repeat in " + failureIntervalMs + " ms",
                        t
                    );
                    if (failureIntervalMs > 0L) {
                        long deadlineTs = ClockUtils.toDeadlineTs(failureIntervalMs, ClockUtils.UNI_CLOCK);
                        while (true) {
                            try {
                                ClockUtils.waitDeadline(deadlineTs, ClockUtils.UNI_CLOCK);
                                break;
                            } catch (InterruptedException ie) {
                                //NOTE: assume that thread interruption flag is clear
                                if (!continueOnInterruption) {
                                    //checkState(!interrupted);
                                    throw ie;
                                }
                                interrupted = true;
                            }
                        }
                    }
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
