package com.freesolutions.pocket.pocketutils.actions;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.freesolutions.pocket.pocketutils.utils.Utils.interruptionAwarePropagate;

/**
 * @author Stanislau Mirzayeu
 */
public interface DataHandler<T, R> extends FromNonnullDataHandler<T, R> {

    DataHandler<?, ?> IDENTITY = ignored -> null;

    @Nonnull
    @SuppressWarnings("unchecked")
    static <T, R> DataHandler<T, R> identity() {
        return (DataHandler<T, R>) IDENTITY;
    }

    @Nonnull
    static <T, R> DataHandler<T, R> toResult(@Nullable R result) {
        return ignored -> result;
    }

    @Nullable
    R handle(@Nullable T value) throws Exception;

    @Nullable
    static <T, R> R executeQuietly(@Nonnull DataHandler<T, R> dataHandler, @Nullable T value, @Nullable R resultIfError) {
        try {
            return dataHandler.handle(value);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return resultIfError;
        } catch (Throwable ignored) {
            return resultIfError;
        }
    }

    @Nullable
    static <T, R> R executeSafe(@Nonnull DataHandler<T, R> dataHandler, @Nullable T value, @Nullable R resultIfError) {
        try {
            return dataHandler.handle(value);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return resultIfError;
        } catch (Throwable t) {
            LoggerFactory.getLogger(DataHandler.class).error("", t);
            return resultIfError;
        }
    }

    @Nullable
    static <T, R> R executeUnchecked(@Nonnull DataHandler<T, R> dataHandler, @Nullable T value) {
        try {
            return dataHandler.handle(value);
        } catch (Throwable t) {
            throw interruptionAwarePropagate(t);
        }
    }

    @Nullable
    static <T, R> R executeWithRetries(@Nonnull DataHandler<T, R> dataHandler, @Nullable T value, int attempts, long failureIntervalMs, boolean continueOnInterruption) throws Exception {
        boolean interrupted = continueOnInterruption && Thread.interrupted();
        try {
            int remaining = Math.max(0, attempts);
            while (true) {
                remaining = Math.max(-1, remaining - 1);
                try {
                    return dataHandler.handle(value);
                } catch (Throwable t) {
                    if (t instanceof InterruptedException) {
                        //NOTE: assume that thread interruption flag is clear
                        if (!continueOnInterruption || remaining == 0) {
                            interrupted = false;
                            throw t;
                        }
                        interrupted = true;
                    }
                    if (remaining == 0) {
                        throw t; //not InterruptedException
                    }
                    LoggerFactory.getLogger(DataHandler.class).error(
                        "Failed attempt to execute, ignore" +
                            ", remaining attempts: " + (remaining < 0 ? "infinity" : remaining) +
                            ", repeat in " + failureIntervalMs + " ms",
                        t
                    );
                    if (failureIntervalMs > 0L) {
                        long deadlineTs = ClockUtils.toDeadlineTs(failureIntervalMs, ClockUtils.UNI_CLOCK);
                        while (true) {
                            try {
                                ClockUtils.waitDeadline(deadlineTs, ClockUtils.UNI_CLOCK);
                                break;
                            } catch (InterruptedException ie) {
                                //NOTE: assume that thread interruption flag is clear
                                if (!continueOnInterruption) {
                                    //checkState(!interrupted);
                                    throw ie;
                                }
                                interrupted = true;
                            }
                        }
                    }
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
