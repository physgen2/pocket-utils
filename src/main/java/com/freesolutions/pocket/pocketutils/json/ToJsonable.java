package com.freesolutions.pocket.pocketutils.json;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * @author Stanislau Mirzayeu
 */
public interface ToJsonable {

    void toJson(@Nonnull Writer writer) throws IOException;

    @Nonnull
    default String toJson() {
        StringWriter w = new StringWriter();
        try {
            toJson(w);
        } catch (Throwable t) {
            throw new IllegalStateException("must never be thrown", t);
        }
        return w.toString();
    }
}
