package com.freesolutions.pocket.pocketutils.json;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * @author Stanislau Mirzayeu
 */
public interface FromJsonable {

    void fromJson(@Nonnull Reader reader) throws IOException;

    default void fromJson(@Nonnull String s) throws IOException {
        fromJson(new StringReader(s));
    }
}
