package com.freesolutions.pocket.pocketutils.opresult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class NonnullOpResult<V, E> extends NullableOpResult<V, E> {

    @SuppressWarnings("unchecked")
    @Nullable
    public static <V, E> NonnullOpResult<V, E> ofOkIfOk(@Nonnull NonnullOpResult<V, ?> source) {
        return source.isOk() ? (NonnullOpResult<V, E>) source : null;//direct cast due to error is null
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static <V, E> NonnullOpResult<V, E> ofNotOkIfNotOk(@Nonnull NullableOpResult<?, E> source) {
        boolean ok = source.isOk();
        if (ok) {
            return null;
        }
        if (source instanceof NonnullOpResult) {
            return (NonnullOpResult<V, E>) source;//direct cast due to value is null
        }
        return new NonnullOpResult<>(null, source.error());
    }

    @Nonnull
    public static <V, E> NonnullOpResult<V, E> ofOk(@Nonnull V value) {
        return new NonnullOpResult<>(value, null);
    }

    @Nonnull
    public static <V, E> NonnullOpResult<V, E> ofNotOk(@Nonnull E error) {
        return new NonnullOpResult<>(null, checkNotNull(error));
    }

    @Override
    public boolean isOk() {
        return super.isOk();
    }

    @Nonnull
    @Override
    public V value() {
        return checkNotNull(super.value());
    }

    @Nonnull
    @Override
    public E error() {
        return super.error();
    }

    @Nonnull
    @Override
    public NonnullOpResult<V, E> mustOk() throws IllegalStateException {
        return (NonnullOpResult<V, E>) super.mustOk();
    }

    @Nonnull
    @Override
    public NonnullOpResult<V, E> mustNotOk() throws IllegalStateException {
        return (NonnullOpResult<V, E>) super.mustNotOk();
    }

    protected NonnullOpResult(@Nullable V value, @Nullable E error) {
        super(value, error);
        checkArgument(error != null || value != null);
    }
}
