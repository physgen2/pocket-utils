package com.freesolutions.pocket.pocketutils.opresult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability of value unknown, see implementations, ok
 */
@Immutable
public interface OpResult<V, E> {

    boolean isOk();

    V value();

    @Nonnull
    E error();

    @Nullable
    default V valueOrNull() {
        return isOk() ? value() : null;
    }

    @Nullable
    default E errorOrNull() {
        return isOk() ? null : error();
    }

    //nullability unknown, calling code can perform checks according to it logic if necessary
    default V orElse(V defaultValue) {
        return isOk() ? value() : defaultValue;
    }

    @Nonnull
    OpResult<V, E> mustOk() throws IllegalStateException;

    @Nonnull
    OpResult<V, E> mustNotOk() throws IllegalStateException;
}
