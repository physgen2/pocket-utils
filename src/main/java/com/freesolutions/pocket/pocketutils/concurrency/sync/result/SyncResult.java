package com.freesolutions.pocket.pocketutils.concurrency.sync.result;

import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncStatus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability unknown, see implementations, ok
 */
@Immutable
public interface SyncResult<T> {

    @Nonnull
    SyncStatus status();

    T value();

    @Nullable
    default T valueOrNull() {
        return status().ok ? value() : null;
    }

    //nullability unknown, calling code can perform checks according to it logic if necessary, ok
    default T orElse(T defaultValue) {
        return status().ok ? value() : defaultValue;
    }

    @Nonnull
    SyncResult<T> notRejected() throws RejectedExecutionException;

    @Nonnull
    SyncResult<T> notInterrupted() throws InterruptedException;

    @Nonnull
    SyncResult<T> noProblems() throws RejectedExecutionException, InterruptedException;

    @Nonnull
    SyncResult<T> notTimedOut() throws TimeoutException;

    @Nonnull
    SyncResult<T> mustOk() throws RejectedExecutionException, InterruptedException, TimeoutException;
}
