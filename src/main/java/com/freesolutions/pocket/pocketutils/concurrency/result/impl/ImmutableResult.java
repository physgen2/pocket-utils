package com.freesolutions.pocket.pocketutils.concurrency.result.impl;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.WritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NullableSyncResult;
import com.freesolutions.pocket.pocketutils.actions.Action;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.atomic.AtomicLong;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability unknown, see derived classes, ok
 */
@Immutable
public abstract class ImmutableResult<T> implements WritableResult<T> {

    public static final AtomicLong GLOBAL_COUNTER = new AtomicLong(0L);

    private final Status status;
    private final Object value;

    @Nonnull
    @Override
    public NullableSyncResult<Void> sync(@Nonnull SyncQuery ignored) {
        return status().done ? NullableSyncResult.ofOkNull() : NullableSyncResult.ofRejected();
    }

    @Nonnull
    @Override
    public Status status() {
        return status;
    }

    @Override
    public T value() {
        checkState(status.present, "not present");
        @SuppressWarnings("unchecked")
        T result = (T) value;
        return result;
    }

    @Nullable
    @Override
    public Throwable error() {
        return status.error ? (Throwable) value : null;
    }

    @Override
    public void whenDone(@Nonnull Pipe pipe, @Nonnull Action action) {
        if (status.done) {
            pipe.add(action);
        }
        //NOTE: otherwise done will never occur due to result is immutable, so ignore
    }

    @Override
    public void purgeCallbacks() {
        //no callbacks, do nothing
    }

    @Override
    public boolean present(T value) {
        return false;//immutable, ok
    }

    @Override
    public boolean error(@Nonnull Throwable t) {
        return false;//immutable, ok
    }

    @Override
    public boolean cancel() {
        return false;//immutable, ok
    }

    @Override
    public boolean timeout() {
        return false;//immutable, ok
    }

    @Override
    public boolean interrupt() {
        return false;//immutable, ok
    }

    protected ImmutableResult(@Nonnull Status status, Object value) {
        switch (status) {
            case PENDING:
                value = null;
                break;
            case CANCELLED:
            case TIMED_OUT:
            case INTERRUPTED:
                value = null;
                break;
            case ERROR:
                checkArgument(value instanceof Throwable);
                break;
            case PRESENT:
                //nothing
                break;
            default:
                throw new IllegalArgumentException("Unknown status: " + status);
        }
        this.status = status;
        this.value = value;
        GLOBAL_COUNTER.incrementAndGet();
    }

    @Override
    protected void finalize() {
        GLOBAL_COUNTER.decrementAndGet();
    }
}
