package com.freesolutions.pocket.pocketutils.concurrency.logic.iface;

import com.freesolutions.pocket.pocketlifecycle.startable.AutoStartable;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.HasLifecycleState;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Logic<CONTEXT, CONTROL> extends AutoStartable, HasLifecycleState {

    @Nonnull
    CONTEXT context();

    @Nonnull
    CONTROL control();
}
