package com.freesolutions.pocket.pocketutils.concurrency.feed.iface;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
@FunctionalInterface
public interface ListenerAction<G, P, L> {

    void execute(@Nonnull G group, @Nonnull P context, @Nonnull L listener) throws Exception;
}
