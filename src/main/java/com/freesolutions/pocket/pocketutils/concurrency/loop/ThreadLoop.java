package com.freesolutions.pocket.pocketutils.concurrency.loop;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Optional;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class ThreadLoop implements Loop {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadLoop.class);

    @Nonnull
    private final String name;
    @Nonnull
    private final LoopConfig config;

    //NOTE: all mutable state is guarded by this

    //these values are controlled by caller and can't be reset
    @Nullable
    private volatile NonnullDataAction<ThreadLoop> finishHook;

    //these values can be reset
    @Nullable
    private volatile Thread thread;
    @Nullable
    private Optional<Throwable> syncStartSignal;
    private volatile boolean invokeRequested;
    private volatile boolean skipNearestInvoke;
    private volatile boolean stopRequested;

    public ThreadLoop(@Nonnull String name, @Nonnull LoopConfig config) {
        this.name = name;
        this.config = config;
        reset();
    }

    @Nonnull
    public synchronized ThreadLoop finishHook(@Nullable NonnullDataAction<ThreadLoop> finishHook) {
        checkState(thread == null, "allowed to setup finish hook only at non-started state");
        this.finishHook = finishHook;
        return this;
    }

    @Nonnull
    @Override
    public synchronized String name() {
        return name;
    }

    @Nonnull
    @Override
    public LoopConfig config() {
        return config;
    }

    @Override
    public boolean started() {
        return thread != null;//volatile
    }

    @Override
    public synchronized void start() throws Exception {

        //checks
        if (thread != null) {
            return;
        }

        //create & launch thread
        Thread thread;
        try {
            thread = new Thread(
                () -> {
                    Action.executeSafe(this::doRun);
                    NonnullDataAction<ThreadLoop> fh = finishHook;//volatile
                    if (fh != null) {
                        Thread.interrupted();//clear possible interruption
                        NonnullDataAction.executeSafe(fh, ThreadLoop.this);
                    }
                },
                name
            );
            thread.start();
        } catch (Throwable t) {
            reset();
            throw new IllegalStateException("failed to start thread loop \"" + name + "\"", t);
        }

        //sync start
        boolean interrupted = false;
        if (config.syncStart) {
            interrupted = Action.executeOrRepeatOnInterruption(() ->
                ClockUtils.waitDeadline(
                    null,
                    ClockUtils.UNI_CLOCK,
                    ThreadLoop.this, //monitor
                    ignored -> syncStartSignal != null
                )
            );
            Throwable exc = syncStartSignal != null ? syncStartSignal.orElse(null) : null;
            if (exc != null) {
                //NOTE: no need to finish thread here, because thread finishes itself in case of start action failure
                reset();
                if (interrupted) { //don't remember about delayed interruption
                    Thread.currentThread().interrupt();
                }
                throw new IllegalStateException("failed to make sync start of thread loop \"" + name + "\"", exc);
            }
        }

        //assign thread => mark started
        this.thread = thread;

        //throw delayed interruption if necessary
        if (interrupted) {
            throw new InterruptedException();
        }
    }

    @Override
    public void stop() {
        //NOTE: stop method must work without exceptions

        Thread thread;
        synchronized (this) {

            //checks
            thread = this.thread;
            if (thread == null) {
                return;
            }

            //launch stopping procedure
            this.stopRequested = true;
            this.notifyAll();

            //interrupt
            try {
                thread.interrupt();
            } catch (Throwable t) {
                LOGGER.error("", t);
            }
        }

        //NOTE: out of sync

        //join
        try {
            if (Action.executeOrRepeatOnInterruption(thread::join)) {
                Thread.currentThread().interrupt();
            }
        } catch (Throwable t) {
            LOGGER.error("", t);
        }

        //final reset
        reset();
    }

    @Override
    public synchronized void invoke() {
        if (thread == null) {
            return;
        }
        this.invokeRequested = true;
        this.notifyAll();
    }

    @Override
    public synchronized void skipNearestInvoke() {
        if (thread == null) {
            return;
        }
        this.skipNearestInvoke = true;
    }

    private synchronized void reset() {
        this.thread = null;
        this.syncStartSignal = null;
        this.invokeRequested = false;
        this.skipNearestInvoke = false;
        this.stopRequested = false;
    }

    private void doRun() {

        //start action
        Throwable ex = null;
        if (config.startAction != null) {
            try {
                config.startAction.execute();
            } catch (Throwable t) {
                ex = t;
            }
        }

        //handle result of start action
        if (config.syncStart) {
            //send signal about start
            synchronized (this) {
                this.syncStartSignal = Optional.ofNullable(ex);
                this.notifyAll();
            }
        } else {
            //simply log start error if present
            if (ex != null) {
                LOGGER.error("", ex);
            }
        }
        if (ex != null) {
            //finish thread because of start failure
            return;
        }

        //loop
        try {
            doLoop();
        } catch (Throwable t) {
            LOGGER.error("", t);
        }

        //wait for stop request
        synchronized (this) {
            while (true) {
                Thread.interrupted();//clear possible interruption
                try {
                    ClockUtils.waitDeadline(
                        null,
                        ClockUtils.UNI_CLOCK,
                        ThreadLoop.this, //monitor
                        ignored -> stopRequested
                    );
                    break;
                } catch (InterruptedException ignored) {
                    //NOTE: ignore interrupts, we know that waiting will be finished
                }
            }
        }

        //stop action
        if (config.stopAction != null) {
            Thread.interrupted();//clear possible interruption
            Action.executeSafe(config.stopAction);
        }
    }

    private void doLoop() {
        boolean initial = true;
        boolean error = false;
        Long invokeTs = ClockUtils.UNI_CLOCK.millis();
        while (
            (!Thread.interrupted() || config.ignoreInterruption) //always read interruption flag to reset it
                && !stopRequested
                && !(config.stopCondition != null && NonnullHandler.executeSafe(() -> config.stopCondition.test(null), false))
        ) {
            //calculate next invokeTs
            invokeTs = config.nextInvokeTs(checkNotNull(invokeTs), initial, error);

            //wait
            if (!waitForInvoke(invokeTs)) {
                break;//exit loop
            }

            //invoke
            invokeTs = ClockUtils.UNI_CLOCK.millis();
            try {
                if (!error || !config.noRepeatOnError) {
                    this.invokeRequested = false;
                    if (!skipNearestInvoke && (!initial || !config.skipInitialInvoke)) {
                        if (config.loopAction != null) {
                            config.loopAction.execute();
                        } else {
                            checkNotNull(config.loopActionWithLoopArg).execute(ThreadLoop.this);
                        }
                    }
                    this.skipNearestInvoke = false;
                }
                initial = false;
                error = false;
            } catch (Throwable t) {
                if (t instanceof InterruptedException && !config.ignoreInterruption) {
                    break;
                }
                error = true;
                LOGGER.error("exception in loop action of \"" + name + "\", ignore", t);
            }
        }
    }

    private boolean waitForInvoke(@Nullable Long invokeTs) {
        while (true) {
            try {
                synchronized (this) {
                    ClockUtils.waitDeadline(
                        invokeTs,
                        ClockUtils.UNI_CLOCK,
                        ThreadLoop.this, //monitor
                        ignored -> invokeRequested || stopRequested //both volatile
                    );
                }
                //exit because stop was requested during waiting
                return !stopRequested;
            } catch (InterruptedException ignored) {
                if (stopRequested || !config.ignoreInterruption) {
                    //exit because interruption happened during waiting
                    return false;
                }
            }
        }
    }
}
