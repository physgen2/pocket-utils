package com.freesolutions.pocket.pocketutils.concurrency.queue;

import com.freesolutions.pocket.pocketutils.concurrency.primitives.Vars;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.lang.invoke.VarHandle;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class LinkedNLQueue<T> implements NLQueue<T> {

    private static final VarHandle HEAD_VH = Vars.getVarHandle(LinkedNLQueue.class, "head", Node.class);
    private static final VarHandle TAIL_VH = Vars.getVarHandle(LinkedNLQueue.class, "tail", Node.class);
    private static final VarHandle SIZE_VH = Vars.getVarHandle(LinkedNLQueue.class, "size", int.class);
    private static final VarHandle COLLISIONS_VH = Vars.getVarHandle(LinkedNLQueue.class, "collisions", int.class);

    @Nonnull
    private volatile Node<T> head = Node.initial();
    @Nonnull
    private volatile Node<T> tail = head;//not included in queue data
    private volatile int size;//see vh-access in code, ok
    private volatile int collisions;//see vh-access in code, ok

    public LinkedNLQueue() {
        //empty
    }

    public int getCollisions() {
        return collisions;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == tail;//comparison by reference, ok
    }

    @Override
    public void add(@Nonnull T value) {
        checkNotNull(value);
        incrementInt(SIZE_VH);

        Node<T> nextTail = Node.initial();
        Node<T> tail = this.tail;
        while (!tail.casNext(NULL(), nextTail)) {
            tail = tail.next;
            if (tail == null) {//check if was disposed
                tail = this.tail;
            }
            incrementInt(COLLISIONS_VH);
        }

        //activate node with value
        tail.value = value;

        //commit tail
        while (cas(TAIL_VH, tail, nextTail)) {
            if (nextTail.value == null) {
                //no more data exist
                break;
            }
            tail = nextTail;
            nextTail = tail.next;
            if (nextTail == null || nextTail == NULL()) {//if disposed or NULL node
                break;
            }
            incrementInt(COLLISIONS_VH);
        }
    }

    @Nullable
    @Override
    public T poll() {

        //detach head node
        Node<T> head;
        while (true) {
            head = this.head;
            if (head == tail) {//if empty queue
                return null;
            }
            if (cas(HEAD_VH, head, head.next)) {//next node must be correct if cas succeeds
                break;
            }
            incrementInt(COLLISIONS_VH);
        }

        //detach node & help gc
        head.next = null;

        decrementInt(SIZE_VH);
        return checkNotNull(head.value);//value must be present
    }

    @Nullable
    @Override
    public T peek() {
        Node<T> head = this.head;
        return head != tail ? head.value : null;
    }

    @Override
    public void clear() {

        //detach all available nodes
        Node<T> head;
        Node<T> tail;
        while (true) {
            head = this.head;
            tail = this.tail;
            if (head == tail) {//if empty queue
                return;
            }
            if (cas(HEAD_VH, head, tail)) {
                break;
            }
            incrementInt(COLLISIONS_VH);
        }

        //detach nodes & help gc
        int count = 0;
        while (head != tail) {
            Node<T> detachable = head;
            head = checkNotNull(detachable.next);
            detachable.next = null;
            count--;
        }

        addLong(SIZE_VH, count);
    }

    @Nonnull
    @Override
    public Iterator<T> iterator() {
        //NOTE: tail read after head, so even if tail updated after head read,
        //then all interval remains correct.
        return new Iter<>(head, tail);
    }

    private <V> boolean cas(@Nonnull VarHandle vh, @Nullable V expect, @Nullable V update) {
        return vh.compareAndSet(this, expect, update);
    }

    private void incrementInt(@Nonnull VarHandle vh) {
        vh.getAndAdd(this, 1);
    }

    private void decrementInt(@Nonnull VarHandle vh) {
        vh.getAndAdd(this, -1);
    }

    private void addLong(@Nonnull VarHandle vh, long delta) {
        vh.getAndAdd(this, delta);
    }

    @SuppressWarnings("unchecked")
    private static <T> Node<T> NULL() {
        return (Node<T>) Node.NULL;
    }

    private static class Node<T> {

        private static final VarHandle NEXT_VH = Vars.getVarHandle(Node.class, "next", Node.class);

        private static final Node<?> NULL = new Node<>(null, null);

        @Nullable
        public volatile Node<T> next;
        @Nullable
        public volatile T value;

        @SuppressWarnings("unchecked")
        public static <T> Node<T> initial() {
            return new Node<>(null, (Node<T>) NULL);
        }

        public boolean casNext(@Nullable Node<T> expect, @Nullable Node<T> update) {
            return NEXT_VH.compareAndSet(this, expect, update);
        }

        private Node(@Nullable T value, @Nullable Node<T> next) {
            this.value = value;
            this.next = next;
        }
    }

    private static class Iter<T> implements Iterator<T> {

        private static final VarHandle CURRENT_VH = Vars.getVarHandle(Iter.class, "current", Node.class);

        @Nullable
        private volatile Node<T> current;//null if node was removed concurrently, treat as end of iteration
        @Nonnull
        private final Node<T> end;

        public Iter(@Nonnull Node<T> current, @Nonnull Node<T> end) {
            this.current = current;
            this.end = end;
        }

        @Override
        public boolean hasNext() {
            Node<T> current = this.current;
            return current != null && current != end;
        }

        @Nonnull
        @Override
        public T next() {
            while (true) {
                Node<T> current = this.current;
                if (current == null || current == end) {
                    throw new NoSuchElementException();
                }
                Node<T> next = current.next;
                if (casCurrent(current, next)) {
                    return checkNotNull(current.value);
                }
            }
        }

        private boolean casCurrent(@Nullable Node<T> expect, @Nullable Node<T> update) {
            return CURRENT_VH.compareAndSet(this, expect, update);
        }
    }
}
