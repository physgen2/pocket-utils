package com.freesolutions.pocket.pocketutils.concurrency.result.impl;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class NonnullFuture<T> extends Future<T> implements NonnullResult<T> {

    public static <T> NonnullFuture<T> of() {
        return new NonnullFuture<>();
    }

    @Nonnull
    public T value() {
        return super.value();//delegate to super, just for nullability marker
    }

    @Nonnull
    public T syncedValue() throws Exception {
        return super.syncedValue();//delegate to super, just for nullability marker
    }

    public boolean present(@Nonnull T value) {
        return super.present(value);//delegate to super, just for nullability marker
    }

    @Override
    public boolean assignFrom(@Nonnull NonnullReadableResult<T> source) {
        Status status = source.status();
        return state(status, status.present ? source.value() : status.error ? source.error() : null);
    }

    @Override
    public boolean assignFromIfPresent(@Nonnull NonnullReadableResult<T> source) {
        Status status = source.status();
        return status.present && state(status, source.value());
    }

    @Override
    public boolean assignFromIfNotPresent(@Nonnull NullableReadableResult<?> source) {//source is nullable, ok
        Status status = source.status();
        return status.done && !status.present && state(status, status.error ? source.error() : null);
    }

    protected NonnullFuture() {
        super();
    }
}
