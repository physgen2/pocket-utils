package com.freesolutions.pocket.pocketutils.concurrency.topic.iface;

import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Addable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.function.Predicate;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Topic<T> extends Addable<T> {

    @Nonnull
    ManualConsumer<T> manualConsumer(@Nonnull Predicate<T> filter);

    @Nonnull
    ListenableConsumer<T> listenableConsumer(@Nonnull Predicate<T> filter, @Nonnull Addable<T> output);

    int size();
}
