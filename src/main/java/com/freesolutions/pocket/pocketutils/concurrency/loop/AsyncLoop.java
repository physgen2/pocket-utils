package com.freesolutions.pocket.pocketutils.concurrency.loop;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Cancellable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.concurrency.timing.Monitor;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Tracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Optional;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class AsyncLoop implements Loop {
    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncLoop.class);

    @Nonnull
    private final Pipe pipe;
    @Nonnull
    private final String name;
    @Nonnull
    private final LoopConfig config;

    //NOTE: all mutable state is guarded by this

    //these values are controlled by caller and can't be reset
    @Nullable
    private volatile Tracker tracker;
    @Nullable
    private volatile NonnullDataAction<AsyncLoop> finishHook;

    //these values can be reset
    @Nullable
    private volatile Monitor.Async monitor;
    @Nullable
    private Optional<Throwable> syncStartSignal;
    private volatile boolean invokeRequested;
    private volatile boolean skipNearestInvoke;
    private volatile boolean stopRequested;

    public AsyncLoop(@Nonnull Pipe pipe, @Nonnull String name, @Nonnull LoopConfig config) {
        this.pipe = pipe;
        this.name = name;
        this.config = config;
        reset();
    }

    @Nonnull
    public synchronized AsyncLoop tracker(@Nullable Tracker tracker) {
        checkState(monitor == null, "allowed to setup tracker only at non-started state");
        this.tracker = tracker;
        return this;
    }

    @Nonnull
    public synchronized AsyncLoop finishHook(@Nullable NonnullDataAction<AsyncLoop> finishHook) {
        checkState(monitor == null, "allowed to setup finish hook only at non-started state");
        this.finishHook = finishHook;
        return this;
    }

    @Nonnull
    public Pipe pipe() {
        return pipe;
    }

    @Nonnull
    @Override
    public String name() {
        return name;
    }

    @Nonnull
    @Override
    public LoopConfig config() {
        return config;
    }

    @Override
    public boolean started() {
        return monitor != null;//volatile
    }

    @Override
    public synchronized void start() throws Exception {

        //checks
        if (monitor != null) {
            return;
        }

        //create monitor & launch
        Monitor.Async monitor = new Monitor.Async();
        Monitor.Async.immediateLaunch(pipe, monitor, () -> doStartAction(monitor));

        //sync start
        boolean interrupted = false;
        if (config.syncStart) {
            interrupted = Action.executeOrRepeatOnInterruption(() ->
                ClockUtils.waitDeadline(
                    null,
                    ClockUtils.UNI_CLOCK,
                    AsyncLoop.this, //monitor
                    ignored -> syncStartSignal != null
                )
            );
            Throwable exc = syncStartSignal != null ? syncStartSignal.orElse(null) : null;
            if (exc != null) {
                //NOTE: no need to cancel monitor here, because activity finishes itself in case of start action failure
                reset();
                if (interrupted) { //don't remember about delayed interruption
                    Thread.currentThread().interrupt();
                }
                throw new IllegalStateException("failed to make sync start of async loop \"" + name + "\"", exc);
            }
        }

        //assign monitor => mark started
        this.monitor = monitor;

        //throw delayed interruption if necessary
        if (interrupted) {
            throw new InterruptedException();
        }
    }

    @Override
    public void stop() {
        //NOTE: stop method must work without exceptions

        Monitor.Async monitor;
        synchronized (this) {

            //checks
            monitor = this.monitor;
            if (monitor == null) {
                return;
            }

            //launch stopping procedure
            this.stopRequested = true;
            monitor.trigger();
        }

        //NOTE: out of sync

        //join monitor, that will be cancelled at the end
        try {
            Cancellable.joinUninterruptibly(monitor);
        } catch (Throwable t) {
            LOGGER.error("", t);
        }

        //final reset
        reset();
    }

    @Override
    public synchronized void invoke() {
        var monitor = this.monitor;
        if (monitor == null) {
            return;
        }
        this.invokeRequested = true;
        monitor.trigger();
    }

    @Override
    public synchronized void skipNearestInvoke() {
        if (monitor == null) {
            return;
        }
        this.skipNearestInvoke = true;
    }

    private synchronized void reset() {
        this.monitor = null;
        this.syncStartSignal = null;
        this.invokeRequested = false;
        this.skipNearestInvoke = false;
        this.stopRequested = false;
    }

    private void doStartAction(Monitor.Async monitor) {

        //start action
        Throwable ex = null;
        if (config.startAction != null) {
            try {
                config.startAction.execute();
            } catch (Throwable t) {
                ex = t;
            }
        }

        //handle result of start action
        if (config.syncStart) {
            //send signal about start
            synchronized (this) {
                this.syncStartSignal = Optional.ofNullable(ex);
                this.notifyAll();
            }
        } else {
            //simply log start error if present
            if (ex != null) {
                LOGGER.error("", ex);
            }
        }
        if (ex != null) {
            //finish async-chain because of start failure
            monitor.cancel();
            return;
        }

        //next
        doLoopCycle(monitor, true, true, false);
    }

    private void doLoopCycle(Monitor.Async monitor, boolean loopHead, boolean initial, boolean error) {
        Long invokeTs = ClockUtils.UNI_CLOCK.millis();

        //invoke
        if (!loopHead) {
            boolean interrupted = Thread.interrupted();//always read interruption flag to reset it
            try {
                if (!error || !config.noRepeatOnError) {
                    this.invokeRequested = false;
                    if (!skipNearestInvoke && (!initial || !config.skipInitialInvoke)) {
                        if (config.loopAction != null) {
                            config.loopAction.execute();
                        } else {
                            checkNotNull(config.loopActionWithLoopArg).execute(AsyncLoop.this);
                        }
                    }
                    this.skipNearestInvoke = false;
                }
                initial = false;
                error = false;
            } catch (Throwable t) {
                if (t instanceof InterruptedException) {
                    interrupted = true;
                }
                error = true;
                LOGGER.error("exception in loop action of \"" + name + "\", ignore", t);
            }
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }

        //stop conditions
        if (stopRequested
            || (config.stopCondition != null && NonnullHandler.executeSafe(() -> config.stopCondition.test(null), false))
        ) {
            doWaitForStopRequest(monitor);
            return;
        }

        //calculate next invokeTs
        invokeTs = config.nextInvokeTs(checkNotNull(invokeTs), initial, error);

        //wait
        final boolean fInitial = initial;
        final boolean fError = error;
        Monitor.Async.waitDeadline(
            pipe,
            tracker,
            invokeTs,
            ClockUtils.UNI_CLOCK,
            monitor,
            () -> invokeRequested || stopRequested, //both volatile
            ignored -> {
                if (!stopRequested) {
                    //continue loop
                    doLoopCycle(monitor, false, fInitial, fError);
                } else {
                    //exit loop because stop was requested during waiting
                    doWaitForStopRequest(monitor);
                }
            }
        );
    }

    private void doWaitForStopRequest(Monitor.Async monitor) {

        //immediately next step if possible
        if (config.stopAction == null || stopRequested) {
            doStopAction(monitor);
            return;
        }

        //async wait for stop request
        Monitor.Async.waitDeadline(
            pipe,
            tracker,
            null, //deadlineTs
            ClockUtils.UNI_CLOCK,
            monitor,
            () -> stopRequested,
            ignored -> doStopAction(monitor)
        );
    }

    private void doStopAction(Monitor.Async monitor) {
        boolean interrupted = false;
        if (config.stopAction != null) {
            if (Thread.interrupted()) {//clear possible interruption
                interrupted = true;
            }
            Action.executeSafe(config.stopAction);
        }
        NonnullDataAction<AsyncLoop> fh = finishHook;//volatile
        if (fh != null) {
            if (Thread.interrupted()) {//clear possible interruption
                interrupted = true;
            }
            NonnullDataAction.executeSafe(fh, this);
        }
        if (interrupted) {
            Thread.currentThread().interrupt();
        }
        //NOTE: this is last step, async-chain is finished
        monitor.cancel();
    }
}
