package com.freesolutions.pocket.pocketutils.concurrency.queue;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.QueueStore;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class LinkedActionQueue implements QueueStore<Action> {

    private final BlockingQueue<Action> queue = new LinkedBlockingDeque<>();

    @Override
    public void add(@Nonnull Action item) {
        queue.offer(item);
    }

    @Nullable
    @Override
    public Action take(long timeoutMs) throws InterruptedException {
        return queue.poll(timeoutMs, TimeUnit.MILLISECONDS);
    }

    @Nonnull
    @Override
    public Action take() throws InterruptedException {
        return queue.take();
    }
}
