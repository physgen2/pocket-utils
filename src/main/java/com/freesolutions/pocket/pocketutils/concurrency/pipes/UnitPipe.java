package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class UnitPipe extends QuotaPipe {

    @Nonnull
    public static UnitPipe of(@Nonnull Pipe delegate) {
        if (delegate instanceof UnitPipe) {
            return (UnitPipe) delegate;//reuse existing without additional wrapping
        }
        return new UnitPipe(delegate);
    }

    protected UnitPipe(@Nonnull Pipe delegate) {
        super(delegate, 1);
    }
}
