package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import javax.annotation.concurrent.ThreadSafe;
import java.lang.invoke.VarHandle;

/**
 * @author Stanislau Mirzayeu
 */
//NOTE: nullability unknown
@ThreadSafe
public class Arb<T> {

    private static final VarHandle VALUE_VH = Vars.getVarHandle(Arb.class, "value", Object.class);

    private volatile T value;

    protected Arb() {
        //nothing
    }

    protected Arb(T initial) {
        this.value = initial;
    }

    protected T arbGet() {
        return value;
    }

    protected void arbSet(T value) {
        this.value = value;
    }

    protected boolean arbCompareAndSet(T expect, T update) {
        return VALUE_VH.compareAndSet(this, expect, update);
    }

    protected T arbGetAndSet(T newValue) {
        @SuppressWarnings("unchecked")
        T result = (T) VALUE_VH.getAndSet(this, newValue);
        return result;
    }
}
