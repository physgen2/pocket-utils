package com.freesolutions.pocket.pocketutils.concurrency.timing.control;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketlifecycle.startable.AutoStartable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.threads.IntervalStrategy;
import com.freesolutions.pocket.pocketutils.concurrency.threads.MonitoredIntervalStrategy;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.GlobalTracker;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Query;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Tracker;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.LifecycleStartable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicReference;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public class LoopControl extends LifecycleStartable implements AutoStartable {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoopControl.class);

    @Nonnull
    private final Tracker tracker;
    @Nonnull
    private final Pipe pipe;
    @Nullable
    private final IntervalStrategy intervalStrategy;
    @Nonnull
    private final Action action;
    @Nonnull
    private final AtomicReference<Query> query = new AtomicReference<>();

    public LoopControl(
        @Nonnull Pipe pipe,
        @Nullable IntervalStrategy intervalStrategy,
        @Nonnull Action action
    ) {
        this(GlobalTracker.instance(), pipe, intervalStrategy, action);
    }

    public LoopControl(
        @Nonnull Tracker tracker,
        @Nonnull Pipe pipe,
        @Nullable IntervalStrategy intervalStrategy,
        @Nonnull Action action
    ) {
        checkArgument(!(intervalStrategy instanceof MonitoredIntervalStrategy), "MonitoredIntervalStrategy is not supported");
        this.tracker = tracker;
        this.pipe = pipe;
        this.intervalStrategy = intervalStrategy;
        this.action = action;
    }

    @Override
    public void onStart() throws Exception {
        schedule(Query.of(pipe, 0L, this::bodyAction), false);//initial
    }

    @Override
    public void onStop() throws Exception {
        Query oldQuery = query.getAndSet(null);
        if (oldQuery != null) {
            oldQuery.cancel();
        }
    }

    public void invoke() {
        schedule(Query.of(pipe, 0L, this::bodyAction), true);//immediate
    }

    private void bodyAction() throws Exception {
        long invokeTs = ClockUtils.UNI_CLOCK.millis();
        Long nextInvokeTs = invokeTs;
        try {
            action.execute();
            try {
                nextInvokeTs = intervalStrategy != null ? invokeTs + intervalStrategy.getNextIntervalMs() : null;
            } catch (Throwable t) {
                LOGGER.error("Exception while getting next interval, wait 100ms and repeat", t);
                nextInvokeTs = invokeTs + 100L;
            }
        } finally {
            if (nextInvokeTs != null) {
                schedule(Query.of(pipe, nextInvokeTs, ClockUtils.UNI_CLOCK, this::bodyAction), true);
            }
        }
    }

    private void schedule(@Nonnull Query newQuery, boolean ignoreIfOldAbsent) {
        do {
            Query oldQuery = query.get();
            if (oldQuery == null && ignoreIfOldAbsent) {
                return;
            }
            if (query.compareAndSet(oldQuery, newQuery)) {
                if (oldQuery != null) {
                    oldQuery.cancel();
                }
                tracker.query(newQuery);
                break;
            }
        } while (true);
    }
}
