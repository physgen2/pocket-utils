package com.freesolutions.pocket.pocketutils.concurrency.feed.iface;

import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Feed<G, P, L> {

    @Nonnull
    Subscription<G, P, L> subscribe(
            @Nonnull Pipe pipe,
            @Nonnull G group,
            @Nonnull P context,
            @Nonnull L listener
    );

    void applyAction(@Nonnull G group, @Nonnull ListenerAction<G, P, L> listenerAction);

    int size();

    int groupSize(@Nonnull G group);
}
