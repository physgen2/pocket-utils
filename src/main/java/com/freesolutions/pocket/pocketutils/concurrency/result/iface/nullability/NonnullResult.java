package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext.*;

/**
 * @author Stanislau Mirzayeu
 */
public interface NonnullResult<T> extends
        NonnullReadableResult<T>, NonnullWritableResult<T>,
        NonnullReadableResultDelegatesExt<T, NonnullResult<T>>,
        NonnullReadableResultSubmitsExt<T, NonnullResult<T>>, NonnullWritableResultSubmitsExt<T, NonnullResult<T>>,
        NonnullWritableResultTransformsExt<T, NonnullResult<T>>,
        NonnullWritableResultExpirationsExt<T, NonnullResult<T>>, WritableResultExpirationsExt<T, NonnullResult<T>>
{
    //nothing
}
