package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncStatus;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NonnullSyncResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface ResultTakeable<T> extends Takeable<T> {

    @Nonnull
    NonnullResult<T> takeR();

    @Nullable
    default T take(long timeoutMs) throws InterruptedException {
        T v;
        NonnullResult<T> r = takeR();
        SyncStatus ss = r.sync(SyncQuery.timeout(timeoutMs)).status();
        switch (ss) {
            case OK:
                checkState(r.status().present);
                v = r.value();
                break;
            case TIMED_OUT:
                r.cancel();
                v = r.valueOrNull();//optimistic
                break;
            case REJECTED:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                throw new IllegalStateException("Rejected must be impossible");
            case INTERRUPTED:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                throw new InterruptedException("Interrupted by take");
            default:
                throw new IllegalStateException("Unknown status result: " + ss);
        }
        return v;
    }

    @Nonnull
    default T take() throws InterruptedException {
        T v;
        NonnullResult<T> r = takeR();
        SyncStatus ss = r.sync(SyncQuery.infinite()).status();
        switch (ss) {
            case OK:
                checkState(r.status().present);
                v = r.value();
                break;
            case TIMED_OUT:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                throw new IllegalStateException("Timeout must be impossible");
            case REJECTED:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                throw new IllegalStateException("Rejected must be impossible");
            case INTERRUPTED:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                throw new InterruptedException("Interrupted by take");
            default:
                throw new IllegalStateException("Unknown sync status: " + ss);
        }
        return v;
    }

    @Nonnull
    default NonnullSyncResult<T> sync(@Nonnull SyncQuery syncQuery) {
        T v;
        NonnullResult<T> r = takeR();
        SyncStatus ss = r.sync(syncQuery).status();
        switch (ss) {
            case OK:
                checkState(r.status().present);
                v = r.value();
                break;
            case TIMED_OUT:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                return NonnullSyncResult.ofTimedOut();
            case REJECTED:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                return NonnullSyncResult.ofRejected();
            case INTERRUPTED:
                r.cancel();
                v = r.valueOrNull();
                if (v != null) {
                    break;//optimistic
                }
                return NonnullSyncResult.ofInterrupted();
            default:
                throw new IllegalStateException("Unknown sync status: " + ss);
        }
        return NonnullSyncResult.ofOk(v);
    }
}
