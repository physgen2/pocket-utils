package com.freesolutions.pocket.pocketutils.concurrency.timing;

import com.freesolutions.pocket.pocketutils.actions.FromNonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
public class ClockUtils {

    public static final Clock UTC = Clock.systemUTC();
    public static final Clock UNI_CLOCK = UTC;

    public static boolean isTimeoutExist(long timeoutMs) {
        return timeoutMs > 0L;
    }

    public static boolean isTimedOut(long deadlineTs, @Nonnull Clock clock) {
        return clock.millis() >= deadlineTs;
    }

    public static boolean isTimedOut(long deadlineTs, long baseTs) {
        return baseTs >= deadlineTs;
    }

    public static long toTimeout(long deadlineTs, @Nonnull Clock clock) {
        return toTimeout(deadlineTs, clock.millis());
    }

    public static long toTimeout(long deadlineTs, long baseTs) {
        return deadlineTs > baseTs ? deadlineTs - baseTs : 0L;
    }

    public static long toDeadlineTs(long timeoutMs, @Nonnull Clock clock) {
        return toDeadlineTs(timeoutMs, clock.millis());
    }

    public static long toDeadlineTs(long timeoutMs, long baseTs) {
        return isTimeoutExist(timeoutMs) ? baseTs + timeoutMs : baseTs;
    }

    public static long convertTs(long fromTs, @Nonnull Clock fromClock, @Nonnull Clock toClock) {
        return fromClock != toClock ? //comparison by reference, ok
            fromTs + (toClock.millis() - fromClock.millis()) :
            fromTs;
    }

    public static void waitTimeout(long timeoutMs) throws InterruptedException {
        Monitor.Sync.waitTimeout(timeoutMs);
    }

    public static <M> boolean waitTimeout(
        @Nullable Long timeoutMs,
        @Nonnull M monitor,
        @Nonnull NonnullDataHandler<M, Boolean> condition
    ) throws InterruptedException {
        return Monitor.Sync.waitTimeout(timeoutMs, monitor, condition);
    }

    @Nullable
    public static <M, R> R waitTimeoutR(
        @Nullable Long timeoutMs,
        @Nonnull M monitor,
        @Nonnull FromNonnullDataHandler<M, R> handler
    ) throws InterruptedException {
        return Monitor.Sync.waitTimeoutR(timeoutMs, monitor, handler);
    }

    public static void waitDeadline(long deadlineTs, @Nonnull Clock clock) throws InterruptedException {
        Monitor.Sync.waitDeadline(deadlineTs, clock);
    }

    public static <M> boolean waitDeadline(
        @Nullable Long deadlineTs,
        @Nonnull Clock clock,
        @Nonnull M monitor,
        @Nonnull NonnullDataHandler<M, Boolean> condition
    ) throws InterruptedException {
        return Monitor.Sync.waitDeadline(deadlineTs, clock, monitor, condition);
    }

    @Nullable
    public static <M, R> R waitDeadlineR(
        @Nullable Long deadlineTs,
        @Nonnull Clock clock,
        @Nonnull M monitor,
        @Nonnull FromNonnullDataHandler<M, R> handler
    ) throws InterruptedException {
        return Monitor.Sync.waitDeadlineR(deadlineTs, clock, monitor, handler);
    }
}
