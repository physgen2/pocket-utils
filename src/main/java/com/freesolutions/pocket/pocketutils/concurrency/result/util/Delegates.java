package com.freesolutions.pocket.pocketutils.concurrency.result.util;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class Delegates {
    private static final Logger LOGGER = LoggerFactory.getLogger(Delegates.class);

    //---------------------------------------------------------------------------------------------

    public static <T> void logIfError(NullableReadableResult<T> source) {
        if (source.status().error) {
            LOGGER.error("", checkNotNull(source.error()));
        }
    }

    public static <T> void logIfError(NonnullReadableResult<T> source) {
        if (source.status().error) {
            LOGGER.error("", checkNotNull(source.error()));
        }
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void gagN(Pipe pipe, NullableReadableResult<T> source) {
        source.whenDone(pipe, () -> logIfError(source));
    }

    public static <T> void gag(Pipe pipe, NonnullReadableResult<T> source) {
        source.whenDone(pipe, () -> logIfError(source));
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void delegateNR2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFrom(source)) {
                logIfError(source);
            }
        });
    }

    public static <T> void delegateR2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFrom(source)) {
                logIfError(source);
            }
        });
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void delegateIfPresentNR2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFromIfPresent(source)) {
                logIfError(source);
            }
        });
    }

    public static <T> void delegateIfPresentR2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFromIfPresent(source)) {
                logIfError(source);
            }
        });
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void delegateIfNotPresentNR2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFromIfNotPresent(source)) {
                logIfError(source);
            }
        });
    }

    public static <T> void delegateIfNotPresentNR2R(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFromIfNotPresent(source)) {
                logIfError(source);
            }
        });
    }

    public static <T> void delegateIfNotPresentR2NR(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFromIfNotPresent(source)) {
                logIfError(source);
            }
        });
    }

    public static <T> void delegateIfNotPresentR2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<T> target) {
        source.whenDone(pipe, () -> {
            if (!target.assignFromIfNotPresent(source)) {
                logIfError(source);
            }
        });
    }

    //---------------------------------------------------------------------------------------------
}
