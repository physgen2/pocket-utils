package com.freesolutions.pocket.pocketutils.concurrency.feed.impl;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.concurrency.feed.iface.Feed;
import com.freesolutions.pocket.pocketutils.concurrency.feed.iface.ListenerAction;
import com.freesolutions.pocket.pocketutils.concurrency.feed.iface.Subscription;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.primitives.CountingMap;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Consumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Topic;
import com.freesolutions.pocket.pocketutils.concurrency.topic.impl.topic.DistributingTopic;
import com.freesolutions.pocket.pocketlifecycle.value.LifeAction;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.LifecycleListener;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.LifecycleStartable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @param <G> type of group key, data class for grouping subscriptions by it, must have proper equals() & hashCode() redefined
 * @param <P> type of context, any data class, assumed, that it able to determine group key
 * @param <L> type of listener, any interface
 *
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public abstract class AbstractFeed<G, P, L> implements Feed<G, P, L> {

    @ThreadSafe
    public abstract class AbstractSubscription extends LifecycleStartable implements Subscription<G, P, L> {

        @Nonnull
        protected final Consumer<Item<G, P, L>> consumer;
        @Nonnull
        protected final Pipe pipe;
        @Nonnull
        protected final G group;
        @Nonnull
        protected final P context;
        @Nonnull
        protected final L listener;

        public AbstractSubscription(
            @Nonnull Pipe pipe,
            @Nonnull G group,
            @Nonnull P context,
            @Nonnull L listener
        ) {
            this.consumer = topic.listenableConsumer(
                item -> item.group.equals(group),
                item -> applyAction(item.listenerAction)
            );
            this.pipe = pipe;
            this.group = group;
            this.context = context;
            this.listener = listener;
        }

        @Nonnull
        @Override
        public Feed<G, P, L> feed() {
            return AbstractFeed.this;
        }

        @Nonnull
        @Override
        public Pipe pipe() {
            return pipe;
        }

        @Nonnull
        @Override
        public G group() {
            return group;
        }

        @Nonnull
        @Override
        public P context() {
            return context;
        }

        @Nonnull
        @Override
        public L listener() {
            return listener;
        }

        @Override
        public void onLifeAction(@Nonnull LifeAction lifeAction) throws Exception {
            switch (lifeAction) {
                case PRE_START: {
                    incrementGroup(group);
                    try {
                        onPreStart();//allow failure, ok
                    } catch (Throwable t) {
                        decrementGroup(group);
                        throw t;
                    }
                    try {
                        notifyLifecycleListener(lifeAction);//allow failure, ok
                    } catch (Throwable t) {
                        Action.executeSafe(this::onPostStop);
                        decrementGroup(group);
                        throw t;
                    }
                    break;
                }
                case START: {
                    Startable.startSafe(consumer);//assume consumer can't fail on start, ok
                    try {
                        onStart();//allow failure, ok
                    } catch (Throwable t) {
                        consumer.stop();
                        throw t;
                    }
                    try {
                        notifyLifecycleListener(lifeAction);//allow failure, ok
                    } catch (Throwable t) {
                        Action.executeSafe(this::onStop);
                        consumer.stop();
                        throw t;
                    }
                    break;
                }
                case POST_START: {
                    Action.executeSafe(() -> notifyLifecycleListener(lifeAction));
                    Action.executeSafe(this::onPostStart);
                    break;
                }
                case PRE_STOP: {
                    Action.executeSafe(this::onPreStop);
                    Action.executeSafe(() -> notifyLifecycleListener(lifeAction));
                    break;
                }
                case STOP: {
                    Action.executeSafe(() -> notifyLifecycleListener(lifeAction));
                    Action.executeSafe(this::onStop);
                    consumer.stop();
                    break;
                }
                case POST_STOP: {
                    Action.executeSafe(() -> notifyLifecycleListener(lifeAction));
                    Action.executeSafe(this::onPostStop);
                    decrementGroup(group);
                    break;
                }
                default:
                    throw new IllegalStateException("Unknown lifeAction: " + lifeAction);
            }
        }

        private void notifyLifecycleListener(@Nonnull LifeAction lifeAction) throws Exception {
            if (listener instanceof LifecycleListener) {
                ((LifecycleListener) listener).onLifeAction(lifeAction);
            }
        }
    }

    @Nonnull
    private final NonnullDataAction<G> startGroup;
    @Nonnull
    private final NonnullDataAction<G> stopGroup;

    @Nonnull
    private final Topic<Item<G, P, L>> topic = new DistributingTopic<>();
    @Nonnull
    private final CountingMap<G> groupCounters = new CountingMap<>();

    public AbstractFeed() {
        this(
            NonnullDataAction.identity(),
            NonnullDataAction.identity()
        );
    }

    public AbstractFeed(
        @Nonnull NonnullDataAction<G> startGroup,
        @Nonnull NonnullDataAction<G> stopGroup
    ) {
        this.startGroup = startGroup;
        this.stopGroup = stopGroup;
    }

    @Override
    public void applyAction(@Nonnull G group, @Nonnull ListenerAction<G, P, L> listenerAction) {
        topic.add(new Item<>(group, listenerAction));
    }

    @Override
    public int size() {
        return topic.size();
    }

    @Override
    public int groupSize(@Nonnull G group) {
        CountingMap.Counter groupCounter = groupCounters.get(group);
        return groupCounter != null ? groupCounter.get() : 0;
    }

    private void incrementGroup(@Nonnull G group) throws Exception {
        CountingMap.Counter groupCounter = groupCounters.incrementAlwaysAndGetIfInitial(group);
        if (groupCounter != null) {
            try {
                startGroup.execute(group);//allow failure, ok
            } catch (Throwable t) {
                groupCounters.decrement(group, groupCounter);
                throw t;
            }
        }
    }

    private void decrementGroup(@Nonnull G group) throws Exception {
        CountingMap.Counter groupCounter = groupCounters.get(group);
        if (groupCounter != null && groupCounters.decrementAlwaysAndGetIfNotLast(group, groupCounter) == null) {
            NonnullDataAction.executeSafe(stopGroup, group);
        }
    }

    @Immutable
    private static class Item<G, P, L> {

        @Nonnull
        public final G group;
        @Nonnull
        public final ListenerAction<G, P, L> listenerAction;

        public Item(@Nonnull G group, @Nonnull ListenerAction<G, P, L> listenerAction) {
            this.group = group;
            this.listenerAction = listenerAction;
        }
    }
}
