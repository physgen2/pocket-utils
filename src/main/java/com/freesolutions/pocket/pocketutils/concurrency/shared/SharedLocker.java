package com.freesolutions.pocket.pocketutils.concurrency.shared;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.loop.LoopConfig;
import com.freesolutions.pocket.pocketutils.concurrency.loop.ThreadLoop;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.time.InstantTs;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class SharedLocker implements Startable {

    @ThreadSafe
    public interface IdGenerator {

        IdGenerator NONE = () -> {
            throw new UnsupportedOperationException("none");
        };

        long generateId();
    }

    @ThreadSafe
    public interface IORoutines {

        void acquire(
            long lockId,
            @Nonnull InstantTs expirationTs,
            @Nonnull InstantTs deathTs
        ) throws Exception;

        Set<Long> prolongate(
            @Nonnull Iterable<Long> lockIds,
            @Nullable Integer lockIdsCount,
            @Nonnull InstantTs nowTs,
            @Nonnull InstantTs expirationTs,
            @Nonnull InstantTs deathTs
        ) throws Exception;

        void close(@Nonnull Iterable<Long> lockIds, @Nullable Integer lockIdsCount) throws Exception;

        void closeDead(@Nonnull InstantTs nowTs) throws Exception;
    }

    @ThreadSafe
    public static class Lock implements AutoCloseable {

        public final long lockId;

        private volatile long expirationTs;
        private volatile long deathTs;
        private volatile boolean closed;

        @Override
        public void close() throws Exception {
            this.closed = true;
        }

        private Lock(long lockId) {
            this.lockId = lockId;
        }

        private boolean isActive(@Nonnull InstantTs nowTs) {
            return !closed && nowTs.instantTs < expirationTs;
        }

        private boolean isClosedOrDead(@Nonnull InstantTs nowTs) {
            return closed || nowTs.instantTs >= deathTs;
        }

        private void forceProlongate(
            @Nonnull InstantTs expirationTs,
            @Nonnull InstantTs deathTs
        ) {
            checkArgument(deathTs.isAfterOrEqual(expirationTs));
            //NOTE: death - first, expiration - last
            this.deathTs = deathTs.instantTs;
            this.expirationTs = expirationTs.instantTs;
        }
    }

    private static final long UPDATE_INTERVAL_MS = TimeUnit.SECONDS.toMillis(1L);
    private static final long CLEAN_INTERVAL_MS = TimeUnit.MINUTES.toMillis(5L);
    private static final long CLEAN_ERROR_INTERVAL_MS = TimeUnit.SECONDS.toMillis(1L);

    private final long expirationMs;
    private final long silenceMs;
    @Nonnull
    private final IdGenerator idGenerator;
    @Nonnull
    private final IORoutines ioRoutines;

    @Nonnull
    private final ConcurrentMap<Long, Lock> locks = new ConcurrentHashMap<>();
    private final ConcurrentMap<Long, Lock> locksToClose = new ConcurrentHashMap<>();

    @Nonnull
    private final ThreadLoop updater;
    @Nonnull
    private final ThreadLoop cleaner;

    public SharedLocker(
        @Nonnull String name,
        long expirationMs,
        long silenceMs,
        @Nonnull IdGenerator idGenerator,
        @Nonnull IORoutines ioRoutines
    ) {
        checkArgument(expirationMs >= 0L, "expirationMs: %s", expirationMs);
        checkArgument(silenceMs >= 0L, "silenceMs: %s", silenceMs);

        this.expirationMs = expirationMs;
        this.silenceMs = silenceMs;
        this.idGenerator = idGenerator;
        this.ioRoutines = ioRoutines;

        this.updater = new ThreadLoop(
            name + "-updater",
            LoopConfig.builder()
                .intervalMs(UPDATE_INTERVAL_MS)
                .defaultIntervalPerturbation()
                .loopAction(this::periodicalUpdate)
                .build()
        );
        this.cleaner = new ThreadLoop(
            name + "-cleaner",
            LoopConfig.builder()
                .intervalMs(CLEAN_INTERVAL_MS)
                .defaultIntervalPerturbation()
                .errorDelayMs(CLEAN_ERROR_INTERVAL_MS)
                .loopAction(this::periodicalClean)
                .build()
        );
    }

    @Override
    public void start() throws Exception {
        cleaner.start();
        updater.start();
    }

    @Override
    public void stop() {
        updater.stop();
        cleaner.stop();
        //NOTE: nothing to do more, leave active locks as is
    }

    @Nonnull
    public Lock acquire() {
        long lockId = idGenerator.generateId();
        InstantTs nowTs = InstantTs.now(ClockUtils.UTC);
        InstantTs expirationTs = nowTs.plusMillis(expirationMs);
        InstantTs deathTs = nowTs.plusMillis(expirationMs + silenceMs);
        Action.executeUnchecked(() ->
            ioRoutines.acquire(
                lockId,
                expirationTs,
                deathTs
            )
        );
        Lock lock = new Lock(lockId);
        lock.forceProlongate(expirationTs, deathTs);
        locks.put(lockId, lock); //only after IO-operation
        // NOTE: ignore if previous lock present for such lockId, should be impossible, but avoid hard check
        return lock;
    }

    private void periodicalUpdate() {

        InstantTs nowTs = InstantTs.now(ClockUtils.UTC);

        //[at start] detect locks to prolongate
        Map<Long, Lock> locksToProlongate = locks.values().stream()
            .filter(x -> x.isActive(nowTs))
            .collect(Collectors.toMap(x -> x.lockId, Function.identity()));

        //[at start] detect locks to close & move them into separated collection
        locks.values().stream()
            .filter(x -> x.isClosedOrDead(nowTs))
            .forEach(x -> locksToClose.put(x.lockId, x));
        locksToClose.values().forEach(x -> locks.remove(x.lockId, x));

        //prolongate
        {
            Collection<Long> lockIds = locksToProlongate.keySet();
            InstantTs expirationTs = nowTs.plusMillis(expirationMs);
            InstantTs deathTs = nowTs.plusMillis(expirationMs + silenceMs);
            Action.executeSafe(() -> {
                Set<Long> prolongatedLockIds = ioRoutines.prolongate(
                    lockIds, //lockIds
                    lockIds.size(), //lockIdsCount
                    nowTs,
                    expirationTs,
                    deathTs
                );
                prolongatedLockIds.stream()
                    .map(locksToProlongate::get)
                    .filter(Objects::nonNull)
                    .forEach(lock -> lock.forceProlongate(expirationTs, deathTs));
            });
        }

        //close
        {
            Collection<Long> lockIds = locksToClose.keySet();
            Action.executeSafe(() -> {
                ioRoutines.close(lockIds, lockIds.size());
                lockIds.stream()
                    .map(locksToClose::get)
                    .filter(Objects::nonNull)
                    .forEach(lock -> locksToClose.remove(lock.lockId, lock));
            });
        }
    }

    private void periodicalClean() {
        InstantTs nowTs = InstantTs.now(ClockUtils.UTC);
        Action.executeUnchecked(() ->
            ioRoutines.closeDead(nowTs)
        );
    }
}
