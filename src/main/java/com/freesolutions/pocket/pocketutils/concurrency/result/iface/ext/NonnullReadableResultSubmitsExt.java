package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Submits;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NonnullReadableResultSubmitsExt<T, RESULT extends NonnullReadableResult<T>> extends NonnullReadableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT submitR(Pipe pipe, NonnullDataAction<NonnullReadableResult<T>> handler) {
        Submits.submitR(pipe, this, handler);
        return (RESULT) this;
    }

    default RESULT submitX(Pipe pipe, NonnullDataAction<T> handler) {
        Submits.submitX(pipe, this, handler);
        return (RESULT) this;
    }
}
