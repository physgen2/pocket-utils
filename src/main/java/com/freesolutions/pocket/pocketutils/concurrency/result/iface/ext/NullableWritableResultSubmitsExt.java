package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.actions.*;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Submits;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NullableWritableResultSubmitsExt<T, RESULT extends NullableWritableResult<T>> extends NullableWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofSubmit2NR(Pipe pipe, NonnullHandler<NullableReadableResult<T>> handler) {
        Submits.submit2NR(pipe, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT ofSubmit2NX(Pipe pipe, Handler<T> handler) {
        Submits.submit2NX(pipe, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofSubmitNS2NR(Pipe pipe, @Nullable S source, ToNonnullDataHandler<S, NullableReadableResult<T>> handler) {
        Submits.submitNS2NR(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofSubmitS2NR(Pipe pipe, S source, NonnullDataHandler<S, NullableReadableResult<T>> handler) {
        Submits.submitS2NR(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofSubmitNS2NX(Pipe pipe, @Nullable S source, DataHandler<S, T> handler) {
        Submits.submitNS2NX(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofSubmitS2NX(Pipe pipe, S source, FromNonnullDataHandler<S, T> handler) {
        Submits.submitS2NX(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
