package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Stanislau Mirzayeu
 */
@RestartableLifecycle
@ThreadSafe
public class MultiTracker implements Tracker, Startable {

    @Nonnull
    private final SimpleTracker[] trackers;

    public MultiTracker(@Nonnull String name, int count) {
        SimpleTracker[] trackers = new SimpleTracker[count];
        for (int i = 0; i < count; i++) {
            trackers[i] = new SimpleTracker(name + "-" + (i + 1));
        }
        this.trackers = trackers;
    }

    @Override
    public void start() throws Exception {
        forEach(trackers, false, Startable::start);//exception can be thrown out, ok
    }

    @Override
    public void stop() {
        Action.executeSafe(() -> forEach(trackers, true, Startable::stop));
    }

    @Override
    public void query(@Nonnull Query query) {
        trackers[ThreadLocalRandom.current().nextInt(trackers.length)].query(query);
    }

    private static <T> void forEach(@Nonnull T[] a, boolean reversed, @Nonnull NonnullDataAction<T> action) throws Exception {
        if (!reversed) {
            for (int i = 0; i < a.length; i++) {
                action.execute(a[i]);
            }
        } else {
            for (int i = a.length - 1; i >= 0; i--) {
                action.execute(a[i]);
            }
        }
    }
}
