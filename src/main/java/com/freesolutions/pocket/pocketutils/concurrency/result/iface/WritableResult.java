package com.freesolutions.pocket.pocketutils.concurrency.result.iface;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability unknown, see derived interfaces, ok
 */
public interface WritableResult<T> extends Result<T>, PresentableResult<T> {

    boolean error(@Nonnull Throwable t);

    boolean cancel();

    boolean timeout();

    boolean interrupt();
}
