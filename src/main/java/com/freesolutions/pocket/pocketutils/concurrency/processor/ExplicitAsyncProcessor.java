package com.freesolutions.pocket.pocketutils.concurrency.processor;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.QueueStore;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NonnullSyncResult;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NullableSyncResult;
import com.freesolutions.pocket.pocketutils.concurrency.sync.syncable.Syncable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class ExplicitAsyncProcessor<Q extends QueueStore<Action>> implements QueueBasedAsyncProcessor<Q>, Syncable<Void> {

    @Nonnull
    private final Q actionQueue;

    public ExplicitAsyncProcessor(@Nonnull Q actionQueue) {
        this.actionQueue = actionQueue;
    }

    @Override
    public void add(@Nonnull Action action) {
        actionQueue.add(action);
    }

    @Nonnull
    @Override
    public Q actionQueue() {
        return actionQueue;
    }

    @Nonnull
    @Override
    public NullableSyncResult<Void> sync(@Nonnull SyncQuery syncQuery) {
        NonnullSyncResult<Action> syncResult = actionQueue.sync(syncQuery);
        NullableSyncResult<Void> notOk = NullableSyncResult.ofNotOkIfNotOk(syncResult);
        if (notOk != null) {//pass if not ok
            return notOk;
        }
        checkState(syncResult.status().ok);
        Action.executeSafe(syncResult.value());
        return NullableSyncResult.ofOkNull();
    }

    @Override
    public void start() throws Exception {
        //nothing
    }

    @Override
    public void stop() {
        //nothing
    }
}
