package com.freesolutions.pocket.pocketutils.concurrency.processor;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.executor.StartableThreadPoolExecutor;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class ExecutorServiceAsyncProcessor implements AsyncProcessor {

    @Nonnull
    private final StartableThreadPoolExecutor executor;

    public ExecutorServiceAsyncProcessor(@Nonnull StartableThreadPoolExecutor executor) {
        this.executor = executor;
    }

    @Override
    public void add(@Nonnull Action action) {
        Action.executeSafe(() -> executor.submit(() -> Action.executeSafe(action)));
    }

    @Override
    public void start() throws Exception {
        executor.start();
    }

    @Override
    public void stop() {
        executor.stop();
    }
}
