package com.freesolutions.pocket.pocketutils.concurrency.topic.iface;

import com.freesolutions.pocket.pocketlifecycle.startable.AutoStartable;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Addable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.function.Predicate;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Consumer<T> extends Addable<T>, AutoStartable {

    @Nonnull
    Topic<T> topic();

    @Nonnull
    Predicate<T> filter();
}
