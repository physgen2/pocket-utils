package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public interface NullableWritableResult<T> extends NonnullWritableResult<T>, NullablePresentableResult<T> {

    boolean assignFrom(@Nonnull NullableReadableResult<T> source);

    boolean assignFromIfPresent(@Nonnull NullableReadableResult<T> source);

    boolean assignFromIfNotPresent(@Nonnull NullableReadableResult<?> source);
}
