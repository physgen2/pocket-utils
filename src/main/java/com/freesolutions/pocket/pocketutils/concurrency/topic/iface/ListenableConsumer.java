package com.freesolutions.pocket.pocketutils.concurrency.topic.iface;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface ListenableConsumer<T> extends Consumer<T> {
    //empty
}
