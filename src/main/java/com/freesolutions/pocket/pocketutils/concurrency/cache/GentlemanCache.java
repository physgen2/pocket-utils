package com.freesolutions.pocket.pocketutils.concurrency.cache;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;

import javax.annotation.Nonnull;
import java.time.Clock;
import java.util.Map;

/**
 * @author Stanislau Mirzayeu
 */
public class GentlemanCache<K, V> implements Startable {

    @Nonnull
    public final ExpirableMap<K, ExpirableValue<V>> map;
    @Nonnull
    public final UniqueLocker<K> locker = new UniqueLocker<>();
    @Nonnull
    public final Clock clock;

    public GentlemanCache(
        @Nonnull String name,
        long expirationCheckIntervalMs,
        @Nonnull Clock clock
    ) {
        this.map = new ExpirableMap<>(
            new ExpirableMap.Config<>(
                name,
                expirationCheckIntervalMs,
                entry -> entry.getValue().expirationTs,
                NonnullDataAction.identity(),
                clock
            )
        );
        this.clock = clock;
    }

    public GentlemanCache(
        @Nonnull String name,
        long expirationCheckIntervalMs,
        @Nonnull Clock clock,
        @Nonnull NonnullDataAction<Map.Entry<K, ExpirableValue<V>>> postRemovalAction
    ) {
        this.map = new ExpirableMap<>(
            new ExpirableMap.Config<>(
                name,
                expirationCheckIntervalMs,
                entry -> entry.getValue().expirationTs,
                postRemovalAction,
                clock
            )
        );
        this.clock = clock;
    }

    @Override
    public void start() throws Exception {
        map.start();
    }

    @Override
    public void stop() {
        map.stop();
    }
}
