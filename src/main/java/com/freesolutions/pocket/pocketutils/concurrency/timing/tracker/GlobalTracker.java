package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketlifecycle.application.starter.Starter;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class GlobalTracker extends SimpleTracker {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalTracker.class);

    @ThreadSafe
    private static class GlobalTrackerStoppable implements Startable {

        @Override
        public void start() throws Exception {
            //nothing
        }

        @Override
        public void stop() {
            stopWithoutTouch();
        }
    }

    public static final String NAME = GlobalTracker.class.getSimpleName();

    private static final GlobalTracker INSTANCE = new GlobalTracker();
    private static final GlobalTrackerStoppable STOPPABLE = new GlobalTrackerStoppable();

    //NOTE: register <GlobalTracker> as global-to-stop object in <Starter>
    static {
        Starter.registerGlobalToStop(STOPPABLE);
    }

    @Nonnull
    public static SimpleTracker instance() {
        if (!INSTANCE.isStarted()) {
            Startable.startUnchecked(INSTANCE);
        }
        return INSTANCE;
    }

    @Nonnull
    public static Startable stoppable() {
        return STOPPABLE;
    }

    public static boolean started() {
        return INSTANCE.isStarted();
    }

    public static void stopWithoutTouch() {
        if (INSTANCE.isStarted()) {
            INSTANCE.stop();
        }
    }

    @Override
    public synchronized void start() throws Exception {
        if (!isStarted()) {
            try {
                super.start();
                LOGGER.info("Started " + NAME);
            } catch (Throwable t) {
                LOGGER.error("Failed to start GlobalTracker, critical error, halt VM", t);
                Runtime.getRuntime().halt(-1);
            }
        }
    }

    @Override
    public synchronized void stop() {
        super.stop();//method is necessary only to wrap with sync
    }

    private GlobalTracker() {
        super(NAME);
    }
}
