package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 *
 * Special interface to represent pure concept of queue store.
 * Two key operations are present: add & take.
 */
@ThreadSafe
public interface QueueStore<T> extends Addable<T>, Takeable<T> {
    //empty
}
