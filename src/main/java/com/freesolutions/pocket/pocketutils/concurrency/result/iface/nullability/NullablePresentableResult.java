package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public interface NullablePresentableResult<T> extends NonnullPresentableResult<T> {

    boolean presentNull();

    boolean present(@Nullable T value);
}
