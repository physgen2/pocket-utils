package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;

/**
 * @author Stanislau Mirzayeu
 */
public class Vars {

    public static VarHandle getVarHandle(
            @Nonnull Class<?> cls,
            @Nonnull String declaredFieldName,
            @Nonnull Class<?> declaredFieldType
    ) {
        try {
            return MethodHandles.privateLookupIn(cls, MethodHandles.lookup()).findVarHandle(cls, declaredFieldName, declaredFieldType);
        } catch (Throwable t) {
            throw new Error(
                    "Can't retrieve var handle for declared field" +
                            ", class: " + cls +
                            ", declaredFieldName: " + declaredFieldName +
                            ", declaredFieldType: " + declaredFieldType,
                    t
            );
        }
    }
}
