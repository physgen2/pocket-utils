package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import com.freesolutions.pocket.pocketutils.actions.Action;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class SyncPipe extends UnitPipe {

    @Nonnull
    private final Object monitor;

    @Nonnull
    public static SyncPipe of(@Nonnull Pipe delegate) {
        return new SyncPipe(delegate);
    }

    @Nonnull
    public static SyncPipe of(@Nonnull Pipe delegate, @Nonnull Object monitor) {
        if (delegate instanceof SyncPipe) {
            SyncPipe syncDelegate = (SyncPipe) delegate;
            if (syncDelegate.monitor == monitor) {//comparison by reference
                return syncDelegate;//reuse existing without additional wrapping
            }
        }
        return new SyncPipe(delegate, monitor);
    }

    @Nonnull
    public Object monitor() {
        return monitor;
    }

    @Override
    public void add(@Nonnull Action action) {
        super.add(wrapAction(action));
    }

    private SyncPipe(@Nonnull Pipe delegate) {
        super(delegate);
        this.monitor = this;
    }

    private SyncPipe(@Nonnull Pipe delegate, @Nonnull Object monitor) {
        super(delegate);
        this.monitor = monitor;
    }

    @Nonnull
    private Action wrapAction(@Nonnull Action action) {
        return () -> {
            synchronized (monitor) {
                action.execute();
            }
        };
    }
}
