package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Clock;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class Query {

    private static final int PENDING = 0;
    private static final int APPLIED = 1;
    private static final int CANCELLED = 2;
    private static final int EXECUTING = 3;
    private static final int FINISHED = 4;

    @Nonnull
    private final Pipe pipe;
    private final long uniDeadlineTs;//NOTE: value corresponds to uni-clock
    @Nonnull
    private volatile Action action;//NOTE: action can be set later, after query schedule
    @Nonnull
    private final AtomicInteger status = new AtomicInteger();

    @Nonnull
    public static Query of(@Nonnull Pipe pipe, long delayMs) {
        return of(pipe, delayMs, Action.IDENTITY);
    }

    @Nonnull
    public static Query of(@Nonnull Pipe pipe, long delayMs, @Nonnull Action action) {
        return of(pipe, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, action);
    }

    @Nonnull
    public static Query of(@Nonnull Pipe pipe, long deadlineTs, @Nonnull Clock clock) {
        return of(pipe, deadlineTs, clock, Action.IDENTITY);
    }

    @Nonnull
    public static Query of(@Nonnull Pipe pipe, long deadlineTs, @Nonnull Clock clock, @Nonnull Action action) {
        return new Query(
            pipe,
            ClockUtils.convertTs(deadlineTs, clock, ClockUtils.UNI_CLOCK),
            action
        );
    }

    public long uniDeadlineTs() {
        return uniDeadlineTs;
    }

    public boolean isExpired() {
        return !isPending() || ClockUtils.isTimedOut(uniDeadlineTs, ClockUtils.UNI_CLOCK);
    }

    public boolean isPending() {
        return status.get() == PENDING;
    }

    @Nonnull
    public Query useAction(@Nonnull Action action) {
        this.action = action;
        return this;
    }

    public boolean apply() {
        if (!status.compareAndSet(PENDING, APPLIED)) {
            return false;
        }
        pipe.add(() -> {
            if (enterExecution()) {
                try {
                    action.execute();
                } finally {
                    finishExecution();
                }
            }
        });
        return true;
    }

    public boolean cancel() {
        return status.compareAndSet(PENDING, CANCELLED)
            || status.compareAndSet(APPLIED, CANCELLED);
    }

    protected Query(@Nonnull Pipe pipe, long uniDeadlineTs, @Nonnull Action action) {
        this.pipe = pipe;
        this.uniDeadlineTs = uniDeadlineTs;
        this.action = action;
    }

    protected boolean isFinal() {
        int s = status.get();
        return s == CANCELLED || s == FINISHED;
    }

    protected boolean enterExecution() {
        return status.compareAndSet(APPLIED, EXECUTING);
    }

    protected void finishExecution() {
        status.set(FINISHED);
    }
}
