package com.freesolutions.pocket.pocketutils.concurrency.timing.control;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.GlobalTracker;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Query;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Tracker;
import com.freesolutions.pocket.pocketlifecycle.startable.AutoStartable;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.LifecycleStartable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Clock;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public class ActionControl extends LifecycleStartable implements AutoStartable {

    @Nonnull
    private final Tracker tracker;
    @Nonnull
    private final AtomicReference<Optional<Query>> query = new AtomicReference<>();

    public ActionControl() {
        this.tracker = GlobalTracker.instance();
    }

    public ActionControl(@Nonnull Tracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void onStart() throws Exception {
        while (true) {
            Optional<Query> oldQuery = query.get();
            if (oldQuery != null) {
                break;
            }
            if (query.compareAndSet(null, Optional.empty())) {
                break;
            }
        }
    }

    @Override
    public void onStop() throws Exception {
        while (true) {
            Optional<Query> oldQuery = query.get();
            if (oldQuery == null) {
                break;
            }
            if (query.compareAndSet(oldQuery, null)) {
                oldQuery.ifPresent(Query::cancel);
                break;
            }
        }
    }

    public boolean schedule(@Nonnull Pipe pipe, long delayMs, @Nonnull Action callback) {
        return schedule(pipe, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, callback);
    }

    public boolean schedule(@Nonnull Pipe pipe, long deadlineTs, @Nonnull Clock clock, @Nonnull Action callback) {
        Optional<Query> oldQuery = query.get();
        if (oldQuery == null) {
            return false;
        }
        Query rawQuery = Query.of(pipe, deadlineTs, clock, callback);
        if (!rawQuery.isPending()) {
            return false;
        }
        tracker.query(rawQuery);
        Optional<Query> newQuery = Optional.of(rawQuery);
        while (!query.compareAndSet(oldQuery, newQuery)) {
            oldQuery = query.get();
            if (oldQuery == null) {
                rawQuery.cancel();//cancel, ok
                return false;
            }
        }
        oldQuery.ifPresent(Query::cancel);
        return true;
    }

    public void cancel() {
        while (true) {
            Optional<Query> oldQuery = query.get();
            if (oldQuery == null) {
                break;
            }
            if (query.compareAndSet(oldQuery, Optional.empty())) {
                oldQuery.ifPresent(Query::cancel);
                break;
            }
        }
    }
}
