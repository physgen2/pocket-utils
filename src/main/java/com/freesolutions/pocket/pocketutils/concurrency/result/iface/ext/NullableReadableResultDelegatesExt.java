package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Delegates;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NullableReadableResultDelegatesExt<T, RESULT extends NullableReadableResult<T>> extends NullableReadableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT gagN(Pipe pipe) {
        Delegates.gagN(pipe, this);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT delegateNR2NR(Pipe pipe, NullableWritableResult<T> target) {
        Delegates.delegateNR2NR(pipe, this, target);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT delegateIfPresentNR2NR(Pipe pipe, NullableWritableResult<T> target) {
        Delegates.delegateIfPresentNR2NR(pipe, this, target);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT delegateIfNotPresentNR2NR(Pipe pipe, NullableWritableResult<T> target) {
        Delegates.delegateIfNotPresentNR2NR(pipe, this, target);
        return (RESULT) this;
    }

    default RESULT delegateIfNotPresentNR2R(Pipe pipe, NonnullWritableResult<T> target) {
        Delegates.delegateIfNotPresentNR2R(pipe, this, target);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
