package com.freesolutions.pocket.pocketutils.concurrency.logic.iface;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.List;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface LogicGroup<CONTEXT, CONTROL> {

    @Nonnull
    Logic<CONTEXT, CONTROL> submit(@Nonnull CONTEXT context);

    int size();

    @Nonnull
    List<Logic<CONTEXT, CONTROL>> all();
}
