package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class ReplacingTracker implements Tracker {

    @Nonnull
    private final Tracker delegate;
    @Nonnull
    private final AtomicReference<Query> current = new AtomicReference<>();

    public ReplacingTracker() {
        this.delegate = GlobalTracker.instance();
    }

    public ReplacingTracker(@Nonnull Tracker delegate) {
        this.delegate = delegate;
    }

    @Override
    public void query(@Nonnull Query query) {
        delegate.query(query);
        Query old = current.getAndSet(query);
        if (old != null) {
            old.cancel();
        }
    }

    public void cancel() {
        Query old = current.getAndSet(null);
        if (old != null) {
            old.cancel();
        }
    }
}
