package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NullableFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NullableImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Delegates;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Handlers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class AsyncMonitor {

    private interface Waiter {
        void trigger();
    }

    private class NullableWaiter<T> implements Waiter, Startable {
        @Nonnull
        public final Pipe pipe;
        @Nullable
        public final NullableResult<T> stopper;
        @Nonnull
        public final NonnullHandler<NullableImmutableResult<T>> handler;
        @Nonnull
        public final NullableResult<T> result = NullableFuture.of();

        public NullableWaiter(
            @Nonnull Pipe pipe,
            @Nullable NullableResult<T> stopper,
            @Nonnull NonnullHandler<NullableImmutableResult<T>> handler
        ) {
            this.pipe = pipe;
            this.stopper = stopper;
            this.handler = handler;
        }

        @Override
        public void start() throws Exception {
            waiters.add(this);
            result.submitNR(pipe, ignored -> stop());
            if (stopper != null) {
                stopper.submitNR(pipe, ignored -> {
                    if (!result.status().done) {//fast check
                        synchronized (AsyncMonitor.this) {
                            if (!result.assignFrom(stopper)) {
                                Delegates.logIfError(stopper);
                            }
                        }
                    }
                    stop();
                });
            }
            trigger();//initial trigger
        }

        @Override
        public void stop() {
            checkState(result.status().done);
            waiters.remove(this);
        }

        @Override
        public void trigger() {
            pipe.add(() -> {
                boolean done = result.status().done;//fast check, ok
                if (!done) {
                    synchronized (AsyncMonitor.this) {
                        done = result.status().done || handleAndAssignResult();
                    }
                }
                if (done) {
                    stop();
                }
            });
        }

        private boolean handleAndAssignResult() {
            NullableReadableResult<T> r = Handlers.handleSafe2NR(handler);
            if (!r.status().done) {
                return false;
            }
            if (!result.assignFrom(r)) {
                Delegates.logIfError(r);
            }
            return true;
        }
    }

    private class NonnullWaiter<T> implements Waiter, Startable {
        @Nonnull
        public final Pipe pipe;
        @Nullable
        public final NonnullResult<T> stopper;
        @Nonnull
        public final NonnullHandler<NonnullImmutableResult<T>> handler;
        @Nonnull
        public final NonnullResult<T> result = NonnullFuture.of();

        public NonnullWaiter(
            @Nonnull Pipe pipe,
            @Nullable NonnullResult<T> stopper,
            @Nonnull NonnullHandler<NonnullImmutableResult<T>> handler
        ) {
            this.pipe = pipe;
            this.stopper = stopper;
            this.handler = handler;
        }

        @Override
        public void start() throws Exception {
            waiters.add(this);
            result.submitR(pipe, ignored -> stop());
            if (stopper != null) {
                stopper.submitR(pipe, ignored -> {
                    if (!result.status().done) {//fast check
                        synchronized (AsyncMonitor.this) {
                            if (!result.assignFrom(stopper)) {
                                Delegates.logIfError(stopper);
                            }
                        }
                    }
                    stop();
                });
            }
            trigger();//initial trigger
        }

        @Override
        public void stop() {
            checkState(result.status().done);
            waiters.remove(this);
        }

        @Override
        public void trigger() {
            pipe.add(() -> {
                boolean done = result.status().done;//fast check, ok
                if (!done) {
                    synchronized (AsyncMonitor.this) {
                        done = result.status().done || handleAndAssignResult();
                    }
                }
                if (done) {
                    stop();
                }
            });
        }

        private boolean handleAndAssignResult() {
            NonnullReadableResult<T> r = Handlers.handleSafe2R(handler);
            if (!r.status().done) {
                return false;
            }
            if (!result.assignFrom(r)) {
                Delegates.logIfError(r);
            }
            return true;
        }
    }

    @Nonnull
    private final Set<Waiter> waiters = Collections.synchronizedSet(new HashSet<>());

    @Nonnull
    public <T> NullableResult<T> fetchNR(
        @Nonnull Pipe pipe,
        @Nullable NullableResult<T> stopper,
        @Nonnull NonnullHandler<NullableImmutableResult<T>> handler
    ) {
        NullableWaiter<T> waiter = new NullableWaiter<>(pipe, stopper, handler);
        Startable.startSafe(waiter);
        return waiter.result;
    }

    @Nonnull
    public <T> NonnullResult<T> fetchR(
        @Nonnull Pipe pipe,
        @Nullable NonnullResult<T> stopper,
        @Nonnull NonnullHandler<NonnullImmutableResult<T>> handler
    ) {
        NonnullWaiter<T> waiter = new NonnullWaiter<>(pipe, stopper, handler);
        Startable.startSafe(waiter);
        return waiter.result;
    }

    public void trigger() {
        synchronized (waiters) {//don't remember to synchronize, because iteration over synchronized set must be sync manually
            for (Waiter waiter : waiters) {
                waiter.trigger();
            }
        }
    }
}
