package com.freesolutions.pocket.pocketutils.concurrency.processor;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface AsyncProcessor extends Pipe, Startable {

    AsyncProcessor DIRECT = new AsyncProcessor() {
        @Override
        public void start() throws Exception {
            //nothing
        }

        @Override
        public void stop() {
            //nothing
        }

        @Override
        public void add(@Nonnull Action item) {
            Action.executeSafe(item);
        }
    };
}
