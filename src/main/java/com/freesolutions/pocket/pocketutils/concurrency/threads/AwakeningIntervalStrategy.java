package com.freesolutions.pocket.pocketutils.concurrency.threads;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public class AwakeningIntervalStrategy implements MonitoredIntervalStrategy {

    @Nullable
    private final IntervalStrategy delegate;
    @Nonnull
    private final AtomicBoolean awakeRequired = new AtomicBoolean();

    @Nonnull
    public static AwakeningIntervalStrategy pure() {
        return new AwakeningIntervalStrategy(null);
    }

    @Nonnull
    public static AwakeningIntervalStrategy of(@Nonnull IntervalStrategy delegate) {
        return new AwakeningIntervalStrategy(delegate);
    }

    public void awake() {
        synchronized (this) {
            awakeRequired.set(true);
            this.notifyAll();
        }
    }

    @Override
    public boolean isAwakeRequired() throws Exception {
        boolean r1 = false;
        if (delegate instanceof MonitoredIntervalStrategy) {
            r1 = ((MonitoredIntervalStrategy) delegate).isAwakeRequired();
        }
        boolean r2 = awakeRequired.compareAndSet(true, false);
        return r1 || r2;
    }

    @Override
    public boolean isIntervalsEnabled() {
        if (delegate instanceof MonitoredIntervalStrategy) {
            return ((MonitoredIntervalStrategy) delegate).isIntervalsEnabled();
        }
        return delegate != null;
    }

    @Override
    public long getNextIntervalMs() {
        return delegate != null ? delegate.getNextIntervalMs() : 0L;
    }

    private AwakeningIntervalStrategy(@Nullable IntervalStrategy delegate) {
        this.delegate = delegate;
    }
}
