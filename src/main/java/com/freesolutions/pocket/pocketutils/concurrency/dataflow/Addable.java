package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Interface to make implementer class able to add some items inside.
 *
 * Implementations must guarantee that {@link #add} method does not contain
 * any side effects and its call is cheap operation.
 *
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Addable<T> {

    void add(@Nonnull T item);
}
