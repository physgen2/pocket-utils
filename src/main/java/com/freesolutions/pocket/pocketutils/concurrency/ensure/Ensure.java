package com.freesolutions.pocket.pocketutils.concurrency.ensure;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext.NullableReadableResultDelegatesExt;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext.NullableReadableResultSubmitsExt;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;

/**
 * @author Stanislau Mirzayeu
 */
public interface Ensure extends
        NullableReadableResult<Void>,
        NullableReadableResultDelegatesExt<Void, Ensure>,
        NullableReadableResultSubmitsExt<Void, Ensure>
{
    //nothing
}
