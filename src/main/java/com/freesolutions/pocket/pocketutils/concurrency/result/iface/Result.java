package com.freesolutions.pocket.pocketutils.concurrency.result.iface;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;
import com.freesolutions.pocket.pocketutils.concurrency.sync.syncable.Syncable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability unknown, see derived interfaces, ok
 */
public interface Result<T> extends Syncable<Void> {

    @Nonnull
    Status status();

    T value();

    @Nullable
    default T valueOrNull() {
        return status().present ? value() : null;
    }

    //nullability unknown, calling code can perform checks according to it logic if necessary, ok
    default T orElse(T defaultValue) {
        return status().present ? value() : defaultValue;
    }

    default T syncedValue() throws Exception {
        Status s = status();
        switch (s) {
            case PENDING:
                throw new IllegalStateException("not done");
            case PRESENT:
                return value();
            case ERROR:
                throw new ExecutionException(checkNotNull(error()));
            case CANCELLED:
                throw new CancellationException();
            case TIMED_OUT:
                throw new TimeoutException();
            case INTERRUPTED:
                //NOTE:
                // Don't throw <InterruptedException>, because current thread wasn't interrupted.
                // Actually we have cancellation of execution, because thread, where execution was
                // performed, was interrupted, while current thread is not interrupted.
                throw new CancellationException("interrupted");
            default:
                throw new IllegalStateException("unknown status: " + s);
        }
    }

    @Nullable
    Throwable error();

    void whenDone(@Nonnull Pipe pipe, @Nonnull Action action);

    void purgeCallbacks();
}
