package com.freesolutions.pocket.pocketutils.concurrency.threads;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public class ThreadBucket implements Startable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadBucket.class);

    @Nonnull
    private final Object lock = new Object();

    @Nonnull
    private final String name;
    private final int threadCount;
    @Nonnull
    private final Thread[] threads;

    private volatile boolean started = false;
    private volatile boolean stopped = false;
    private volatile int actualCount = 0;

    public ThreadBucket(@Nonnull String name, int threadCount) {
        checkArgument(threadCount > 0);
        this.name = name;
        this.threadCount = threadCount;
        this.threads = new Thread[threadCount];
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isStopped() {
        return stopped;
    }

    public int remainingToSubmit() {
        //NOTE: don't care about race conditions, result will be ok
        if (started || stopped) {
            return 0;
        }
        int result = threadCount - actualCount;
        checkState(result >= 0);
        return result;
    }

    public void submit(@Nonnull Runnable runnable) {
        int submitted = submitIfPossible(runnable, 1, true);
        checkState(submitted > 0, "Can't submit thread within bucket \"" + name + "\"");
    }

    public void submit(@Nonnull Runnable runnable, int count) {
        int submitted = submitIfPossible(runnable, count, true);
        checkState(submitted > 0, "Can't submit thread within bucket \"" + name + "\"");
    }

    public boolean submitIfPossible(@Nonnull Runnable runnable) {
        return submitIfPossible(runnable, 1, true) != 0;
    }

    public int submitIfPossible(@Nonnull Runnable runnable, int count, boolean all) {
        synchronized (lock) {
            checkArgument(count >= 0, "Negative number of threads to submit: " + count);
            checkState(!started, "Can't submit " + count + " threads within started bucket \"" + name + "\"");
            checkState(!stopped, "Can't submit " + count + " threads within stopped bucket \"" + name + "\"");
            int result = 0;
            if (count > 0) {
                checkState(actualCount <= threadCount);
                if (actualCount < threadCount) {
                    result = Math.min(count, threadCount - actualCount);
                    if (all && result < count) {
                        result = 0;
                    } else {
                        this.actualCount += result;
                    }
                }
                for (int i = actualCount - result; i < actualCount; i++) {
                    threads[i] = new Thread(runnable, name + (threadCount > 1 ? ("-" + (i + 1)) : ""));
                }
            }
            return result;
        }
    }

    public void submitAll(@Nonnull Runnable runnable) {
        synchronized (lock) {
            int submitted = submitIfPossible(runnable, threadCount - actualCount, true);
            checkState(submitted > 0, "Can't submit threads within bucket \"" + name + "\"");
            checkState(actualCount == threadCount);
        }
    }

    @Override
    public void start() throws Exception {
        synchronized (lock) {
            checkState(!started, "Can't start bucket \"" + name + "\", because it was already started");
            checkState(!stopped, "Can't start bucket \"" + name + "\", because it was already stopped");
            this.started = true;
            try {
                for (int i = 0; i < actualCount; i++) {
                    threads[i].start();
                }
            } catch (Throwable t) {
                stop();
                throw new IllegalStateException("Can't start bucket \"" + name + "\"", t);
            }
        }
    }

    @Override
    public void stop() {
        synchronized (lock) {
            if (!stopped) {
                this.stopped = true;
                for (int i = 0; i < actualCount; i++) {
                    try {
                        threads[i].interrupt();
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                    }
                }
                for (int i = 0; i < actualCount; i++) {
                    try {
                        threads[i].join(TimeUnit.SECONDS.toMillis(10L));
                    } catch (InterruptedException ignored) {
                        Thread.currentThread().interrupt();
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                    }
                }
            }
        }
    }
}
