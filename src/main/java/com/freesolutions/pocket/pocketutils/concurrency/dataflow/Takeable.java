package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NonnullSyncResult;
import com.freesolutions.pocket.pocketutils.concurrency.sync.syncable.NonnullSyncable;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Takeable<T> extends NonnullSyncable<T> {

    @Nullable
    T take(long timeoutMs) throws InterruptedException;

    @Nonnull
    T take() throws InterruptedException;

    @Nonnull
    default NonnullSyncResult<T> sync(@Nonnull SyncQuery syncQuery) {
        Long timeoutMs = syncQuery.toTimeoutMs();
        try {
            T v = timeoutMs != null ?
                take(timeoutMs) :
                take();//infinite
            if (v != null) {
                return NonnullSyncResult.ofOk(v);
            } else {
                return NonnullSyncResult.ofTimedOut();
            }
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return NonnullSyncResult.ofInterrupted();
        } catch (Throwable t) {
            LoggerFactory.getLogger(getClass()).error("impossible", t);
            return NonnullSyncResult.ofRejected();
        }
    }
}
