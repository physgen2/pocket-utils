package com.freesolutions.pocket.pocketutils.concurrency.queue;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullImmutableResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicInteger;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class SyncableLinkedNLQueue<T> extends LinkedNLQueue<T> implements SyncableNLQueue<T> {

    private final NLQueue<NonnullResult<T>> waiters = new LinkedNLQueue<>();
    private final AtomicInteger balance = new AtomicInteger();

    @Override
    public void add(@Nonnull T value) {
        checkNotNull(value);//check to avoid further undefined behavior in case of null
        while (true) {
            int b = balance.incrementAndGet();
            if (b > 0) {
                super.add(value);
                return;
            }
            //waiter must exist
            while (true) {
                NonnullResult<T> waiter = waiters.poll();
                if (waiter != null) {
                    if (waiter.present(value)) {//try to directly setup value to waiter
                        return;
                    }
                    break;//skip waiter due to it already done, try all again, ok
                }
                Thread.yield();
            }
            Thread.yield();
        }
    }

    @Override
    public int size() {
        return super.size();
    }

    @Nullable
    @Override
    public T poll() {
        while (true) {
            int b = balance.get();
            if (b <= 0) {
                return null;//queue is empty, ok
            }
            if (balance.compareAndSet(b, b - 1)) {
                //value must exist
                while (true) {
                    T result = super.poll();
                    if (result != null) {
                        return result;
                    }
                    Thread.yield();
                }
            }
            Thread.yield();
        }
    }

    @Nullable
    @Override
    public T peek() {
        return super.peek();
    }

    @Nonnull
    @Override
    public NonnullResult<T> takeR() {
        int b = balance.decrementAndGet();
        if (b < 0) {
            //queue is empty, create waiter, ok
            NonnullResult<T> result = NonnullFuture.of();
            waiters.add(result);
            return result;
        }
        //value must exist
        while (true) {
            T v = super.poll();
            if (v != null) {
                return NonnullImmutableResult.ofPresent(v);
            }
            Thread.yield();
        }
    }
}
