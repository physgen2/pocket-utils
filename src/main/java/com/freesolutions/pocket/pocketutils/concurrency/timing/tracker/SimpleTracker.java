package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.concurrency.loop.Threads;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Comparator;
import java.util.PriorityQueue;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class SimpleTracker implements Tracker, Startable {

    @Nonnull
    private final Threads worker;
    @Nonnull
    private final PriorityQueue<Query> queries = new PriorityQueue<>(Comparator.comparingLong(Query::uniDeadlineTs));

    public SimpleTracker(@Nonnull String name) {
        this.worker = new Threads(
            name,
            1, //threadCount
            true, //ignoreInterruption
            this::handle
        );
    }

    @Override
    public void start() throws Exception {
        worker.start();
    }

    @Override
    public void stop() {
        worker.stop();
        synchronized (queries) {
            queries.clear();
        }
    }

    public boolean isStarted() {
        return worker.isStarted();
    }

    @Override
    public void query(@Nonnull Query query) {
        if (query.isExpired()) {//fast check
            query.apply();
            return;
        }
        synchronized (queries) {
            queries.offer(query);
            queries.notifyAll();//notify that queue is changed
        }
    }

    private void handle() throws Exception {
        Query query;
        synchronized (queries) {
            while (true) {
                query = waitAndPeekNearestPendingQuery();
                if (query.isExpired()) {
                    break;
                }
                queries.wait(ClockUtils.toTimeout(query.uniDeadlineTs(), ClockUtils.UNI_CLOCK));
            }
            removeFromQueueHead(query);
        }
        query.apply();
    }

    @GuardedBy("queries")
    @Nonnull
    private Query waitAndPeekNearestPendingQuery() throws InterruptedException {
        while (true) {
            Query query = queries.peek();
            if (query == null) {
                queries.wait();
                continue;
            }
            if (!query.isPending()) {
                removeFromQueueHead(query);
                continue;
            }
            return query;
        }
    }

    @GuardedBy("queries")
    private void removeFromQueueHead(@Nonnull Query query) {
        Query removedQuery = checkNotNull(queries.poll());
        if (removedQuery != query) {
            queries.offer(removedQuery);//put back
        }
    }
}
