package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Transforms;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.ToNonnullDataHandler;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NonnullWritableResultTransformsExt<T, RESULT extends NonnullWritableResult<T>> extends NonnullWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNR2R(Pipe pipe, NullableReadableResult<S> source, NonnullDataHandler<NullableReadableResult<S>, NonnullReadableResult<T>> handler) {
        Transforms.transformNR2R(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformR2R(Pipe pipe, NonnullReadableResult<S> source, NonnullDataHandler<NonnullReadableResult<S>, NonnullReadableResult<T>> handler) {
        Transforms.transformR2R(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNR2X(Pipe pipe, NullableReadableResult<S> source, NonnullDataHandler<NullableReadableResult<S>, T> handler) {
        Transforms.transformNR2X(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformR2X(Pipe pipe, NonnullReadableResult<S> source, NonnullDataHandler<NonnullReadableResult<S>, T> handler) {
        Transforms.transformR2X(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNX2R(Pipe pipe, NullableReadableResult<S> source, ToNonnullDataHandler<S, NonnullReadableResult<T>> handler) {
        Transforms.transformNX2R(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformX2R(Pipe pipe, NonnullReadableResult<S> source, NonnullDataHandler<S, NonnullReadableResult<T>> handler) {
        Transforms.transformX2R(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNX2X(Pipe pipe, NullableReadableResult<S> source, ToNonnullDataHandler<S, T> handler) {
        Transforms.transformNX2X(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformX2X(Pipe pipe, NonnullReadableResult<S> source, NonnullDataHandler<S, T> handler) {
        Transforms.transformX2X(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
