package com.freesolutions.pocket.pocketutils.concurrency.result.util;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.actions.DataHandler;
import com.freesolutions.pocket.pocketutils.actions.FromNonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.ToNonnullDataHandler;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class Transforms {

    //---------------------------------------------------------------------------------------------

    public static <T, R> void transformNR2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, NonnullDataHandler<NullableReadableResult<T>, NullableReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNR2NR(pipe, source, target, handler));
    }

    public static <T, R> void transformNR2R(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NullableReadableResult<T>, NonnullReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNR2R(pipe, source, target, handler));
    }

    public static <T, R> void transformR2NR(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, NonnullDataHandler<NonnullReadableResult<T>, NullableReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateR2NR(pipe, source, target, handler));
    }

    public static <T, R> void transformR2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NonnullReadableResult<T>, NonnullReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateR2R(pipe, source, target, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void transformNR2NX(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, FromNonnullDataHandler<NullableReadableResult<T>, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNR2NX(pipe, source, target, handler));
    }

    public static <T, R> void transformNR2X(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NullableReadableResult<T>, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNR2X(pipe, source, target, handler));
    }

    public static <T, R> void transformR2NX(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, FromNonnullDataHandler<NonnullReadableResult<T>, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateR2NX(pipe, source, target, handler));
    }

    public static <T, R> void transformR2X(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NonnullReadableResult<T>, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateR2X(pipe, source, target, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void transformNX2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, ToNonnullDataHandler<T, NullableReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNX2NR(pipe, source, target, handler));
    }

    public static <T, R> void transformNX2R(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, ToNonnullDataHandler<T, NonnullReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNX2R(pipe, source, target, handler));
    }

    public static <T, R> void transformX2NR(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, NonnullDataHandler<T, NullableReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateX2NR(pipe, source, target, handler));
    }

    public static <T, R> void transformX2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<T, NonnullReadableResult<R>> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateX2R(pipe, source, target, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void transformNX2NX(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, DataHandler<T, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNX2NX(pipe, source, target, handler));
    }

    public static <T, R> void transformNX2X(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, ToNonnullDataHandler<T, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateNX2X(pipe, source, target, handler));
    }

    public static <T, R> void transformX2NX(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, FromNonnullDataHandler<T, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateX2NX(pipe, source, target, handler));
    }

    public static <T, R> void transformX2X(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<T, R> handler) {
        source.whenDone(pipe, () -> Handlers.handleAndDelegateX2X(pipe, source, target, handler));
    }

    //---------------------------------------------------------------------------------------------
}
