package com.freesolutions.pocket.pocketutils.concurrency.sync;

import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public enum SyncStatus {

    OK          (true,  false, false, false),
    TIMED_OUT   (false, true,  false, false),
    INTERRUPTED (false, false, true,  false),
    REJECTED    (false, false, false, true);

    public final boolean ok;
    public final boolean timedOut;
    public final boolean interrupted;
    public final boolean rejected;

    SyncStatus(
            boolean ok,
            boolean timedOut,
            boolean interrupted,
            boolean rejected
    ) {
        this.ok = ok;
        this.timedOut = timedOut;
        this.interrupted = interrupted;
        this.rejected = rejected;
    }
}
