package com.freesolutions.pocket.pocketutils.concurrency.topic.iface;

import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Pollable;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.ResultTakeable;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface ManualConsumer<T> extends Consumer<T>, Pollable<T>, ResultTakeable<T> {
    //empty
}
