package com.freesolutions.pocket.pocketutils.concurrency.timing;

import com.freesolutions.pocket.pocketutils.actions.*;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Cancellable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.GlobalTracker;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Query;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Tracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Clock;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class Monitor {
    private static final Logger LOGGER = LoggerFactory.getLogger(Monitor.class);

    @ThreadSafe
    public static class Sync {

        public static void waitTimeout(long timeoutMs) throws InterruptedException {
            waitDeadline(
                ClockUtils.toDeadlineTs(timeoutMs, ClockUtils.UNI_CLOCK),
                ClockUtils.UNI_CLOCK
            );
        }

        public static <M> boolean waitTimeout(
            @Nullable Long timeoutMs,
            @Nonnull M monitor,
            @Nonnull NonnullDataHandler<M, Boolean> condition
        ) throws InterruptedException {
            return waitDeadline(
                timeoutMs != null ? ClockUtils.toDeadlineTs(timeoutMs, ClockUtils.UTC) : null,
                ClockUtils.UTC,
                monitor,
                condition
            );
        }

        @Nullable
        public static <M, R> R waitTimeoutR(
            @Nullable Long timeoutMs,
            @Nonnull M monitor,
            @Nonnull FromNonnullDataHandler<M, R> handler
        ) throws InterruptedException {
            return waitDeadlineR(
                timeoutMs != null ? ClockUtils.toDeadlineTs(timeoutMs, ClockUtils.UTC) : null,
                ClockUtils.UTC,
                monitor,
                handler
            );
        }

        public static void waitDeadline(long deadlineTs, @Nonnull Clock clock) throws InterruptedException {
            long nowTs = clock.millis();
            while (nowTs < deadlineTs) {
                Thread.sleep(deadlineTs - nowTs);
                nowTs = clock.millis();
            }
        }

        public static <M> boolean waitDeadline(
            @Nullable Long deadlineTs,
            @Nonnull Clock clock,
            @Nonnull M monitor,
            @Nonnull NonnullDataHandler<M, Boolean> condition
        ) throws InterruptedException {
            if (deadlineTs == null) {
                while (true) {
                    boolean cond = false;
                    try {
                        cond = condition.handle(monitor);
                    } catch (InterruptedException ie) {
                        throw ie;
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                        //treat exception as false, ok
                    }
                    if (cond) {
                        return true;
                    }
                    monitor.wait();
                }
            } else {
                while (true) {
                    long nowTs = clock.millis();
                    if (nowTs >= deadlineTs) {
                        return false;
                    }
                    boolean cond = false;
                    try {
                        cond = condition.handle(monitor);
                    } catch (InterruptedException ie) {
                        throw ie;
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                        //treat exception as false
                    }
                    if (cond) {
                        return true;
                    }
                    //one more check of deadline before we continue waiting
                    nowTs = clock.millis();
                    if (nowTs >= deadlineTs) {
                        return false;
                    }
                    monitor.wait(deadlineTs - nowTs);//ok, passed timeout is strict more than zero
                }
            }
        }

        @Nullable
        public static <M, R> R waitDeadlineR(
            @Nullable Long deadlineTs,
            @Nonnull Clock clock,
            @Nonnull M monitor,
            @Nonnull FromNonnullDataHandler<M, R> handler
        ) throws InterruptedException {
            if (deadlineTs == null) {
                while (true) {
                    R result = null;
                    try {
                        result = handler.handle(monitor);
                    } catch (InterruptedException ie) {
                        throw ie;
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                        //treat exception as null result
                    }
                    if (result != null) {
                        return result;
                    }
                    monitor.wait();
                }
            } else {
                while (true) {
                    long nowTs = clock.millis();
                    if (nowTs >= deadlineTs) {
                        return null;
                    }
                    R result = null;
                    try {
                        result = handler.handle(monitor);
                    } catch (InterruptedException ie) {
                        throw ie;
                    } catch (Throwable t) {
                        LOGGER.error("", t);
                        //treat exception as null result
                    }
                    if (result != null) {
                        return result;
                    }
                    //one more check of deadline before we continue waiting
                    nowTs = clock.millis();
                    if (nowTs >= deadlineTs) {
                        return null;
                    }
                    monitor.wait(deadlineTs - nowTs);//ok, passed timeout is strict more than zero
                }
            }
        }
    }

    @ThreadSafe
    public static class Async implements Cancellable {

        public static void immediateLaunch(
            @Nonnull Pipe pipe,
            @Nonnull Async monitor,
            @Nonnull Action action
        ) {
            monitor.add(
                new Waiter<>( //immediate waiter
                    monitor,
                    pipe,
                    action
                ),
                null //tracker
            );
        }

        public static void waitTimeout(
            @Nonnull Pipe pipe,
            long timeoutMs,
            @Nonnull Async monitor,
            @Nonnull Action action
        ) {
            waitTimeout(
                pipe,
                null, //tracker
                timeoutMs,
                monitor,
                action
            );
        }

        public static void waitTimeout(
            @Nonnull Pipe pipe,
            @Nullable Tracker tracker,
            long timeoutMs,
            @Nonnull Async monitor,
            @Nonnull Action action
        ) {
            waitDeadline(
                pipe,
                tracker,
                ClockUtils.toDeadlineTs(timeoutMs, ClockUtils.UNI_CLOCK),
                ClockUtils.UNI_CLOCK,
                monitor,
                action
            );
        }

        public static void waitTimeout(
            @Nonnull Pipe pipe,
            @Nullable Long timeoutMs,
            @Nonnull Async monitor,
            @Nonnull NonnullHandler<Boolean> condition,
            @Nonnull NonnullDataAction<Boolean> action
        ) {
            waitTimeout(
                pipe,
                null, //tracker
                timeoutMs,
                monitor,
                condition,
                action
            );
        }

        public static void waitTimeout(
            @Nonnull Pipe pipe,
            @Nullable Tracker tracker,
            @Nullable Long timeoutMs,
            @Nonnull Async monitor,
            @Nonnull NonnullHandler<Boolean> condition,
            @Nonnull NonnullDataAction<Boolean> action
        ) {
            waitDeadline(
                pipe,
                tracker,
                timeoutMs != null ? ClockUtils.toDeadlineTs(timeoutMs, ClockUtils.UTC) : null,
                ClockUtils.UTC,
                monitor,
                condition,
                action
            );
        }

        public static <R> void waitTimeoutR(
            @Nonnull Pipe pipe,
            @Nullable Long timeoutMs,
            @Nonnull Async monitor,
            @Nonnull Handler<R> handler,
            @Nonnull DataAction<R> action
        ) {
            waitTimeoutR(
                pipe,
                null, //tracker
                timeoutMs,
                monitor,
                handler,
                action
            );
        }

        public static <R> void waitTimeoutR(
            @Nonnull Pipe pipe,
            @Nullable Tracker tracker,
            @Nullable Long timeoutMs,
            @Nonnull Async monitor,
            @Nonnull Handler<R> handler,
            @Nonnull DataAction<R> action
        ) {
            waitDeadlineR(
                pipe,
                tracker,
                timeoutMs != null ? ClockUtils.toDeadlineTs(timeoutMs, ClockUtils.UTC) : null,
                ClockUtils.UTC,
                monitor,
                handler,
                action
            );
        }

        public static void waitDeadline(
            @Nonnull Pipe pipe,
            @Nullable Tracker tracker,
            long deadlineTs,
            @Nonnull Clock clock,
            @Nonnull Async monitor,
            @Nonnull Action action
        ) {
            monitor.add(
                new Waiter<>(
                    monitor,
                    pipe,
                    deadlineTs,
                    clock,
                    null, //handler
                    (@Nullable Boolean ignored) -> action.execute() //action
                ),
                tracker
            );
        }

        public static void waitDeadline(
            @Nonnull Pipe pipe,
            @Nullable Tracker tracker,
            @Nullable Long deadlineTs,
            @Nonnull Clock clock,
            @Nonnull Async monitor,
            @Nonnull NonnullHandler<Boolean> condition,
            @Nonnull NonnullDataAction<Boolean> action
        ) {
            monitor.add(
                new Waiter<>(
                    monitor,
                    pipe,
                    deadlineTs,
                    clock,
                    () -> condition.handle() ? true : null, //handler
                    (@Nullable Boolean r) -> action.execute(r != null) //action
                ),
                tracker
            );
        }

        public static <R> void waitDeadlineR(
            @Nonnull Pipe pipe,
            @Nullable Tracker tracker,
            @Nullable Long deadlineTs,
            @Nonnull Clock clock,
            @Nonnull Async monitor,
            @Nonnull Handler<R> handler,
            @Nonnull DataAction<R> action
        ) {
            monitor.add(
                new Waiter<>(
                    monitor,
                    pipe,
                    deadlineTs,
                    clock,
                    handler,
                    action
                ),
                tracker
            );
        }

        public boolean isCancelled() {
            return cancelled;//volatile
        }

        public synchronized void trigger() {
            for (Waiter<?> waiter = head; waiter != null; waiter = waiter.next) {
                waiter.trigger();
            }
        }

        @Override
        public boolean isSilent() {
            return cancelled && busy == 0;//volatile
        }

        @Override
        public synchronized boolean cancel() {

            if (cancelled) {
                return false;
            }

            this.cancelled = true;

            for (Waiter<?> waiter = head; waiter != null; waiter = waiter.next) {
                waiter.cancel();
            }

            if (busy == 0) {
                notifyAll();
            }

            return true;
        }

        @Override
        public synchronized void join() throws InterruptedException {
            ClockUtils.waitTimeout(null, this, Cancellable::isSilent);
        }

        //---------------------------------------------------------------------------------------------

        //state: guarded by monitor
        private volatile boolean cancelled;
        private volatile int busy;

        @GuardedBy("monitor")
        @Nullable
        private Waiter<?> head;

        private synchronized void add(@Nonnull Waiter<?> waiter, @Nullable Tracker tracker) {

            if (cancelled) {
                return;
            }

            //always add into chain
            Waiter<?> next = head;
            waiter.next = next;
            this.head = waiter;
            if (next != null) {
                next.prev = waiter;
            }

            waiter.launch(tracker);
        }

        @GuardedBy("monitor")
        private void remove(@Nonnull Waiter<?> waiter) {

            //do remove
            Waiter<?> prev = waiter.prev;
            Waiter<?> next = waiter.next;
            if (prev != null) {
                prev.next = next;
            } else {
                checkState(waiter == head);//comparison by reference
                this.head = next;
            }
            if (next != null) {
                next.prev = prev;
            }

            //help gc
            waiter.prev = null;
            waiter.next = null;
        }

        @GuardedBy("monitor")
        private boolean activate(@Nonnull Waiter<?> waiter) {
            if (waiter.active) {
                return false;
            }
            waiter.active = true;
            busy++;
            return true;
        }

        @GuardedBy("monitor")
        private void deactivate(@Nonnull Waiter<?> waiter) {
            if (!waiter.active) {
                return;
            }
            waiter.active = false;
            if (--busy == 0) {
                if (cancelled) {
                    notifyAll();
                }
            }
        }

        @ThreadSafe
        private static class Waiter<R> {

            private static final int PENDING = 0;
            private static final int EXPIRED = 1;
            private static final int FINISHED = 2;

            @Nonnull
            public final Async monitor;

            @GuardedBy("monitor")
            @Nullable
            public Waiter<?> prev;

            @GuardedBy("monitor")
            @Nullable
            public Waiter<?> next;

            //config
            @Nonnull
            private final Pipe pipe;
            private final boolean immediate;
            @Nullable
            private final Query expiration;
            @Nullable
            private final Handler<R> handler;
            @Nonnull
            private final DataAction<R> action;

            //state: guarded by monitor
            private int status;//initially PENDING
            private int triggered;//0 - no trigger requests
            private boolean active;//initially !active

            //NOTE: immediate waiter
            public Waiter(
                @Nonnull Async monitor,
                @Nonnull Pipe pipe,
                @Nonnull Action action
            ) {
                this.monitor = monitor;
                this.pipe = pipe;
                this.immediate = true;
                this.expiration = null;
                this.handler = null;
                this.action = (@Nullable R ignored) -> action.execute();
            }

            //NOTE: normal waiter
            public Waiter(
                @Nonnull Async monitor,
                @Nonnull Pipe pipe,
                @Nullable Long deadlineTs,
                @Nonnull Clock clock,
                @Nullable Handler<R> handler,
                @Nonnull DataAction<R> action
            ) {
                this.monitor = monitor;
                this.pipe = pipe;
                this.immediate = false;
                this.expiration = deadlineTs != null ? Query.of(pipe, deadlineTs, clock) : null;
                this.handler = handler;
                this.action = action;
            }

            @GuardedBy("monitor")
            public void launch(@Nullable Tracker tracker) {
                checkState((expiration == null && handler == null) || !immediate);
                if (immediate || (expiration != null && expiration.isExpired())) {
                    //immediate logic
                    this.status = Waiter.EXPIRED;
                    this.triggered = 1;
                    pipe.add(this::doTrigger);
                } else {

                    //launch expiration
                    if (expiration != null) {
                        expiration.useAction(this::doExpire);
                        if (tracker == null) {
                            tracker = GlobalTracker.instance();
                        }
                        tracker.query(expiration);
                    }

                    //initial trigger
                    trigger();
                }
            }

            @GuardedBy("monitor")
            public void trigger() {

                if (status != PENDING) {
                    return;
                }

                if (handler == null) {
                    return;
                }

                if (++triggered > 1) {
                    return;
                }

                pipe.add(this::doTrigger);
            }

            @GuardedBy("monitor")
            public void cancel() {

                if (status == FINISHED) {
                    return;
                }

                this.status = FINISHED;
                monitor.remove(this);

                if (expiration != null) {
                    expiration.cancel();
                }
            }

            //NOTE: called under pipe
            private void doExpire() throws Exception {

                synchronized (monitor) {
                    if (status != PENDING) {
                        return;
                    }
                    if (!monitor.activate(this)) {
                        //trigger activity is present, just mark EXPIRED
                        this.status = EXPIRED;
                        return;
                    }
                    this.status = FINISHED;
                    monitor.remove(this);
                    //NOTE: no need to cancel expiration, because already expired
                }

                //NOTE: here only we have last activity, other activities became impossible after this moment

                //apply result
                try {
                    action.execute(null);
                } finally {
                    synchronized (monitor) {
                        monitor.deactivate(this);
                    }
                }
            }

            //NOTE: called under pipe
            private void doTrigger() throws Exception {

                boolean apply = false;
                R result = null;
                boolean expired = false;
                boolean handled = false;
                while (true) {

                    synchronized (monitor) {
                        if (status == FINISHED) {
                            return;
                        }
                        if (status == EXPIRED || result != null || expired) {
                            this.status = FINISHED;
                            monitor.remove(this);
                            monitor.activate(this);
                            apply = true;
                            break;
                        }
                        checkState(status == PENDING);
                        if (handled) {
                            if (--triggered == 0) {
                                monitor.deactivate(this);
                                break;
                            }
                        }
                        this.triggered = 1;
                        monitor.activate(this);
                    }

                    result = handler != null ? Handler.executeSafe(handler, null) : null;//treat exception as null result
                    expired = expiration != null && expiration.isExpired();
                    handled = true;
                }

                //apply result if necessary
                if (apply) {
                    //NOTE: here status == FINISHED
                    try {
                        action.execute(result);
                    } finally {
                        synchronized (monitor) {
                            monitor.deactivate(this);
                        }
                    }
                }
            }
        }
    }
}
