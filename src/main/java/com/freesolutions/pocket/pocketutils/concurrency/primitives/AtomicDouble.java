package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

/**
 * @author Stanislau Mirzayeu
 */
public class AtomicDouble extends Number implements Serializable {

    private static final AtomicLongFieldUpdater<AtomicDouble> VALUE_UPDATER =
        AtomicLongFieldUpdater.newUpdater(AtomicDouble.class, "value");

    private volatile long value;

    public AtomicDouble() {
        //empty
    }

    public AtomicDouble(double initialValue) {
        this.value = Double.doubleToRawLongBits(initialValue);
    }

    public double get() {
        return Double.longBitsToDouble(this.value);
    }

    public void set(double newValue) {
        this.value = Double.doubleToRawLongBits(newValue);
    }

    public void lazySet(double newValue) {
        VALUE_UPDATER.lazySet(
            this,
            Double.doubleToRawLongBits(newValue)
        );
    }

    public double getAndSet(double newValue) {
        return Double.longBitsToDouble(
            VALUE_UPDATER.getAndSet(
                this,
                Double.doubleToRawLongBits(newValue)
            )
        );
    }

    public boolean compareAndSet(double expect, double update) {
        return VALUE_UPDATER.compareAndSet(
            this,
            Double.doubleToRawLongBits(expect),
            Double.doubleToRawLongBits(update)
        );
    }

    public boolean weakCompareAndSet(double expect, double update) {
        return VALUE_UPDATER.weakCompareAndSet(
            this,
            Double.doubleToRawLongBits(expect),
            Double.doubleToRawLongBits(update)
        );
    }

    public double getAndAdd(double delta) {
        double cur;
        long lCur;
        long next;
        do {
            lCur = value;
            cur = Double.longBitsToDouble(lCur);
            next = Double.doubleToRawLongBits(cur + delta);
        } while (!VALUE_UPDATER.compareAndSet(this, lCur, next));
        return cur;
    }

    public double addAndGet(double delta) {
        long lCur;
        double next;
        long lNext;
        do {
            lCur = value;
            next = Double.longBitsToDouble(lCur) + delta;
            lNext = Double.doubleToRawLongBits(next);
        } while (!VALUE_UPDATER.compareAndSet(this, lCur, lNext));
        return next;
    }

    public int intValue() {
        return (int) get();
    }

    public long longValue() {
        return (long) get();
    }

    public float floatValue() {
        return (float) get();
    }

    public double doubleValue() {
        return get();
    }

    @Override
    public String toString() {
        return Double.toString(get());
    }
}
