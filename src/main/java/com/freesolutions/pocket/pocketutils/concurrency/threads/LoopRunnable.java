package com.freesolutions.pocket.pocketutils.concurrency.threads;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public class LoopRunnable implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoopRunnable.class);

    public interface IsStopRequiredPredicate {
        boolean isStopRequired();
    }

    @Nonnull
    private final IntervalStrategy intervalStrategy;
    @Nonnull
    private final IsStopRequiredPredicate isStopRequiredPredicate;
    @Nonnull
    private final Action action;
    @Nonnull
    private final Action preAction;
    @Nonnull
    private final Action postAction;

    public LoopRunnable(
            @Nonnull IntervalStrategy intervalStrategy,
            @Nonnull IsStopRequiredPredicate isStopRequiredPredicate,
            @Nonnull Action action
    ) {
        this(
                intervalStrategy,
                isStopRequiredPredicate,
                action,
                Action.IDENTITY,
                Action.IDENTITY
        );
    }

    public LoopRunnable(
            @Nonnull IntervalStrategy intervalStrategy,
            @Nonnull IsStopRequiredPredicate isStopRequiredPredicate,
            @Nonnull Action action,
            @Nonnull Action preAction,
            @Nonnull Action postAction
    ) {
        this.intervalStrategy = intervalStrategy;
        this.isStopRequiredPredicate = isStopRequiredPredicate;
        this.action = action;
        this.preAction = preAction;
        this.postAction = postAction;
    }

    @Override
    public void run() {
        boolean started = false;
        try {
            preAction.execute();
            started = true;
        } catch (Throwable t) {
            LOGGER.error("", t);
        }
        if (started) {
            try {
                doRun();
            } catch (Throwable t) {
                LOGGER.error("", t);
            } finally {
                try {
                    Thread.interrupted();//clear possible interruption
                    postAction.execute();
                } catch (Throwable t) {
                    LOGGER.error("", t);
                }
            }
        }
    }

    private void doRun() {
        boolean first = true;
        boolean error = false;
        long invokeTs = ClockUtils.UNI_CLOCK.millis();
        while (!isStopRequiredPredicate.isStopRequired() && !Thread.currentThread().isInterrupted()) {
            try {
                if (!error) {
                    if (!first) {
                        try {
                            invokeTs += intervalStrategy.getNextIntervalMs();
                        } catch (Throwable t) {
                            LOGGER.error("Exception while getting next interval, wait 100ms and repeat", t);
                            ClockUtils.waitTimeout(100L);
                            continue;
                        }
                    } else {
                        first = false;
                    }
                    waitDeadline(invokeTs, intervalStrategy);
                }
                invokeTs = ClockUtils.UNI_CLOCK.millis();
                action.execute();
                error = false;
            } catch (InterruptedException ignored) {
                break;
            } catch (Throwable t) {
                error = true;
                LOGGER.error("Exception in loop-runnable body, will be immediately repeated", t);
            }
        }
    }

    private void waitDeadline(long deadlineTs, @Nonnull IntervalStrategy intervalStrategy) throws Exception {
        if (intervalStrategy instanceof MonitoredIntervalStrategy) {
            MonitoredIntervalStrategy mis = (MonitoredIntervalStrategy) intervalStrategy;
            synchronized (mis) {
                ClockUtils.waitDeadline(
                        mis.isIntervalsEnabled() ? deadlineTs : null,
                        ClockUtils.UNI_CLOCK,
                        mis,
                        ignored -> mis.isAwakeRequired()
                );
            }
        } else {
            ClockUtils.waitDeadline(deadlineTs, ClockUtils.UNI_CLOCK);
        }
    }
}
