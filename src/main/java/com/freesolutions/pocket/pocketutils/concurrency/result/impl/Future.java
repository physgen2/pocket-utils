package com.freesolutions.pocket.pocketutils.concurrency.result.impl;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.WritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.primitives.Arb;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NullableSyncResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicLong;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability unknown, see derived classes, ok
 */
@ThreadSafe
public abstract class Future<T> extends Arb<Object> implements WritableResult<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Future.class);

    public static final AtomicLong GLOBAL_COUNTER = new AtomicLong(0L);

    private static final Object LOCKED = new Object();

    @Immutable
    private static class Callback {
        @Nullable
        public final Callback prev;
        @Nonnull
        public final Pipe pipe;
        @Nonnull
        public final Action action;

        public Callback(@Nullable Callback prev, @Nonnull Pipe pipe, @Nonnull Action action) {
            this.prev = prev;
            this.pipe = pipe;
            this.action = action;
        }

        public void run() {
            pipe.add(action);
        }
    }

    //@Nullable
    private volatile Status status;//assume initial <null> means <PENDING>

    @Nonnull
    @Override
    public NullableSyncResult<Void> sync(@Nonnull SyncQuery syncQuery) {

        //fast check
        if (status().done) {
            return NullableSyncResult.ofOkNull();
        }

        //timed out if query is immediate
        if (syncQuery.immediate) {
            return NullableSyncResult.ofTimedOut();
        }

        //use <syncQuery> as monitor, because this object identifies current query, and waiting is related to this query only.
        whenDone(Pipe.DIRECT, () -> {
            synchronized (syncQuery) {
                syncQuery.notifyAll();
            }
        });

        //do wait
        synchronized (syncQuery) {
            try {
                return ClockUtils.waitDeadline(
                        syncQuery.deadlineTs,
                        syncQuery.clock,
                        syncQuery,
                        ignored -> status().done
                ) ? NullableSyncResult.ofOkNull() : NullableSyncResult.ofTimedOut();
            } catch (InterruptedException ignored) {
                Thread.currentThread().interrupt();
                return NullableSyncResult.ofInterrupted();
            } catch (Throwable t) {
                LOGGER.error("impossible", t);
                return NullableSyncResult.ofRejected();
            }
        }
    }

    @Nonnull
    @Override
    public Status status() {
        return status != null ? status : Status.PENDING;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T value() {
        checkState(status().present, "not present");
        return valueNotLocked();
    }

    @Nullable
    @Override
    public Throwable error() {
        return status().error ? valueNotLocked() : null;
    }

    @Override
    public void whenDone(@Nonnull Pipe pipe, @Nonnull Action action) {
        boolean direct = status().done;
        if (!direct) {
            Object value;
            while (true) {//lock-loop
                value = arbGet();
                if (value != LOCKED && arbCompareAndSet(value, LOCKED)) {
                    break;
                }
                Thread.yield();
            }
            try {
                direct = status().done;
                if (!direct) {//double-check
                    value = new Callback((Callback) value, pipe, action);//append callback
                }
            } finally {
                arbSet(value);//setup back
            }
        }
        if (direct) {
            pipe.add(action);
        }
    }

    @Override
    public void purgeCallbacks() {
        if (status().done) {//fast case
            return;
        }
        while (true) {//lock-loop
            Object value = arbGet();
            if (value != LOCKED && arbCompareAndSet(value, LOCKED)) {
                if (!status().done) {
                    arbSet(null);//purge callbacks
                } else {
                    arbSet(value);//setup back done-value
                }
                break;
            }
            Thread.yield();
        }
    }

    @Override
    public boolean present(T value) {
        return state(Status.PRESENT, value);
    }

    @Override
    public boolean error(@Nonnull Throwable t) {
        return state(Status.ERROR, t);
    }

    @Override
    public boolean cancel() {
        return state(Status.CANCELLED, null);
    }

    @Override
    public boolean timeout() {
        return state(Status.TIMED_OUT, null);
    }

    @Override
    public boolean interrupt() {
        return state(Status.INTERRUPTED, null);
    }

    protected Future() {
        GLOBAL_COUNTER.incrementAndGet();
    }

    protected boolean state(@Nonnull Status status, Object value) {

        //fast cases
        if (status().done) {
            return false;
        }
        if (!status.done) {
            return true;//do nothing, ok
        }

        //lock-loop
        Object iValue;
        while (true) {
            iValue = arbGet();
            if (iValue != LOCKED && arbCompareAndSet(iValue, LOCKED)) {
                break;
            }
            Thread.yield();
        }

        //handle
        Callback tailCallback;
        try {
            if (status().done) {//double-check
                return false;
            }
            tailCallback = (Callback) iValue;
            switch (status) {
                case PENDING:
                    throw new IllegalArgumentException("Pending status must be impossible here");
                case CANCELLED:
                case TIMED_OUT:
                case INTERRUPTED:
                    iValue = null;
                    break;
                case ERROR:
                    checkArgument(value instanceof Throwable);
                case PRESENT:
                    iValue = value;
                    break;
                default:
                    throw new IllegalArgumentException("Unknown status: " + status);
            }
            this.status = status;
        } finally {
            arbSet(iValue);//setup back
        }

        //run callbacks, out of lock, ok
        if (tailCallback != null) {
            runCallbacks(tailCallback);
        }

        return true;
    }

    private void runCallbacks(Callback tailCallback) {
        while (tailCallback != null) {//run callbacks, actually in reversed order, that is no matter
            tailCallback.run();
            tailCallback = tailCallback.prev;
        }
    }

    @SuppressWarnings("unchecked")
    private <R> R valueNotLocked() {
        Object result = arbGet();
        while (result == LOCKED) {
            Thread.yield();
            result = arbGet();
        }
        return (R) result;
    }
}
