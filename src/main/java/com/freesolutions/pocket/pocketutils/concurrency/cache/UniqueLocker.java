package com.freesolutions.pocket.pocketutils.concurrency.cache;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.primitives.CountingMap;

import javax.annotation.Nonnull;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
public class UniqueLocker<K> {

    //NOTE: result's nullability unknown
    public interface Handler<K, V> {
        V handle(@Nonnull K key) throws Exception;
    }

    private final CountingMap<K> locks = new CountingMap<>();

    //NOTE: result's nullability unknown
    public <R> R underLock(@Nonnull K key, @Nonnull Handler<K, R> handler) throws Exception {
        CountingMap.Counter lock = checkNotNull(locks.increment(key));
        try {
            synchronized (lock) {
                return handler.handle(key);
            }
        } finally {
            locks.decrement(key, lock);
        }
    }

    public void underLock(@Nonnull K key, @Nonnull Action action) throws Exception {
        CountingMap.Counter lock = checkNotNull(locks.increment(key));
        try {
            synchronized (lock) {
                action.execute();
            }
        } finally {
            locks.decrement(key, lock);
        }
    }
}
