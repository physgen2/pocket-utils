package com.freesolutions.pocket.pocketutils.concurrency.queue;

import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Addable;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Pollable;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface NLQueue<T> extends Addable<T>, Pollable<T>, Iterable<T> {
    //empty
}
