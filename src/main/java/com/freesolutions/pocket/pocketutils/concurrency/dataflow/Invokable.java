package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Interface to make implementer class able to invoke some functionality inside.
 *
 * Implementations must guarantee that {@link #invoke()} method does not contain
 * any side effects and its call is cheap operation.
 *
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Invokable {

    void invoke();

    @Nonnull
    static <T> Invokable of(@Nonnull Addable<T> addable, @Nonnull T item) {
        return () -> addable.add(item);
    }
}
