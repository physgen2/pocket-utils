package com.freesolutions.pocket.pocketutils.concurrency.result.impl;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class NullableImmutableResult<T> extends ImmutableResult<T> implements NullableResult<T> {

    private static final NullableImmutableResult<?> PENDING = new NullableImmutableResult<>(Status.PENDING, null);
    private static final NullableImmutableResult<?> PRESENT_NULL = new NullableImmutableResult<>(Status.PRESENT, null);
    private static final NullableImmutableResult<?> CANCELLED = new NullableImmutableResult<>(Status.CANCELLED, null);
    private static final NullableImmutableResult<?> TIMED_OUT = new NullableImmutableResult<>(Status.TIMED_OUT, null);
    private static final NullableImmutableResult<?> INTERRUPTED = new NullableImmutableResult<>(Status.INTERRUPTED, null);

    @Nonnull
    public static <T> NullableImmutableResult<T> ofResult(@Nonnull NullableReadableResult<T> source) {
        Status status = source.status();
        return new NullableImmutableResult<>(status, status.present ? source.value() : status.error ? source.error() : null);
    }

    @Nullable
    public static <T> NullableImmutableResult<T> ofResultIfPresent(@Nonnull NullableReadableResult<T> source) {
        Status status = source.status();
        return status.present ? new NullableImmutableResult<>(status, source.value()) : null;
    }

    @Nullable
    public static <T> NullableImmutableResult<T> ofResultIfNotPresent(@Nonnull NullableReadableResult<?> source) {
        Status status = source.status();
        return status.done ? (!status.present ? new NullableImmutableResult<>(status, status.error ? source.error() : null) : null) : null;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableImmutableResult<T> ofPending() {
        return (NullableImmutableResult<T>) PENDING;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableImmutableResult<T> ofPresentNull() {
        return (NullableImmutableResult<T>) PRESENT_NULL;
    }

    @Nonnull
    public static <T> NullableImmutableResult<T> ofPresent(@Nullable T value) {
        return new NullableImmutableResult<>(Status.PRESENT, value);
    }

    @Nonnull
    public static <T> NullableImmutableResult<T> ofError(@Nonnull Throwable t) {
        return new NullableImmutableResult<>(Status.ERROR, t);
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableImmutableResult<T> ofCancelled() {
        return (NullableImmutableResult<T>) CANCELLED;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableImmutableResult<T> ofTimedOut() {
        return (NullableImmutableResult<T>) TIMED_OUT;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableImmutableResult<T> ofInterrupted() {
        return (NullableImmutableResult<T>) INTERRUPTED;
    }

    @Nullable
    public T value() {
        return super.value();//delegate to super, just for nullability marker
    }

    @Nullable
    public T syncedValue() throws Exception {
        return super.syncedValue();//delegate to super, just for nullability marker
    }

    @Override
    public boolean presentNull() {
        return false;//immutable, ok
    }

    @Override
    public boolean present(@Nullable T value) {
        return false;//immutable, just for nullability marker, ok
    }

    @Override
    public boolean assignFrom(@Nonnull NullableReadableResult<T> source) {
        return false;//immutable, ok
    }

    @Override
    public boolean assignFromIfPresent(@Nonnull NullableReadableResult<T> source) {
        return false;//immutable, ok
    }

    @Override
    public boolean assignFromIfNotPresent(@Nonnull NullableReadableResult<?> source) {
        return false;//immutable, ok
    }

    @Override
    public boolean assignFrom(@Nonnull NonnullReadableResult<T> source) {
        return false;//immutable, ok
    }

    @Override
    public boolean assignFromIfPresent(@Nonnull NonnullReadableResult<T> source) {
        return false;//immutable, ok
    }

    private NullableImmutableResult(@Nonnull Status status, Object value) {
        super(status, value);
    }
}
