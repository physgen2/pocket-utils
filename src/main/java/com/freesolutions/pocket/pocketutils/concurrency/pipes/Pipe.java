package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Addable;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NullableSyncResult;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Pipe extends Addable<Action> {

    Pipe DIRECT = Action::executeSafe;

    @Nonnull
    static <T> Addable<T> toOutput(@Nonnull Pipe pipe, @Nonnull NonnullDataAction<T> action) {
        return item -> pipe.add(() -> action.execute(item));
    }

    @Nonnull
    static <T> Addable<T> toOutput(@Nonnull Pipe pipe, @Nonnull NonnullDataHandler<T, Action> handler) {
        return item -> pipe.add(NonnullDataHandler.executeSafe(handler, item, Action.identity()));
    }

    @Nonnull
    default NullableSyncResult<Throwable> addSync(@Nonnull SyncQuery syncQuery, @Nonnull Action action) {

        //timed out if query is immediate
        if (syncQuery.immediate) {
            return NullableSyncResult.ofTimedOut();
        }

        //run action on pipe
        AtomicReference<Optional<Throwable>> completed = new AtomicReference<>();
        add(() -> {
            Throwable error = null;
            try {
                action.execute();
            } catch (InterruptedException ignored) {
                Thread.currentThread().interrupt();
                error = new CancellationException("interrupted");
            } catch (Throwable t) {
                error = t;
            } finally {
                synchronized (syncQuery) {
                    completed.set(Optional.ofNullable(error));
                    syncQuery.notifyAll();
                }
            }
        });

        //do wait
        synchronized (syncQuery) {
            try {
                Optional<Throwable> r = ClockUtils.waitDeadlineR(
                    syncQuery.deadlineTs,
                    syncQuery.clock,
                    syncQuery,
                    ignored -> completed.get()
                );
                return r != null ?
                    NullableSyncResult.ofOk(r.orElse(null)) :
                    NullableSyncResult.ofTimedOut();
            } catch (InterruptedException ignored) {
                Thread.currentThread().interrupt();
                return NullableSyncResult.ofInterrupted();
            } catch (Throwable t) {
                LoggerFactory.getLogger(getClass()).error("impossible", t);
                return NullableSyncResult.ofRejected();
            }
        }
    }
}
