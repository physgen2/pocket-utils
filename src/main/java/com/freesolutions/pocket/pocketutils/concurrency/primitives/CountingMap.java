package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import com.freesolutions.pocket.pocketutils.actions.Action;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.lang.invoke.VarHandle;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class CountingMap<K> {

    @Nonnull
    private final ConcurrentMap<K, Counter> counters = new ConcurrentHashMap<>();

    public static class Counter {

        private static final VarHandle VALUE_VH = Vars.getVarHandle(Counter.class, "value", int.class);

        private volatile int value;

        public int get() {
            return value;
        }

        private Counter(int positiveInitial) {
            checkArgument(positiveInitial > 0);
            this.value = positiveInitial;
        }

        private boolean isValid() {
            return value > 0;
        }

        private boolean increment(int positiveDelta) {
            checkArgument(positiveDelta > 0);
            while (true) {
                int v = value;
                if (v == 0) {
                    return false;
                }
                if (cas(v, v + positiveDelta)) {
                    return true;
                }
            }
        }

        private int decrement(int positiveDelta) {
            checkArgument(positiveDelta > 0);
            while (true) {
                int v = value;
                if (v == 0) {
                    return 0;
                }
                int v1 = v > positiveDelta ? v - positiveDelta : 0;
                if (cas(v, v1)) {
                    return v - v1;
                }
            }
        }

        private boolean cas(int expect, int update) {
            return VALUE_VH.compareAndSet(this, expect, update);
        }
    }

    @Nonnull
    public Set<K> keys() {
        return Collections.unmodifiableSet(counters.keySet());
    }

    @Nullable
    public Counter get(@Nonnull K key) {
        return counters.get(key);
    }

    @Nullable
    public Counter incrementIfInitial(@Nonnull K key) {
        Counter counter = new Counter(1);
        while (true) {
            Counter existing = counters.putIfAbsent(key, counter);
            if (existing == null) {
                return counter;
            }
            if (existing.isValid()) {
                return null;
            }
            counters.remove(key, existing);
        }
    }

    @Nullable
    public Counter incrementAlwaysAndGetIfInitial(@Nonnull K key) {
        return incrementAlwaysAndGetIfInitial(key, 1);
    }

    @Nullable
    public Counter incrementAlwaysAndGetIfInitial(@Nonnull K key, int positiveDelta) {
        Counter counter = counters.get(key);
        while (true) {
            if (counter != null) {
                if (counter.increment(positiveDelta)) {
                    return null;
                }
                counters.remove(key, counter);
                counter = counters.get(key);
            } else {
                counter = new Counter(positiveDelta);
                Counter existing = counters.putIfAbsent(key, counter);
                if (existing == null) {
                    return counter;
                }
                counter = existing;
            }
        }
    }

    @Nonnull
    public Counter increment(@Nonnull K key) {
        return increment(key, 1);
    }

    @Nonnull
    public Counter increment(@Nonnull K key, int positiveDelta) {
        Counter counter = counters.get(key);
        while (true) {
            if (counter != null) {
                if (counter.increment(positiveDelta)) {
                    return counter;
                }
                counters.remove(key, counter);
                counter = counters.get(key);
            } else {
                counter = new Counter(positiveDelta);
                Counter existing = counters.putIfAbsent(key, counter);
                if (existing == null) {
                    return counter;
                }
                counter = existing;
            }
        }
    }

    @Nullable
    public Counter decrementAlwaysAndGetIfNotLast(@Nonnull K key, @Nonnull Counter counter) {
        return decrementAlwaysAndGetIfNotLast(key, counter, 1);
    }

    @Nullable
    public Counter decrementAlwaysAndGetIfNotLast(@Nonnull K key, @Nonnull Counter counter, int positiveDelta) {
        counter.decrement(positiveDelta);
        if (!counter.isValid()) {
            counters.remove(key, counter);
            return null;
        }
        return counter;
    }

    public boolean decrement(@Nonnull K key, @Nonnull Counter counter) {
        return decrement(key, counter, 1) != 0;
    }

    public int decrement(@Nonnull K key, @Nonnull Counter counter, int positiveDelta) {
        int result = counter.decrement(positiveDelta);
        if (!counter.isValid()) {
            counters.remove(key, counter);
        }
        return result;
    }

    public boolean incrementAndDoActionIfInitial(@Nonnull K key, @Nonnull Action action) throws Exception {
        Counter counter = incrementAlwaysAndGetIfInitial(key);
        if (counter != null) {
            action.execute();
            return true;
        }
        return false;
    }

    public boolean decrementAndDoActionIfLast(@Nonnull K key, @Nonnull Action action) throws Exception {
        Counter counter = counters.get(key);
        if (counter != null && decrementAlwaysAndGetIfNotLast(key, counter) == null) {
            action.execute();
            return true;
        }
        return false;
    }
}
