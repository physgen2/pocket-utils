package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Submits;
import com.freesolutions.pocket.pocketutils.actions.DataAction;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NullableReadableResultSubmitsExt<T, RESULT extends NullableReadableResult<T>> extends NullableReadableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT submitNR(Pipe pipe, NonnullDataAction<NullableReadableResult<T>> handler) {
        Submits.submitNR(pipe, this, handler);
        return (RESULT) this;
    }

    default RESULT submitNX(Pipe pipe, DataAction<T> handler) {
        Submits.submitNX(pipe, this, handler);
        return (RESULT) this;
    }
}
