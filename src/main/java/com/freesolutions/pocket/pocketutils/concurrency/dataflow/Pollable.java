package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Pollable<T> {

    int size();

    default boolean isEmpty() {
        return size() == 0;
    }

    @Nullable
    T poll();

    @Nullable
    T peek();

    default void clear() {
        while (size() > 0 && poll() != null) {
            //empty
        }
    }

    @Nonnull
    Iterator<T> iterator();

    @Nonnull
    default Iterator<T> consumingIterator() {
        return new Iterator<T>() {
            private T value;

            @Override
            public boolean hasNext() {
                this.value = poll();
                return value != null;
            }

            @Override
            public T next() {
                T result = value;
                if (result == null) {
                    throw new NoSuchElementException();
                }
                this.value = null;
                return result;
            }
        };
    }

    @Nonnull
    default Iterable<T> consumingIterable() {
        return this::consumingIterator;
    }
}
