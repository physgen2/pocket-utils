package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import javax.annotation.concurrent.ThreadSafe;
import java.lang.invoke.VarHandle;

/**
 * @author Stanoslau Mirzayeu
 */
@ThreadSafe
public class Alb {

    private static final VarHandle VALUE_VH = Vars.getVarHandle(Alb.class, "value", long.class);

    private volatile long value;

    protected Alb() {
        //nothing
    }

    protected Alb(long initial) {
        this.value = initial;
    }

    public long albGet() {
        return value;
    }

    public void albSet(long value) {
        this.value = value;
    }

    protected boolean albCompareAndSet(long expect, long update) {
        return VALUE_VH.compareAndSet(this, expect, update);
    }

    protected long albGetAndSet(long newValue) {
        return (long) VALUE_VH.getAndSet(this, newValue);
    }

    protected long getAndIncrement() {
        return (long) VALUE_VH.getAndAdd(this, 1L);
    }

    protected long getAndDecrement() {
        return (long) VALUE_VH.getAndAdd(this, -1L);
    }

    protected long getAndAdd(long delta) {
        return (long) VALUE_VH.getAndAdd(this, delta);
    }

    protected long incrementAndGet() {
        return (long) VALUE_VH.getAndAdd(this, 1L) + 1L;
    }

    protected long decrementAndGet() {
        return (long) VALUE_VH.getAndAdd(this, -1L) - 1L;
    }

    protected long addAndGet(long delta) {
        return (long) VALUE_VH.getAndAdd(this, delta) + delta;
    }
}
