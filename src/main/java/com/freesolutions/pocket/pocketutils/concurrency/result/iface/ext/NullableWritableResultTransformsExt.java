package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Transforms;
import com.freesolutions.pocket.pocketutils.actions.DataHandler;
import com.freesolutions.pocket.pocketutils.actions.FromNonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.ToNonnullDataHandler;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NullableWritableResultTransformsExt<T, RESULT extends NullableWritableResult<T>> extends NullableWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNR2NR(Pipe pipe, NullableReadableResult<S> source, NonnullDataHandler<NullableReadableResult<S>, NullableReadableResult<T>> handler) {
        Transforms.transformNR2NR(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformR2NR(Pipe pipe, NonnullReadableResult<S> source, NonnullDataHandler<NonnullReadableResult<S>, NullableReadableResult<T>> handler) {
        Transforms.transformR2NR(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNR2NX(Pipe pipe, NullableReadableResult<S> source, FromNonnullDataHandler<NullableReadableResult<S>, T> handler) {
        Transforms.transformNR2NX(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformR2NX(Pipe pipe, NonnullReadableResult<S> source, FromNonnullDataHandler<NonnullReadableResult<S>, T> handler) {
        Transforms.transformR2NX(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNX2NR(Pipe pipe, NullableReadableResult<S> source, ToNonnullDataHandler<S, NullableReadableResult<T>> handler) {
        Transforms.transformNX2NR(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformX2NR(Pipe pipe, NonnullReadableResult<S> source, NonnullDataHandler<S, NullableReadableResult<T>> handler) {
        Transforms.transformX2NR(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofTransformNX2NX(Pipe pipe, NullableReadableResult<S> source, DataHandler<S, T> handler) {
        Transforms.transformNX2NX(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofTransformX2NX(Pipe pipe, NonnullReadableResult<S> source, FromNonnullDataHandler<S, T> handler) {
        Transforms.transformX2NX(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
