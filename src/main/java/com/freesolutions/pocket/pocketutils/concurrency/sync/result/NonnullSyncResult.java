package com.freesolutions.pocket.pocketutils.concurrency.sync.result;

import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncStatus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeoutException;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class NonnullSyncResult<T> extends NullableSyncResult<T> {

    private static final NonnullSyncResult<?> TIMED_OUT = new NonnullSyncResult<>(SyncStatus.TIMED_OUT, null);
    private static final NonnullSyncResult<?> INTERRUPTED = new NonnullSyncResult<>(SyncStatus.INTERRUPTED, null);
    private static final NonnullSyncResult<?> REJECTED = new NonnullSyncResult<>(SyncStatus.REJECTED, null);

    @Nullable
    public static <T> NonnullSyncResult<T> ofOkIfOk(@Nonnull NonnullSyncResult<T> source) {
        return source.status().ok ? source : null;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static <T> NonnullSyncResult<T> ofNotOkIfNotOk(@Nonnull NullableSyncResult<?> source) {//source is nullable, ok
        SyncStatus status = source.status();
        if (status.ok) {
            return null;
        }
        if (source instanceof NonnullSyncResult) {
            return (NonnullSyncResult<T>) source;//direct cast due to value is null
        }
        return new NonnullSyncResult<>(status, null);
    }

    @Nonnull
    public static <T> NonnullSyncResult<T> ofOk(@Nonnull T value) {
        return new NonnullSyncResult<>(SyncStatus.OK, value);
    }

    @Nonnull
    public static <T> NonnullSyncResult<T> ofNotOk(@Nonnull SyncStatus status) {
        checkArgument(!status.ok);
        return new NonnullSyncResult<>(status, null);
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullSyncResult<T> ofTimedOut() {
        return (NonnullSyncResult<T>) TIMED_OUT;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullSyncResult<T> ofInterrupted() {
        return (NonnullSyncResult<T>) INTERRUPTED;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullSyncResult<T> ofRejected() {
        return (NonnullSyncResult<T>) REJECTED;
    }

    @Nonnull
    @Override
    public SyncStatus status() {
        return super.status();
    }

    @Nonnull
    @Override
    public T value() {
        return checkNotNull(super.value());
    }

    @Nonnull
    @Override
    public NonnullSyncResult<T> notRejected() throws RejectedExecutionException {
        return (NonnullSyncResult<T>) super.notRejected();
    }

    @Nonnull
    @Override
    public NonnullSyncResult<T> notInterrupted() throws InterruptedException {
        return (NonnullSyncResult<T>) super.notInterrupted();
    }

    @Nonnull
    @Override
    public NonnullSyncResult<T> noProblems() throws RejectedExecutionException, InterruptedException {
        return (NonnullSyncResult<T>) super.noProblems();
    }

    @Nonnull
    @Override
    public NonnullSyncResult<T> notTimedOut() throws TimeoutException {
        return (NonnullSyncResult<T>) super.notTimedOut();
    }

    @Nonnull
    @Override
    public NonnullSyncResult<T> mustOk() throws RejectedExecutionException, InterruptedException, TimeoutException {
        return (NonnullSyncResult<T>) super.mustOk();
    }

    protected NonnullSyncResult(@Nonnull SyncStatus status, @Nullable T value) {
        super(status, value);
        checkArgument((status.ok && value != null) || (!status.ok && value == null));
    }
}
