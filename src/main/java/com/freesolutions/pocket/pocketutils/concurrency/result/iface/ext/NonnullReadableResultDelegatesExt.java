package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Delegates;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NonnullReadableResultDelegatesExt<T, RESULT extends NonnullReadableResult<T>> extends NonnullReadableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT gag(Pipe pipe) {
        Delegates.gag(pipe, this);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT delegateR2R(Pipe pipe, NonnullWritableResult<T> target) {
        Delegates.delegateR2R(pipe, this, target);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT delegateIfPresentR2R(Pipe pipe, NonnullWritableResult<T> target) {
        Delegates.delegateIfPresentR2R(pipe, this, target);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT delegateIfNotPresentR2NR(Pipe pipe, NullableWritableResult<T> target) {
        Delegates.delegateIfNotPresentR2NR(pipe, this, target);
        return (RESULT) this;
    }

    default RESULT delegateIfNotPresentR2R(Pipe pipe, NonnullWritableResult<T> target) {
        Delegates.delegateIfNotPresentR2R(pipe, this, target);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
