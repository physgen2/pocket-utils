package com.freesolutions.pocket.pocketutils.concurrency.loop;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Invokable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public interface Loop extends Startable, Invokable {

    @Nonnull
    String name();

    @Nonnull
    LoopConfig config();

    boolean started();

    void skipNearestInvoke();
}
