package com.freesolutions.pocket.pocketutils.concurrency.topic.impl.consumer;

import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Addable;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Consumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.ListenableConsumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Topic;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.function.Predicate;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class DelegatingListenableConsumer<T> implements ListenableConsumer<T> {

    @Nonnull
    private final Topic<T> topic;
    @Nonnull
    private final Predicate<T> filter;
    @Nonnull
    private final Addable<T> output;
    @Nonnull
    private final NonnullDataAction<Consumer<T>> starter;
    @Nonnull
    private final NonnullDataAction<Consumer<T>> stopper;

    public DelegatingListenableConsumer(
        @Nonnull Topic<T> topic,
        @Nonnull Predicate<T> filter,
        @Nonnull Addable<T> output,
        @Nonnull NonnullDataAction<Consumer<T>> starter,
        @Nonnull NonnullDataAction<Consumer<T>> stopper
    ) {
        this.topic = topic;
        this.filter = filter;
        this.output = output;
        this.starter = starter;
        this.stopper = stopper;
    }

    @Override
    public void start() throws Exception {
        starter.execute(this);
    }

    @Override
    public void stop() {
        NonnullDataAction.executeSafe(stopper, this);
    }

    @Nonnull
    @Override
    public Topic<T> topic() {
        return topic;
    }

    @Nonnull
    @Override
    public Predicate<T> filter() {
        return filter;
    }

    @Override
    public void add(@Nonnull T item) {
        output.add(item);//delegate to output, ok
    }
}
