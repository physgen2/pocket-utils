package com.freesolutions.pocket.pocketutils.concurrency.result.util;

import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.Result;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullablePresentableResult;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class Completions {

    //---------------------------------------------------------------------------------------------

    public static void toCompletionOne(Pipe pipe, NullablePresentableResult<?> target, Result<?> result) {
        result.whenDone(pipe, target::presentNull);
    }

    public static void toCompletionTwo(Pipe pipe, NullablePresentableResult<?> target, Result<?> result1, Result<?> result2) {
        result1.whenDone(pipe, () -> result2.whenDone(pipe, target::presentNull));
    }

    public static void toCompletion(Pipe pipe, NullablePresentableResult<?> target, Result<?>... results) {
        toCompletion(pipe, target, Arrays.asList(results));
    }

    public static void toCompletion(Pipe pipe, NullablePresentableResult<?> target, Iterable<? extends Result<?>> results) {
        AtomicInteger counter = new AtomicInteger(1);
        for (Result<?> result : results) {
            counter.incrementAndGet();
            result.whenDone(pipe, () -> {
                if (counter.decrementAndGet() == 0) {
                    target.presentNull();
                }
            });
        }
        if (counter.decrementAndGet() == 0) {
            target.presentNull();
        }
    }

    //TODO completion with async suppliers of futures to wait

    //---------------------------------------------------------------------------------------------
}
