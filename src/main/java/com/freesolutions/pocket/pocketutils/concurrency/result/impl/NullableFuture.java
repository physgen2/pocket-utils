package com.freesolutions.pocket.pocketutils.concurrency.result.impl;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class NullableFuture<T> extends Future<T> implements NullableResult<T> {

    public static <T> NullableFuture<T> of() {
        return new NullableFuture<>();
    }

    @Nullable
    public T value() {
        return super.value();//delegate to super, just for nullability marker
    }

    @Nullable
    public T syncedValue() throws Exception {
        return super.syncedValue();//delegate to super, just for nullability marker
    }

    @Override
    public boolean presentNull() {
        return super.present(null);
    }

    @Override
    public boolean present(@Nullable T value) {
        return super.present(value);//delegate to super, just for nullability marker
    }

    @Override
    public boolean assignFrom(@Nonnull NullableReadableResult<T> source) {
        Status status = source.status();
        return state(status, status.present ? source.value() : status.error ? source.error() : null);
    }

    @Override
    public boolean assignFromIfPresent(@Nonnull NullableReadableResult<T> source) {
        Status status = source.status();
        return status.present && state(status, source.value());
    }

    @Override
    public boolean assignFromIfNotPresent(@Nonnull NullableReadableResult<?> source) {
        Status status = source.status();
        return status.done && !status.present && state(status, status.error ? source.error() : null);
    }

    @Override
    public boolean assignFrom(@Nonnull NonnullReadableResult<T> source) {
        return assignFrom((NullableReadableResult<T>) source);//just delegate to nullable variant
    }

    @Override
    public boolean assignFromIfPresent(@Nonnull NonnullReadableResult<T> source) {
        return assignFromIfPresent((NullableReadableResult<T>) source);//just delegate to nullable variant
    }

    protected NullableFuture() {
        super();
    }
}
