package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import javax.annotation.Nonnull;
import java.util.function.Function;

/**
 * Abstraction for retrieving some actual value at time of call.
 * Implementations must guarantee that method will never fail, and clients can rely on this.
 * If for some reasons actual value is not available, implementations must return latest available value.
 * If no latest value available then zero-value must be returned by default, not null.
 *
 * @author Stanislau Mirzayeu
 */
public interface Live<T> {

    @Nonnull
    T get();

    static <FROM, TO> Live<TO> map(Live<FROM> live, Function<FROM, TO> mapper) {
        return () -> mapper.apply(live.get());
    }
}
