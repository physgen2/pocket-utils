package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;
import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;
import com.freesolutions.pocket.pocketutils.actions.ToNonnullDataHandler;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Submits;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NonnullWritableResultSubmitsExt<T, RESULT extends NonnullWritableResult<T>> extends NonnullWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofSubmit2R(Pipe pipe, NonnullHandler<NonnullReadableResult<T>> handler) {
        Submits.submit2R(pipe, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT ofSubmit2X(Pipe pipe, NonnullHandler<T> handler) {
        Submits.submit2X(pipe, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofSubmitNS2R(Pipe pipe, @Nullable S source, ToNonnullDataHandler<S, NonnullReadableResult<T>> handler) {
        Submits.submitNS2R(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofSubmitS2R(Pipe pipe, S source, NonnullDataHandler<S, NonnullReadableResult<T>> handler) {
        Submits.submitS2R(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default <S> RESULT ofSubmitNS2X(Pipe pipe, @Nullable S source, ToNonnullDataHandler<S, T> handler) {
        Submits.submitNS2X(pipe, source, this, handler);
        return (RESULT) this;
    }

    default <S> RESULT ofSubmitS2X(Pipe pipe, S source, NonnullDataHandler<S, T> handler) {
        Submits.submitS2X(pipe, source, this, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
