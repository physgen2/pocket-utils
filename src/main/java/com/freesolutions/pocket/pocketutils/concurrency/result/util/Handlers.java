package com.freesolutions.pocket.pocketutils.concurrency.result.util;

import com.freesolutions.pocket.pocketutils.actions.*;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NullableImmutableResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class Handlers {
    private static final Logger LOGGER = LoggerFactory.getLogger(Handlers.class);

    //---------------------------------------------------------------------------------------------

    public static <T> NonnullDataAction<NullableReadableResult<T>> handlerNX(DataAction<T> handler) {
        return from -> {
            if (from.status().error) {
                LOGGER.error("", checkNotNull(from.error()));
                return;
            }
            if (!from.status().done) {
                LOGGER.error("result is not done");
                return;
            }
            if (!from.status().present) {
                //silent, ok
                return;
            }
            handler.execute(from.value());
        };
    }

    public static <T> NonnullDataAction<NonnullReadableResult<T>> handlerX(NonnullDataAction<T> handler) {
        return from -> {
            if (from.status().error) {
                LOGGER.error("", checkNotNull(from.error()));
                return;
            }
            if (!from.status().done) {
                LOGGER.error("result is not done");
                return;
            }
            if (!from.status().present) {
                //silent, ok
                return;
            }
            handler.execute(from.value());
        };
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> NonnullDataHandler<NullableReadableResult<T>, NullableReadableResult<R>> handlerNR2NX(FromNonnullDataHandler<NullableReadableResult<T>, R> handler) {
        return from -> NullableImmutableResult.ofPresent(handler.handle(from));
    }

    public static <T, R> NonnullDataHandler<NullableReadableResult<T>, NonnullReadableResult<R>> handlerNR2X(NonnullDataHandler<NullableReadableResult<T>, R> handler) {
        return from -> NonnullImmutableResult.ofPresent(handler.handle(from));
    }

    public static <T, R> NonnullDataHandler<NonnullReadableResult<T>, NullableReadableResult<R>> handlerR2NX(FromNonnullDataHandler<NonnullReadableResult<T>, R> handler) {
        return from -> NullableImmutableResult.ofPresent(handler.handle(from));
    }

    public static <T, R> NonnullDataHandler<NonnullReadableResult<T>, NonnullReadableResult<R>> handlerR2X(NonnullDataHandler<NonnullReadableResult<T>, R> handler) {
        return from -> NonnullImmutableResult.ofPresent(handler.handle(from));
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> NonnullDataHandler<NullableReadableResult<T>, NullableReadableResult<R>> handlerNX2NR(ToNonnullDataHandler<T, NullableReadableResult<R>> handler) {
        return from -> {
            NullableReadableResult<R> notPresent = NullableImmutableResult.ofResultIfNotPresent(from);
            if (notPresent != null) {//pass if not present
                return notPresent;
            }
            checkState(from.status().present);
            return handler.handle(from.value());
        };
    }

    public static <T, R> NonnullDataHandler<NullableReadableResult<T>, NonnullReadableResult<R>> handlerNX2R(ToNonnullDataHandler<T, NonnullReadableResult<R>> handler) {
        return from -> {
            NonnullReadableResult<R> notPresent = NonnullImmutableResult.ofResultIfNotPresent(from);
            if (notPresent != null) {//pass if not present
                return notPresent;
            }
            checkState(from.status().present);
            return handler.handle(from.value());
        };
    }

    public static <T, R> NonnullDataHandler<NonnullReadableResult<T>, NullableReadableResult<R>> handlerX2NR(NonnullDataHandler<T, NullableReadableResult<R>> handler) {
        return from -> {
            NullableReadableResult<R> notPresent = NullableImmutableResult.ofResultIfNotPresent(from);
            if (notPresent != null) {//pass if not present
                return notPresent;
            }
            checkState(from.status().present);
            return handler.handle(from.value());
        };
    }

    public static <T, R> NonnullDataHandler<NonnullReadableResult<T>, NonnullReadableResult<R>> handlerX2R(NonnullDataHandler<T, NonnullReadableResult<R>> handler) {
        return from -> {
            NonnullReadableResult<R> notPresent = NonnullImmutableResult.ofResultIfNotPresent(from);
            if (notPresent != null) {//pass if not present
                return notPresent;
            }
            checkState(from.status().present);
            return handler.handle(from.value());
        };
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> NonnullDataHandler<NullableReadableResult<T>, NullableReadableResult<R>> handlerNX2NX(DataHandler<T, R> handler) {
        return handlerNX2NR(from -> NullableImmutableResult.ofPresent(handler.handle(from)));
    }

    public static <T, R> NonnullDataHandler<NullableReadableResult<T>, NonnullReadableResult<R>> handlerNX2X(ToNonnullDataHandler<T, R> handler) {
        return handlerNX2R(from -> NonnullImmutableResult.ofPresent(handler.handle(from)));
    }

    public static <T, R> NonnullDataHandler<NonnullReadableResult<T>, NullableReadableResult<R>> handlerX2NX(FromNonnullDataHandler<T, R> handler) {
        return handlerX2NR(from -> NullableImmutableResult.ofPresent(handler.handle(from)));
    }

    public static <T, R> NonnullDataHandler<NonnullReadableResult<T>, NonnullReadableResult<R>> handlerX2X(NonnullDataHandler<T, R> handler) {
        return handlerX2R(from -> NonnullImmutableResult.ofPresent(handler.handle(from)));
    }

    //---------------------------------------------------------------------------------------------

    public static <S> void handleSafeNS(@Nullable S source, DataAction<S> handler) {
        try {
            handler.execute(source);
        } catch (ExecutionException t) {
            LOGGER.error("", t.getCause());
        } catch (CancellationException | TimeoutException ignored) {
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        } catch (Throwable t) {
            LOGGER.error("", t);
        }
    }

    public static <S> void handleSafeS(S source, NonnullDataAction<S> handler) {
        try {
            handler.execute(source);
        } catch (ExecutionException t) {
            LOGGER.error("", t.getCause());
        } catch (CancellationException | TimeoutException ignored) {
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        } catch (Throwable t) {
            LOGGER.error("", t);
        }
    }

    //---------------------------------------------------------------------------------------------

    public static <R> NullableReadableResult<R> handleSafe2NR(NonnullHandler<? extends NullableReadableResult<R>> handler) {
        try {
            return handler.handle();
        } catch (ExecutionException t) {
            return NullableImmutableResult.ofError(t.getCause());
        } catch (CancellationException ignored) {
            return NullableImmutableResult.ofCancelled();
        } catch (TimeoutException ignored) {
            return NullableImmutableResult.ofTimedOut();
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return NullableImmutableResult.ofInterrupted();
        } catch (Throwable t) {
            return NullableImmutableResult.ofError(t);
        }
    }

    public static <R> NonnullReadableResult<R> handleSafe2R(NonnullHandler<? extends NonnullReadableResult<R>> handler) {
        try {
            return handler.handle();
        } catch (ExecutionException t) {
            return NonnullImmutableResult.ofError(t.getCause());
        } catch (CancellationException ignored) {
            return NonnullImmutableResult.ofCancelled();
        } catch (TimeoutException ignored) {
            return NonnullImmutableResult.ofTimedOut();
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
            return NonnullImmutableResult.ofInterrupted();
        } catch (Throwable t) {
            return NonnullImmutableResult.ofError(t);
        }
    }

    //---------------------------------------------------------------------------------------------

    public static <R> void handleAndDelegate2NR(Pipe pipe, NullableWritableResult<R> target, NonnullHandler<? extends NullableReadableResult<R>> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(handler), target);
    }

    public static <R> void handleAndDelegate2R(Pipe pipe, NonnullWritableResult<R> target, NonnullHandler<? extends NonnullReadableResult<R>> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(handler), target);
    }

    //---------------------------------------------------------------------------------------------

    public static <R> void handleAndDelegate2NX(Pipe pipe, NullableWritableResult<R> target, Handler<R> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> NullableImmutableResult.ofPresent(handler.handle())), target);
    }

    public static <R> void handleAndDelegate2X(Pipe pipe, NonnullWritableResult<R> target, NonnullHandler<R> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> NonnullImmutableResult.ofPresent(handler.handle())), target);
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void handleAndDelegateNR2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, NonnullDataHandler<NullableReadableResult<T>, NullableReadableResult<R>> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handler.handle(source)), target);
    }

    public static <T, R> void handleAndDelegateNR2R(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NullableReadableResult<T>, NonnullReadableResult<R>> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handler.handle(source)), target);
    }

    public static <T, R> void handleAndDelegateR2NR(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, NonnullDataHandler<NonnullReadableResult<T>, NullableReadableResult<R>> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handler.handle(source)), target);
    }

    public static <T, R> void handleAndDelegateR2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NonnullReadableResult<T>, NonnullReadableResult<R>> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handler.handle(source)), target);
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void handleAndDelegateNR2NX(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, FromNonnullDataHandler<NullableReadableResult<T>, R> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handlerNR2NX(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateNR2X(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NullableReadableResult<T>, R> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handlerNR2X(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateR2NX(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, FromNonnullDataHandler<NonnullReadableResult<T>, R> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handlerR2NX(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateR2X(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<NonnullReadableResult<T>, R> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handlerR2X(handler).handle(source)), target);
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void handleAndDelegateNX2NR(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, ToNonnullDataHandler<T, NullableReadableResult<R>> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handlerNX2NR(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateNX2R(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, ToNonnullDataHandler<T, NonnullReadableResult<R>> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handlerNX2R(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateX2NR(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, NonnullDataHandler<T, NullableReadableResult<R>> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handlerX2NR(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateX2R(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<T, NonnullReadableResult<R>> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handlerX2R(handler).handle(source)), target);
    }

    //---------------------------------------------------------------------------------------------

    public static <T, R> void handleAndDelegateNX2NX(Pipe pipe, NullableReadableResult<T> source, NullableWritableResult<R> target, DataHandler<T, R> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handlerNX2NX(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateNX2X(Pipe pipe, NullableReadableResult<T> source, NonnullWritableResult<R> target, ToNonnullDataHandler<T, R> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handlerNX2X(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateX2NX(Pipe pipe, NonnullReadableResult<T> source, NullableWritableResult<R> target, FromNonnullDataHandler<T, R> handler) {
        Delegates.delegateNR2NR(pipe, handleSafe2NR(() -> handlerX2NX(handler).handle(source)), target);
    }

    public static <T, R> void handleAndDelegateX2X(Pipe pipe, NonnullReadableResult<T> source, NonnullWritableResult<R> target, NonnullDataHandler<T, R> handler) {
        Delegates.delegateR2R(pipe, handleSafe2R(() -> handlerX2X(handler).handle(source)), target);
    }

    //---------------------------------------------------------------------------------------------
}
