package com.freesolutions.pocket.pocketutils.concurrency.queue.legacy;

import com.freesolutions.pocket.pocketutils.concurrency.primitives.Arb;
import com.freesolutions.pocket.pocketutils.concurrency.queue.NLQueue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public class OldLinkedNLQueue<T> implements NLQueue<T> {

    //Possible states:
    //1) t= h= 0                - queue is empty
    //2) t= h= (i:0)            - one element in queue
    //3) t= i:0, h= 0           - temporal transition state within 1 -> 2 or 2 -> 1
    //4) t= [i:]+ h= (i:0)      - more than one element in queue

    private static class Item<T> extends Arb<Item<T>> {//NOTE: <Arb> is reference to next item
        @Nonnull
        public final T value;

        public Item(@Nonnull T value) {
            this.value = value;
        }

        public Item<T> getNext() {//make method public
            return super.arbGet();
        }

        public void setNext(Item<T> value) {//make method public
            super.arbSet(value);
        }

        public boolean casNext(Item<T> expect, Item<T> update) {//make method public
            return super.arbCompareAndSet(expect, update);
        }
    }

    private final AtomicReference<Item<T>> head = new AtomicReference<>();
    private final AtomicReference<Item<T>> tail = new AtomicReference<>();
    private final AtomicInteger size = new AtomicInteger();
    private final AtomicInteger collisions = new AtomicInteger();

    public OldLinkedNLQueue() {
        //empty
    }

    public int getCollisions() {
        return collisions.get();
    }

    @Override
    public int size() {
        return size.get();
    }

    @Override
    public void add(@Nonnull T value) {
        checkNotNull(value);//check to avoid further undefined behavior in case of null
        incrementSize();
        Item<T> newItem = new Item<>(value);
        while (true) {
            //states: 1..4
            Item<T> head = this.head.get();
            if (head != null) {
                if (head.casNext(null, newItem)) {
                    if (this.head.compareAndSet(head, newItem)) {
                        //states: 4
                        break;
                    }
                    //head item was removed from queue, try again
                    head.setNext(null);//help GC
                }
            } else {
                if (this.tail.compareAndSet(null, newItem)) {
                    //states: 3 - can't be changed here
                    boolean ok = this.head.compareAndSet(null, newItem);
                    checkState(ok);
                    //states: 2
                    break;
                }
            }
            collisions.incrementAndGet();
            Thread.yield();
        }
    }

    @Nullable
    @Override
    public T poll() {
        Item<T> result;
        while (true) {
            result = this.tail.get();
            if (result == null) {
                return null;
            }
            Item<T> head = this.head.get();
            if (head == null) {
                return null;
            }
            if (result != head) {
                Item<T> next = result.getNext();
                if (next != null && this.tail.compareAndSet(result, next)) {
                    //states: 2,4
                    result.setNext(null);//help GC
                    break;
                }
            } else {
                if (this.head.compareAndSet(head, null)) {
                    //states: 3 - can't be changed here
                    boolean ok = this.tail.compareAndSet(result, null);
                    checkState(ok);
                    //states: 1
                    break;
                }
            }
            collisions.incrementAndGet();
            Thread.yield();
        }
        boolean ok = decrementSize();
        checkState(ok);
        return result.value;
    }

    @Nullable
    @Override
    public T peek() {
        Item<T> target = this.tail.get();
        if (target == null) {
            return null;
        }
        Item<T> head = this.head.get();
        if (head == null) {
            return null;
        }
        return target.value;
    }

    @Nonnull
    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException();
    }

    private void incrementSize() {
        size.incrementAndGet();
    }

    private boolean decrementSize() {
        while (true) {
            int size = this.size.get();
            if (size == 0) {
                return false;
            }
            if (this.size.compareAndSet(size, size - 1)) {
                return true;
            }
        }
    }
}

//    @Override
//    public Iterator<T> iterator() {
//        return new Iterator<T>() {
//            private final AtomicReference<Item<T>> current;
//            private final Item<T> last;
//            {
//                Item<T> t = tail.get();
//                Item<T> h = head.get();
//                if (t == null || h == null) {
//                    this.current = new AtomicReference<>();
//                    this.last = null;
//                } else {
//                    this.current = new AtomicReference<>(t);
//                    this.last = h;
//                }
//            }
//            @Override
//            public boolean hasNext() {
//                return current.get() != null;
//            }
//            @Override
//            public T next() {
//                while (true) {
//                    Item<T> result = current.get();
//                    if (result == null) {
//                        throw new NoSuchElementException();
//                    }
//                    Item<T> next = result != last ? retrieveNext(result) : null;
//                    if (current.compareAndSet(result, next)) {
//                        return result.value;
//                    }
//                }
//            }
//            private Item<T> retrieveNext(Item<T> item) {
//                Item<T> result = item.getNext();
//                if (result == null) {
//                    throw new ConcurrentModificationException("next item unavailable");
//                }
//                return result;
//            }
//        };
//    }
