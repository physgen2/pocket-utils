package com.freesolutions.pocket.pocketutils.concurrency.loop;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class Threads implements Startable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Threads.class);

    //NOTE: all state is guarded by this

    @Nonnull
    private final String name;

    private volatile boolean started;

    private int threadCount;
    private final List<Thread> submitted = new LinkedList<>();
    private final List<Thread> working = new LinkedList<>();
    private volatile boolean stopRequested;

    public Threads(
        @Nonnull String name,
        int count,
        boolean ignoreInterruption,
        @Nonnull Action loopBody
    ) {
        this.name = name;
        submitN(count, ignoreInterruption, loopBody);
    }

    public Threads(@Nonnull String name) {
        this.name = name;
    }

    @Override
    public synchronized void start() throws Exception {

        //check
        if (started) {
            return;
        }

        //mark started
        this.started = true;

        //start submitted, allow start failures
        this.stopRequested = false;
        try {
            startSubmitted(false);
        } catch (Throwable t) {
            stop();
            throw t;
        }
    }

    @Override
    public void stop() {
        //NOTE: stop method must work without exceptions

        List<Thread> toJoin;
        synchronized (this) {

            //checks
            if (!started) {
                return;
            }

            //request stop
            this.stopRequested = true;

            //interrupt working threads
            for (Thread thread : working) {
                Action.executeSafe(thread::interrupt);
            }

            //copy
            toJoin = new ArrayList<>(working);
        }

        //NOTE: out of sync

        //join working threads
        boolean interrupted = false;
        for (Thread thread : toJoin) {
            try {
                if (Action.executeOrRepeatOnInterruption(thread::join)) {
                    interrupted = true;
                }
            } catch (Throwable t) {
                LOGGER.error("", t);
            }
        }
        if (interrupted) {
            Thread.currentThread().interrupt();
        }

        synchronized (this) {

            //clear state
            this.threadCount = 0;
            submitted.clear();
            working.clear();
            this.stopRequested = false;

            //mark !started
            this.started = false;
        }
    }

    public boolean isStarted() {
        return started;//volatile
    }

    public void submit(boolean ignoreInterruption, @Nonnull Action loopBody) {
        submitN(1, ignoreInterruption, loopBody);
    }

    public synchronized void submitN(int count, boolean ignoreInterruption, @Nonnull Action loopBody) {
        //NOTE: allow submits at non-started state
        for (int i = 0; i < count; i++) {
            this.threadCount++;
            submitted.add(
                new Thread(
                    () -> {
                        try {
                            runThread(ignoreInterruption, loopBody);
                        } finally {
                            removeWorking(Thread.currentThread());
                        }
                    },
                    threadName(count > 1)
                )
            );
        }
    }

    public synchronized void startSubmitted(boolean ignoreStartFailures) throws Exception {

        //checks
        checkState(started, "threads \"%s\" must be started", name);

        //start submitted threads
        for (var i = submitted.iterator(); i.hasNext(); ) {

            //retrieve thread to start
            Thread thread = i.next();
            i.remove();

            //try start
            working.add(thread);
            Throwable t = null;
            try {
                thread.start();
            } catch (Throwable tt) {
                t = tt;
            }

            //failed ?
            if (t != null) {
                working.remove(thread);

                //failure output
                String errorMessage = "thread \"" + thread.getName() + "\" is failed to start";
                if (ignoreStartFailures) {

                    //recover interruption status if necessary
                    if (t instanceof InterruptedException) {
                        Thread.currentThread().interrupt();
                    }

                    //log error
                    LOGGER.error(errorMessage + ", ignore", t);
                } else {

                    //prepare & handle new exception
                    var tt = (t instanceof InterruptedException) ?
                        new IllegalStateException(errorMessage) :
                        new IllegalStateException(errorMessage, t);
                    if (t instanceof InterruptedException) {
                        t.addSuppressed(tt);
                        throw (InterruptedException) t;
                    }
                    throw tt;
                    //NOTE:
                    // State remain correct after exception throw,
                    //  unhandled submitted threads are waiting for next try
                }
            }
        }
    }

    private void runThread(boolean ignoreInterruption, @Nonnull Action loopBody) {
        while (!stopRequested) {//volatile
            try {
                loopBody.execute();
            } catch (InterruptedException ignored) {
                //NOTE: assume that thread interruption flag is clear
                if (!ignoreInterruption) {
                    break;
                }
            } catch (Throwable t) {
                LOGGER.error("", t);
            }
        }
    }

    private synchronized void removeWorking(Thread thread) {
        working.remove(thread);
    }

    @GuardedBy("this")
    private String threadName(boolean hintMultiple) {
        return name + ((hintMultiple || threadCount > 1) ? ("-" + threadCount) : "");
    }
}
