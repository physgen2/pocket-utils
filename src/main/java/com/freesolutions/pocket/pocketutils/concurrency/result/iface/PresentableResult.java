package com.freesolutions.pocket.pocketutils.concurrency.result.iface;

/**
 * @author Stanislau Mirzayeu
 *
 * NOTE: nullability unknown, see derived interfaces, ok
 */
public interface PresentableResult<T> {

    boolean present(T value);
}
