package com.freesolutions.pocket.pocketutils.concurrency.sync.syncable;

import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NullableSyncResult;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public interface Syncable<T> {

    @Nonnull
    NullableSyncResult<T> sync(@Nonnull SyncQuery syncQuery);
}
