package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Expirations;
import com.freesolutions.pocket.pocketutils.actions.Handler;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NullableWritableResultExpirationsExt<T, RESULT extends NullableWritableResult<T>> extends NullableWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofPresentAfterNX(Pipe pipe, long delayMs, @Nullable T value) {
        Expirations.presentAfterNX(pipe, this, delayMs, value);
        return (RESULT) this;
    }

    default RESULT ofPresentAfterNX(Pipe pipe, long deadlineTs, Clock clock, @Nullable T value) {
        Expirations.presentAfterNX(pipe, this, deadlineTs, clock, value);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT ofPresentAfterNX(Pipe pipe, long delayMs, Handler<T> handler) {
        Expirations.presentAfterNX(pipe, this, delayMs, handler);
        return (RESULT) this;
    }

    default RESULT ofPresentAfterNX(Pipe pipe, long deadlineTs, Clock clock, Handler<T> handler) {
        Expirations.presentAfterNX(pipe, this, deadlineTs, clock, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
