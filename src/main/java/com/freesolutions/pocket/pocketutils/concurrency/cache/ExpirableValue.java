package com.freesolutions.pocket.pocketutils.concurrency.cache;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class ExpirableValue<V> {

    public final V value;//NOTE: nullability unknown, ok
    public volatile long expirationTs = Long.MAX_VALUE;

    @Nonnull
    public static <V> ExpirableValue<V> ofInitial(V value) {
        return new ExpirableValue<>(value);
    }

    @Nonnull
    public static <V> ExpirableValue<V> ofTimeout(V value, long timeoutMs, @Nonnull Clock clock) {
        return new ExpirableValue<V>(value, ClockUtils.toDeadlineTs(timeoutMs, clock));
    }

    public boolean hasTimeout() {
        return expirationTs != Long.MAX_VALUE;
    }

    public boolean isExpired(Clock clock) {
        return ClockUtils.isTimedOut(expirationTs, clock);
    }

    @Nonnull
    public ExpirableValue<V> withValue(V value) {
        return new ExpirableValue<>(value, expirationTs);
    }

    @Nonnull
    public ExpirableValue<V> setTimeout(long timeoutMs, @Nonnull Clock clock) {
        this.expirationTs = ClockUtils.toDeadlineTs(timeoutMs, clock);
        return this;
    }

    @Nonnull
    public ExpirableValue<V> resetTimeout() {
        this.expirationTs = Long.MAX_VALUE;
        return this;
    }

    public V getValue(@Nonnull Clock clock, V defaultValue) {
        return !ClockUtils.isTimedOut(expirationTs, clock) ? value : defaultValue;
    }

    private ExpirableValue(V value) {
        this.value = value;
    }

    private ExpirableValue(V value, long expirationTs) {
        this.value = value;
        this.expirationTs = expirationTs;
    }
}
