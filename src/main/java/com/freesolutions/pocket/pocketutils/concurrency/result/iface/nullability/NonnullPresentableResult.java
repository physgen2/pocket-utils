package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.PresentableResult;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public interface NonnullPresentableResult<T> extends PresentableResult<T> {

    boolean present(@Nonnull T value);
}
