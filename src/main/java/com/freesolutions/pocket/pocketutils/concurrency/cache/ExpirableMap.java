package com.freesolutions.pocket.pocketutils.concurrency.cache;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.Handler;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataHandler;
import com.freesolutions.pocket.pocketutils.concurrency.threads.IntervalStrategy;
import com.freesolutions.pocket.pocketutils.concurrency.threads.LoopRunnable;
import com.freesolutions.pocket.pocketutils.concurrency.threads.ThreadBucket;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.time.Clock;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Stanislau Mirzayeu
 */
public class ExpirableMap<K, V> extends ConcurrentHashMap<K, V> implements Startable {

    @Immutable
    public static class Config<K, V> {

        @Nonnull
        public final String name;
        public final long checkIntervalMs;
        @Nonnull
        public final NonnullDataHandler<Entry<K, V>, Long> toExpirationTs;
        @Nonnull
        public final NonnullDataAction<Entry<K, V>> postRemovalAction;
        @Nonnull
        public final Clock clock;

        public Config(
            @Nonnull String name,
            long checkIntervalMs,
            @Nonnull NonnullDataHandler<Entry<K, V>, Long> toExpirationTs,
            @Nonnull NonnullDataAction<Entry<K, V>> postRemovalAction,
            @Nonnull Clock clock
        ) {
            this.name = name;
            this.checkIntervalMs = checkIntervalMs;
            this.toExpirationTs = toExpirationTs;
            this.postRemovalAction = postRemovalAction;
            this.clock = clock;
        }
    }

    private static final int EXPIRATION_THREADS_COUNT = 1;

    @Nonnull
    private final Config<K, V> config;
    @Nonnull
    private final ThreadBucket expirator;

    public ExpirableMap(@Nonnull Config<K, V> config) {
        this.config = config;
        this.expirator = new ThreadBucket(config.name, EXPIRATION_THREADS_COUNT);
        submitExpirator();
    }

    public ExpirableMap(@Nonnull Config<K, V> config, int initialCapacity) {
        super(initialCapacity);
        this.config = config;
        this.expirator = new ThreadBucket(config.name, EXPIRATION_THREADS_COUNT);
        submitExpirator();
    }

    public ExpirableMap(@Nonnull Config<K, V> config, @Nonnull Map<K, V> source) {
        super(source);
        this.config = config;
        this.expirator = new ThreadBucket(config.name, EXPIRATION_THREADS_COUNT);
        submitExpirator();
    }

    public ExpirableMap(@Nonnull Config<K, V> config, int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        this.config = config;
        this.expirator = new ThreadBucket(config.name, EXPIRATION_THREADS_COUNT);
        submitExpirator();
    }

    public ExpirableMap(@Nonnull Config<K, V> config, int initialCapacity, float loadFactor, int concurrencyLevel) {
        super(initialCapacity, loadFactor, concurrencyLevel);
        this.config = config;
        this.expirator = new ThreadBucket(config.name, EXPIRATION_THREADS_COUNT);
        submitExpirator();
    }

    @Override
    public void start() throws Exception {
        expirator.start();
    }

    @Override
    public void stop() {
        expirator.stop();
    }

    private void submitExpirator() {
        expirator.submit(new LoopRunnable(
            IntervalStrategy.createDefaultPerturbated(config.checkIntervalMs),
            expirator::isStopped,
            this::expire
        ));
    }

    private void expire() throws Exception {
        Iterator<Entry<K, V>> i = entrySet().iterator();
        while (i.hasNext()) {
            Entry<K, V> entry = i.next();
            Entry<K, V> entryCopy = Handler.executeQuietly(
                () -> new SimpleImmutableEntry<>(
                    entry.getKey(),
                    entry.getValue()
                ),
                null
            );
            if (entryCopy != null) {//skip if entry is no longer in the map
                Action.executeSafe(() -> expire(entryCopy, i));
            }
        }
    }

    private void expire(@Nonnull Entry<K, V> entry, @Nonnull Iterator<Entry<K, V>> i) throws Exception {
        Long expirationTs = Handler.executeSafe(
            () -> config.toExpirationTs.handle(entry),
            null
        );
        if (expirationTs != null && ClockUtils.isTimedOut(expirationTs, config.clock)) {
            //NOTE: can fail if entry is no longer in the map.
            //Allow exception to be thrown to avoid calling of removal handler in this case.
            i.remove();
            config.postRemovalAction.execute(entry);//allow exceptions to pop-up, ok
        }
    }
}
