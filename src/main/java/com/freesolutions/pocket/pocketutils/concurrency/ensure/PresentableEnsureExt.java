package com.freesolutions.pocket.pocketutils.concurrency.ensure;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.Result;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullablePresentableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Completions;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Expirations;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface PresentableEnsureExt<RESULT extends NullablePresentableResult<Void>> extends NullablePresentableResult<Void> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofCompletionOne(Pipe pipe, Result<?> result) {
        Completions.toCompletionOne(pipe, this, result);
        return (RESULT) this;
    }

    default RESULT ofCompletionTwo(Pipe pipe, Result<?> result1, Result<?> result2) {
        Completions.toCompletionTwo(pipe, this, result1, result2);
        return (RESULT) this;
    }

    default RESULT ofCompletion(Pipe pipe, Result<?> ... results) {
        Completions.toCompletion(pipe, this, results);
        return (RESULT) this;
    }

    default RESULT ofCompletion(Pipe pipe, Iterable<? extends Result<?>> results) {
        Completions.toCompletion(pipe, this, results);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT ofPresentAfterNX(Pipe pipe, long delayMs) {
        Expirations.presentAfterNX(pipe, this, delayMs, null);
        return (RESULT) this;
    }

    default RESULT ofPresentAfterNX(Pipe pipe, long deadlineTs, Clock clock) {
        Expirations.presentAfterNX(pipe, this, deadlineTs, clock, null);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
