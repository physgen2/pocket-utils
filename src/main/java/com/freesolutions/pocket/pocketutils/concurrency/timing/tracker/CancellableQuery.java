package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Cancellable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class CancellableQuery extends Query implements Cancellable {

    @Nonnull
    public static CancellableQuery of(@Nonnull Pipe pipe, long delayMs) {
        return of(pipe, delayMs, Action.IDENTITY);
    }

    @Nonnull
    public static CancellableQuery of(@Nonnull Pipe pipe, long delayMs, @Nonnull Action action) {
        return of(pipe, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, action);
    }

    @Nonnull
    public static CancellableQuery of(@Nonnull Pipe pipe, long deadlineTs, @Nonnull Clock clock) {
        return of(pipe, deadlineTs, clock, Action.IDENTITY);
    }

    @Nonnull
    public static CancellableQuery of(@Nonnull Pipe pipe, long deadlineTs, @Nonnull Clock clock, @Nonnull Action action) {
        return new CancellableQuery(
            pipe,
            ClockUtils.convertTs(deadlineTs, clock, ClockUtils.UNI_CLOCK),
            action
        );
    }

    @Nonnull
    @Override
    public CancellableQuery useAction(@Nonnull Action action) {
        return (CancellableQuery) super.useAction(action);
    }

    @Override
    public boolean isSilent() {
        return isFinal();
    }

    @Override
    public boolean cancel() {
        if (!super.cancel()) {
            return false;
        }
        notifySilent();
        return true;
    }

    @Override
    public synchronized void join() throws InterruptedException {
        ClockUtils.waitTimeout(null, this, Cancellable::isSilent);
    }

    protected CancellableQuery(@Nonnull Pipe pipe, long uniDeadlineTs, @Nonnull Action action) {
        super(pipe, uniDeadlineTs, action);
    }

    @Override
    protected void finishExecution() {
        super.finishExecution();
        notifySilent();
    }

    private synchronized void notifySilent() {
        notifyAll();
    }
}
