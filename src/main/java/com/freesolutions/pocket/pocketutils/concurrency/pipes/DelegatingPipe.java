package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.queue.LinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.queue.NLQueue;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicInteger;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class DelegatingPipe implements Pipe {

    private static final int ENABLED_SIZE = -1;

    @Nonnull
    private final Pipe delegate;

    @Nonnull
    private final NLQueue<Action> deferred = new LinkedNLQueue<>();
    @Nonnull
    private final AtomicInteger deferredSize = new AtomicInteger(0);//disabled by default, ok

    @Nonnull
    public static DelegatingPipe of(@Nonnull Pipe delegate) {
        return new DelegatingPipe(delegate);
    }

    @Override
    public void add(@Nonnull Action action) {
        while (true) {
            int size = deferredSize.get();
            if (size == ENABLED_SIZE) {//if enabled
                delegate.add(action);
                return;
            }
            if (deferredSize.compareAndSet(size, size + 1)) {
                deferred.add(action);
                return;
            }
        }
    }

    public void enable() {
        int size = deferredSize.get();
        while (size != ENABLED_SIZE) {
            if (size > 0) {
                if (deferredSize.compareAndSet(size, size - 1)) {
                    while (true) {
                        Action action = deferred.poll();
                        if (action != null) {
                            delegate.add(action);
                            break;
                        }
                        Thread.yield();
                    }
                } else {
                    Thread.yield();
                }
            } else {
                checkState(size == 0);
                if (deferredSize.compareAndSet(0, ENABLED_SIZE)) {
                    return;
                }
                Thread.yield();
            }
            size = deferredSize.get();
        }
    }

    public void disable() {
        //NOTE: How disabling works?
        //Here we assume that deferred queue is empty, size is negative, and we are completely enabled.
        //So just set size to zero to mark that we are disabled.
        //If size was not negative before, then:
        //1) we have been already disabled, so do nothing.
        //2) or enabling is in progress right now from disabled state, so do nothing too,
        //   because we can treat our call as occurred before enabling due to race.
        deferredSize.compareAndSet(ENABLED_SIZE, 0);
    }

    private DelegatingPipe(@Nonnull Pipe delegate) {
        this.delegate = delegate;
    }
}
