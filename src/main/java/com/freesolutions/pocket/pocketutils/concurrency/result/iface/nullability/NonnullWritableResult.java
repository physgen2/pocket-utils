package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.WritableResult;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public interface NonnullWritableResult<T> extends WritableResult<T>, NonnullPresentableResult<T> {

    boolean assignFrom(@Nonnull NonnullReadableResult<T> source);

    boolean assignFromIfPresent(@Nonnull NonnullReadableResult<T> source);

    boolean assignFromIfNotPresent(@Nonnull NullableReadableResult<?> source);//source is nullable, ok
}
