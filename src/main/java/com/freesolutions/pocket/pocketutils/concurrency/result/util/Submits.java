package com.freesolutions.pocket.pocketutils.concurrency.result.util;

import com.freesolutions.pocket.pocketutils.actions.*;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class Submits {

    //---------------------------------------------------------------------------------------------

    public static <S> void submitNS(Pipe pipe, @Nullable S source, DataAction<S> handler) {
        pipe.add(() -> Handlers.handleSafeNS(source, handler));
    }

    public static <S> void submitS(Pipe pipe, S source, NonnullDataAction<S> handler) {
        pipe.add(() -> Handlers.handleSafeS(source, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void submitNR(Pipe pipe, NullableReadableResult<T> source, NonnullDataAction<NullableReadableResult<T>> handler) {
        source.whenDone(pipe, () -> Handlers.handleSafeS(source, handler));
    }

    public static <T> void submitR(Pipe pipe, NonnullReadableResult<T> source, NonnullDataAction<NonnullReadableResult<T>> handler) {
        source.whenDone(pipe, () -> Handlers.handleSafeS(source, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void submitNX(Pipe pipe, NullableReadableResult<T> source, DataAction<T> handler) {
        source.whenDone(pipe, () -> Handlers.handleSafeS(source, Handlers.handlerNX(handler)));
    }

    public static <T> void submitX(Pipe pipe, NonnullReadableResult<T> source, NonnullDataAction<T> handler) {
        source.whenDone(pipe, () -> Handlers.handleSafeS(source, Handlers.handlerX(handler)));
    }

    //---------------------------------------------------------------------------------------------

    public static <R> void submit2NR(Pipe pipe, NullableWritableResult<R> target, NonnullHandler<NullableReadableResult<R>> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2NR(pipe, target, handler));
    }

    public static <R> void submit2R(Pipe pipe, NonnullWritableResult<R> target, NonnullHandler<NonnullReadableResult<R>> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2R(pipe, target, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <R> void submit2NX(Pipe pipe, NullableWritableResult<R> target, Handler<R> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2NX(pipe, target, handler));
    }

    public static <R> void submit2X(Pipe pipe, NonnullWritableResult<R> target, NonnullHandler<R> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2X(pipe, target, handler));
    }

    //---------------------------------------------------------------------------------------------

    public static <S, R> void submitNS2NR(Pipe pipe, @Nullable S source, NullableWritableResult<R> target, ToNonnullDataHandler<S, NullableReadableResult<R>> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2NR(pipe, target, () -> handler.handle(source)));
    }

    public static <S, R> void submitNS2R(Pipe pipe, @Nullable S source, NonnullWritableResult<R> target, ToNonnullDataHandler<S, NonnullReadableResult<R>> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2R(pipe, target, () -> handler.handle(source)));
    }

    public static <S, R> void submitS2NR(Pipe pipe, S source, NullableWritableResult<R> target, NonnullDataHandler<S, NullableReadableResult<R>> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2NR(pipe, target, () -> handler.handle(source)));
    }

    public static <S, R> void submitS2R(Pipe pipe, S source, NonnullWritableResult<R> target, NonnullDataHandler<S, NonnullReadableResult<R>> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2R(pipe, target, () -> handler.handle(source)));
    }

    //---------------------------------------------------------------------------------------------

    public static <S, R> void submitNS2NX(Pipe pipe, @Nullable S source, NullableWritableResult<R> target, DataHandler<S, R> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2NX(pipe, target, () -> handler.handle(source)));
    }

    public static <S, R> void submitNS2X(Pipe pipe, @Nullable S source, NonnullWritableResult<R> target, ToNonnullDataHandler<S, R> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2X(pipe, target, () -> handler.handle(source)));
    }

    public static <S, R> void submitS2NX(Pipe pipe, S source, NullableWritableResult<R> target, FromNonnullDataHandler<S, R> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2NX(pipe, target, () -> handler.handle(source)));
    }

    public static <S, R> void submitS2X(Pipe pipe, S source, NonnullWritableResult<R> target, NonnullDataHandler<S, R> handler) {
        pipe.add(() -> Handlers.handleAndDelegate2X(pipe, target, () -> handler.handle(source)));
    }

    //---------------------------------------------------------------------------------------------
}
