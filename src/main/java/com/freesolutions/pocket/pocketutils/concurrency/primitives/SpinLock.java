package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicInteger;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class SpinLock extends Arb<SpinLock.LockedState> {

    public class Locker implements AutoCloseable {
        public Locker() {
            lock();
        }

        @Override
        public void close() {//without exception in declaration
            unlock();
        }
    }

    protected static class LockedState {
        public static final int INITIAL_COUNTER = 1;
        @Nonnull
        public final Thread thread;
        @Nonnull
        public final AtomicInteger counter;

        public LockedState(@Nonnull Thread thread) {
            this.thread = thread;
            this.counter = new AtomicInteger(INITIAL_COUNTER);
        }
    }

    public boolean isLocked() {
        return arbGet() != null;
    }

    @Nonnull
    public Locker openLocker() {
        return new Locker();
    }

    public int tryLock() {
        Thread currentThread = Thread.currentThread();
        LockedState lockedState = lockedByThread(currentThread);
        if (lockedState != null) {
            return lockedState.counter.incrementAndGet();
        }
        lockedState = new LockedState(currentThread);
        if (arbCompareAndSet(null, lockedState)) {
            return LockedState.INITIAL_COUNTER;
        }
        return -1;//failed to lock, ok
    }

    public int lock() {
        Thread currentThread = Thread.currentThread();
        LockedState lockedState = lockedByThread(currentThread);
        if (lockedState != null) {
            return lockedState.counter.incrementAndGet();
        }
        lockedState = new LockedState(currentThread);
        while (true) {
            if (arbCompareAndSet(null, lockedState)) {
                return LockedState.INITIAL_COUNTER;
            }
            Thread.yield();
            waitFree();
        }
    }

    public int unlock() {
        LockedState lockedState = lockedByThread(Thread.currentThread());
        if (lockedState == null) {
            return -1;//fail
        }
        int result = lockedState.counter.decrementAndGet();
        if (result < LockedState.INITIAL_COUNTER) {
            boolean ok = arbCompareAndSet(lockedState, null);
            checkState(ok);
        }
        return result;
    }

    public void waitFree() {
        LockedState lockedState = arbGet();
        if (lockedState != null) {
            do {
                Thread.yield();
                lockedState = arbGet();
            } while (lockedState != null);
        }
    }

    @Nullable
    private LockedState lockedByThread(@Nonnull Thread thread) {
        LockedState lockedState = arbGet();
        return (lockedState != null && lockedState.thread == thread) ? lockedState : null;
    }
}
