package com.freesolutions.pocket.pocketutils.concurrency.timing.tracker;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Tracker {

    void query(@Nonnull Query query);
}
