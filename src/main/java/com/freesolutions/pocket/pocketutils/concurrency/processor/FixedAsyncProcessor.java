package com.freesolutions.pocket.pocketutils.concurrency.processor;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.QueueStore;
import com.freesolutions.pocket.pocketutils.concurrency.loop.Threads;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class FixedAsyncProcessor<Q extends QueueStore<Action>> implements QueueBasedAsyncProcessor<Q> {

    @Nonnull
    private final Threads threads;
    @Nonnull
    private final Q actionQueue;

    public FixedAsyncProcessor(
        @Nonnull String name,
        int threadCount,
        @Nonnull Q actionQueue
    ) {
        this.threads = new Threads(
            name,
            threadCount,
            true, //ignoreInterruption
            () -> actionQueue.take().execute()
        );
        this.actionQueue = actionQueue;
    }

    @Override
    public void add(@Nonnull Action action) {
        actionQueue.add(action);
    }

    @Nonnull
    @Override
    public Q actionQueue() {
        return actionQueue;
    }

    @Override
    public void start() throws Exception {
        threads.start();
    }

    @Override
    public void stop() {
        threads.stop();
    }
}
