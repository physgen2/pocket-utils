package com.freesolutions.pocket.pocketutils.concurrency.feed.iface;

/**
 * @author Stanislau Mirzayeu
 *
 * Short case for group class to indicate "no group"
 */
public enum NoGroup {
    VALUE;
}
