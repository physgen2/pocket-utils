package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Cancellable;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class CancellablePipe implements Pipe, Cancellable {

    @Nonnull
    private final Pipe delegate;

    @Nonnull
    private final AtomicBoolean cancelled = new AtomicBoolean();
    @Nonnull
    private final AtomicInteger active = new AtomicInteger();

    @Nonnull
    public static CancellablePipe of(@Nonnull Pipe delegate) {
        return new CancellablePipe(delegate);
    }

    @Override
    public void add(@Nonnull Action action) {
        if (!cancelled.get()) {
            delegate.add(wrapAction(action));
        }
    }

    @Override
    public boolean isSilent() {
        return active.get() < 0;
    }

    @Override
    public boolean cancel() {
        if (!cancelled.compareAndSet(false, true)) {
            return false;
        }
        if (active.getAndUpdate(x -> x < 0 ? x : x - 1) == 0) { //decrement
            notifySilent();//changed from 0 to -1 => became silent
        }
        return true;
    }

    @Override
    public synchronized void join() throws InterruptedException {
        ClockUtils.waitTimeout(null, this, Cancellable::isSilent);
    }

    private CancellablePipe(@Nonnull Pipe delegate) {
        this.delegate = delegate;
    }

    @Nonnull
    private Action wrapAction(@Nonnull Action action) {
        return () -> {
            if (active.getAndUpdate(x -> x < 0 ? x : x + 1) >= 0) { //try increment
                try {
                    action.execute();
                } finally {
                    if (active.getAndUpdate(x -> x < 0 ? x : x - 1) == 0) { //decrement
                        notifySilent();//changed from 0 to -1 => became silent
                    }
                }
            }
        };
    }

    private synchronized void notifySilent() {
        notifyAll();
    }
}
