package com.freesolutions.pocket.pocketutils.concurrency.result;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public enum Status {

    //important to have value 0 for PENDING status as default initial
    PENDING     (0, false, false, false, false, false, false, false),
    PRESENT     (1, true,  true,  false, false, false, false, false),
    ERROR       (2, true,  false, true,  true,  false, false, false),
    CANCELLED   (3, true,  false, false, true,  true,  false, false),
    TIMED_OUT   (4, true,  false, false, true,  true,  true,  false),
    INTERRUPTED (5, true,  false, false, true,  true,  false, true);

    public final int value;

    public final boolean done;
    public final boolean present;
    public final boolean error;
    public final boolean failed;
    public final boolean cancelled;
    public final boolean timedOut;
    public final boolean interrupted;

    private static final Status[] BY_VALUE = calculateByValue();

    @Nonnull
    public static Status ofValue(int value) {
        return BY_VALUE[value];
    }

    Status(
            int value,
            boolean done,
            boolean present,
            boolean error,
            boolean failed,
            boolean cancelled,
            boolean timedOut,
            boolean interrupted
    ) {
        this.value = value;
        this.done = done;
        this.present = present;
        this.error = error;
        this.failed = failed;
        this.cancelled = cancelled;
        this.timedOut = timedOut;
        this.interrupted = interrupted;
    }

    @Nonnull
    private static Status[] calculateByValue() {
        Status[] result = new Status[values().length];
        for (Status status : values()) {
            result[status.value] = status;
        }
        for (Status status : result) {
            if (status == null) {
                throw new IllegalStateException("Incorrect status values");
            }
        }
        return result;
    }
}
