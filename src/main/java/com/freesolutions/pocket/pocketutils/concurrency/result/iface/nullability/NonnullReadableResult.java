package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public interface NonnullReadableResult<T> extends NullableReadableResult<T> {

    @Nonnull
    T value();

    @Nonnull
    T syncedValue() throws Exception;
}
