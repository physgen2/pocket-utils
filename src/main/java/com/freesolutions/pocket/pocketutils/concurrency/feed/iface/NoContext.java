package com.freesolutions.pocket.pocketutils.concurrency.feed.iface;

/**
 * @author Stanislau Mirzayeu
 *
 * Short case for context class to indicate "no context"
 */
public enum NoContext {
    VALUE;
}
