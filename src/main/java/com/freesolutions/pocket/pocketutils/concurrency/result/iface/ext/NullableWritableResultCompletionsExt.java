package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.Result;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Completions;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NullableWritableResultCompletionsExt<T, RESULT extends NullableWritableResult<T>> extends NullableWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofCompletionOne(Pipe pipe, Result<?> result) {
        Completions.toCompletionOne(pipe, this, result);
        return (RESULT) this;
    }

    default RESULT ofCompletionTwo(Pipe pipe, Result<?> result1, Result<?> result2) {
        Completions.toCompletionTwo(pipe, this, result1, result2);
        return (RESULT) this;
    }

    default RESULT ofCompletion(Pipe pipe, Result<?> ... results) {
        Completions.toCompletion(pipe, this, results);
        return (RESULT) this;
    }

    default RESULT ofCompletion(Pipe pipe, Iterable<? extends Result<?>> results) {
        Completions.toCompletion(pipe, this, results);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
