package com.freesolutions.pocket.pocketutils.concurrency.loop;

import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Tracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class LoopBucket implements Startable {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoopBucket.class);

    //NOTE: all state is guarded by this

    @Nonnull
    private final String name;

    private volatile boolean started;

    private int loopCount;
    private final List<Loop> submitted = new LinkedList<>();
    private final List<Loop> working = new LinkedList<>();

    public LoopBucket(@Nonnull String name) {
        this.name = name;
    }

    @Override
    public synchronized void start() throws Exception {

        //check
        if (started) {
            return;
        }

        //mark started
        this.started = true;

        //start submitted, allow start failures
        try {
            startSubmitted(false);
        } catch (Throwable t) {
            stop();
            throw t;
        }
    }

    @Override
    public void stop() {
        //NOTE: stop method must work without exceptions

        List<Loop> toStop;
        synchronized (this) {

            //checks
            if (!started) {
                return;
            }

            //copy
            toStop = new ArrayList<>(working);
        }

        //NOTE: out of sync

        //stop all working loops
        toStop.forEach(Startable::stop);

        synchronized (this) {

            //clear state
            this.loopCount = 0;
            submitted.clear();
            working.clear();

            //mark !started
            this.started = false;
        }
    }

    public boolean isStarted() {
        return started;//volatile
    }

    public void submitThreadLoop(@Nonnull LoopConfig config) {
        submitNThreadLoop(1, config);
    }

    public synchronized void submitNThreadLoop(int count, @Nonnull LoopConfig config) {
        //NOTE: allow submits at non-started state
        for (int i = 0; i < count; i++) {
            this.loopCount++;
            submitted.add(
                new ThreadLoop(
                    loopName(count > 1),
                    config
                ).finishHook(this::removeWorking)
            );
        }
    }

    public void submitAsyncLoop(@Nonnull Pipe pipe, @Nullable Tracker tracker, @Nonnull LoopConfig config) {
        submitNAsyncLoop(1, pipe, tracker, config);
    }

    public synchronized void submitNAsyncLoop(int count, @Nonnull Pipe pipe, @Nullable Tracker tracker, @Nonnull LoopConfig config) {
        //NOTE: allow submits at non-started state
        for (int i = 0; i < count; i++) {
            this.loopCount++;
            submitted.add(
                new AsyncLoop(
                    pipe,
                    loopName(count > 1),
                    config
                ).tracker(tracker).finishHook(this::removeWorking)
            );
        }
    }

    public synchronized void startSubmitted(boolean ignoreStartFailures) throws Exception {

        //checks
        checkState(started, "loop bucket \"%s\" must be started", name);

        //start submitted loops
        for (var i = submitted.iterator(); i.hasNext(); ) {

            //retrieve loop to start
            Loop loop = i.next();
            i.remove();

            //try start
            working.add(loop);
            Throwable t = null;
            try {
                loop.start();
            } catch (Throwable tt) {
                t = tt;
            }

            //failed ?
            if (t != null) {
                working.remove(loop);

                //failure output
                String errorMessage = "loop \"" + loop.name() + "\" is failed to start";
                if (ignoreStartFailures) {

                    //recover interruption status if necessary
                    if (t instanceof InterruptedException) {
                        Thread.currentThread().interrupt();
                    }

                    //log error
                    LOGGER.error(errorMessage + ", ignore", t);
                } else {

                    //prepare & handle new exception
                    var tt = (t instanceof InterruptedException) ?
                        new IllegalStateException(errorMessage) :
                        new IllegalStateException(errorMessage, t);
                    if (t instanceof InterruptedException) {
                        t.addSuppressed(tt);
                        throw (InterruptedException) t;
                    }
                    throw tt;
                    //NOTE:
                    // State remain correct after exception throw,
                    //  unhandled submitted loops are waiting for next try
                }
            }
        }
    }

    private synchronized void removeWorking(Loop loop) {
        working.remove(loop);
    }

    @GuardedBy("this")
    private String loopName(boolean hintMultiple) {
        return name + ((hintMultiple || loopCount > 1) ? ("-" + loopCount) : "");
    }
}
