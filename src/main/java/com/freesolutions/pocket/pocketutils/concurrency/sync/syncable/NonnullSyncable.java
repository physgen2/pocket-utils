package com.freesolutions.pocket.pocketutils.concurrency.sync.syncable;

import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NonnullSyncResult;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public interface NonnullSyncable<T> extends Syncable<T> {

    @Nonnull
    NonnullSyncResult<T> sync(@Nonnull SyncQuery syncQuery);
}
