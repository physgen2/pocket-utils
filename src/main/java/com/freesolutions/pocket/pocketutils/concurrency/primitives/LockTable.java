package com.freesolutions.pocket.pocketutils.concurrency.primitives;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated //use <CountingMap> instead
@ThreadSafe
public class LockTable<K> {

    @Nonnull
    private final ConcurrentMap<K, LockObject> lockHolders = new ConcurrentHashMap<>();

    private static class LockObject {
        @Nonnull
        private final AtomicInteger refCount = new AtomicInteger(1);//initial

        public boolean isValid() {
            return refCount.get() > 0;
        }

        public boolean increment() {
            while (true) {
                int v = refCount.get();
                if (v == 0) {
                    return false;
                }
                if (refCount.compareAndSet(v, v + 1)) {
                    return true;
                }
            }
        }

        public boolean decrement() {
            while (true) {
                int v = refCount.get();
                if (v == 0) {
                    return false;
                }
                if (refCount.compareAndSet(v, v - 1)) {
                    return v > 1;
                }
            }
        }
    }

    @Nullable
    public Object createLockIfNotExist(@Nonnull K key) {
        LockObject lockObject = new LockObject();
        while (true) {
            LockObject existing = lockHolders.putIfAbsent(key, lockObject);
            if (existing == null) {
                return lockObject;
            }
            if (existing.isValid()) {
                return null;
            }
            lockHolders.remove(key, existing);
        }
    }

    @Nonnull
    public Object getOrCreateLock(@Nonnull K key) {
        LockObject lockObject = lockHolders.get(key);
        while (true) {
            if (lockObject != null) {
                if (lockObject.increment()) {
                    return lockObject;
                }
                lockHolders.remove(key, lockObject);
                lockObject = lockHolders.get(key);
            } else {
                lockObject = new LockObject();
                LockObject existing = lockHolders.putIfAbsent(key, lockObject);
                if (existing == null) {
                    return lockObject;
                }
                lockObject = existing;
            }
        }
    }

    public void returnLock(@Nonnull K key, @Nonnull Object lock) {
        LockObject lockObject = (LockObject) lock;
        if (!lockObject.decrement()) {
            lockHolders.remove(key, lockObject);
        }
    }
}
