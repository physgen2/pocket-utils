package com.freesolutions.pocket.pocketutils.concurrency.topic.impl.consumer;

import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;
import com.freesolutions.pocket.pocketutils.concurrency.queue.SyncableLinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Consumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.ManualConsumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Topic;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.function.Predicate;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class SyncableQueueManualConsumer<T> extends SyncableLinkedNLQueue<T> implements ManualConsumer<T> {

    @Nonnull
    private final Topic<T> topic;
    @Nonnull
    private final Predicate<T> filter;
    @Nonnull
    private final NonnullDataAction<Consumer<T>> starter;
    @Nonnull
    private final NonnullDataAction<Consumer<T>> stopper;

    public SyncableQueueManualConsumer(
        @Nonnull Topic<T> topic,
        @Nonnull Predicate<T> filter,
        @Nonnull NonnullDataAction<Consumer<T>> starter,
        @Nonnull NonnullDataAction<Consumer<T>> stopper
    ) {
        this.topic = topic;
        this.filter = filter;
        this.starter = starter;
        this.stopper = stopper;
    }

    @Override
    public void start() throws Exception {
        starter.execute(this);
    }

    @Override
    public void stop() {
        NonnullDataAction.executeSafe(stopper, this);
    }

    @Nonnull
    @Override
    public Topic<T> topic() {
        return topic;
    }

    @Nonnull
    @Override
    public Predicate<T> filter() {
        return filter;
    }
}
