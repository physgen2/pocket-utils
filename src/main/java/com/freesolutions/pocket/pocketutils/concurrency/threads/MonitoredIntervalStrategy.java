package com.freesolutions.pocket.pocketutils.concurrency.threads;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated
@ThreadSafe
public interface MonitoredIntervalStrategy extends IntervalStrategy {

    boolean isAwakeRequired() throws Exception;

    boolean isIntervalsEnabled();
}
