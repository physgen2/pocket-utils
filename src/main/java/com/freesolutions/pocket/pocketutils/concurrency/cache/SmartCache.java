package com.freesolutions.pocket.pocketutils.concurrency.cache;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.primitives.LockTable;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NonnullImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NullableFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Handlers;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncStatus;
import com.freesolutions.pocket.pocketutils.concurrency.threads.IntervalStrategy;
import com.freesolutions.pocket.pocketutils.concurrency.threads.LoopRunnable;
import com.freesolutions.pocket.pocketutils.concurrency.threads.ThreadBucket;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Clock;
import java.util.Iterator;
import java.util.Map;
import java.util.OptionalLong;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Deprecated //use <GentlemanCache> instead
@ThreadSafe
public class SmartCache<K, V> implements Startable {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmartCache.class);

    private static final Clock CLOCK = ClockUtils.UNI_CLOCK;
    private static final long EXPIRATION_CHECK_INTERVAL_MS = TimeUnit.SECONDS.toMillis(1);//1 sec

    public interface Loader<K, V> {
        @Nonnull
        NonnullResult<V> load(@Nonnull K key, @Nullable V oldValue) throws Exception;
    }

    public static class ValueHolder<V> {
        @Nonnull
        private final AtomicReference<V> value;//internal value always not null
        @Nonnull
        private final AtomicLong lastTouchTs = new AtomicLong(CLOCK.millis());

        private ValueHolder(@Nonnull V initialValue) {
            this.value = new AtomicReference<>(checkNotNull(initialValue));
        }

        @Nonnull
        public V getValue() {
            lastTouchTs.set(CLOCK.millis());
            return checkNotNull(value.get());
        }

        @Nonnull
        public V pryValue() {
            return checkNotNull(value.get());
        }

        public void setValue(@Nonnull V v) {
            lastTouchTs.set(CLOCK.millis());
            value.set(checkNotNull(v));
        }

        public void injectValue(@Nonnull V v) {
            value.set(checkNotNull(v));
        }

        public boolean compareAndSet(@Nonnull V expect, @Nonnull V update) {
            lastTouchTs.set(CLOCK.millis());
            return value.compareAndSet(expect, update);
        }

        @Nonnull
        public V getAndSet(@Nonnull V newValue) {
            lastTouchTs.set(CLOCK.millis());
            return value.getAndSet(newValue);
        }

        private boolean isExpired(long timeoutMs, long now) {
            return now >= lastTouchTs.get() + timeoutMs;
        }
    }

    @Nonnull
    private final ConcurrentMap<K, ValueHolder<V>> values = new ConcurrentHashMap<>();
    @Nonnull
    private final LockTable<K> loadingLocks = new LockTable<>();

    @Nonnull
    private final ThreadBucket expirationThread;
    @Nonnull
    private final Loader<K, V> loader;
    @Nonnull
    private final OptionalLong timeoutMs;

    public SmartCache(
        @Nonnull String name,
        @Nonnull Loader<K, V> loader,
        @Nonnull OptionalLong timeoutMs
    ) {
        this.expirationThread = new ThreadBucket(name + "-expiration-thread", 1);
        this.loader = loader;
        this.timeoutMs = timeoutMs;
    }

    @Override
    public void start() throws Exception {
        //don't submit expiration thread if no timeout present
        if (timeoutMs.isPresent()) {
            expirationThread.submit(new LoopRunnable(
                IntervalStrategy.createDefaultPerturbated(EXPIRATION_CHECK_INTERVAL_MS),
                expirationThread::isStopped,
                this::expirationPass
            ));
        }
        expirationThread.start();
    }

    @Override
    public void stop() {
        expirationThread.stop();
    }

    //basic methods
    //---------------------------------------------------------------------------------------------

    @Nullable
    public V getIfPresent(@Nonnull K key) {
        ValueHolder<V> vh = getValueHolderIfPresent(key);
        return vh != null ? vh.getValue() : null;
    }

    @Nonnull
    public NonnullResult<V> getOrLoad(@Nonnull Pipe pipe, @Nonnull K key, boolean timeoutIfAlreadyLoading, boolean forceReloadExisting) {
        return NonnullFuture.<V>of().ofTransformX2X(Pipe.DIRECT,
            getOrLoadValueHolder(pipe, key, timeoutIfAlreadyLoading, forceReloadExisting),
            ValueHolder::getValue
        );
    }

    @Nonnull
    public NonnullResult<V> refresh(@Nonnull Pipe pipe, @Nonnull K key, boolean timeoutIfAlreadyLoading, boolean forceReloadExisting) {
        return NonnullFuture.<V>of().ofTransformX2X(Pipe.DIRECT,
            loadValueHolder(pipe, key, timeoutIfAlreadyLoading, forceReloadExisting),
            ValueHolder::getValue
        );
    }

    public void invalidateAll() {
        values.clear();
    }

    public void refreshAll(@Nonnull Pipe pipe, boolean timeoutIfAlreadyLoading, boolean forceReloadExisting) {
        values.forEach((k, vh) -> loadValueHolder(pipe, k, timeoutIfAlreadyLoading, forceReloadExisting).gag(pipe));
    }

    @Nonnull
    public Map<K, V> copyToMap(@Nonnull Map<K, V> targetMap) {
        values.forEach((k, vh) -> targetMap.put(k, vh.getValue()));
        return targetMap;
    }

    @Nonnull
    public Map<K, V> pryCopyToMap(@Nonnull Map<K, V> targetMap) {
        values.forEach((k, vh) -> targetMap.put(k, vh.pryValue()));
        return targetMap;
    }

    public void forEachValueHolder(@Nonnull BiConsumer<? super K, ? super ValueHolder<V>> consumer) {
        values.forEach(consumer::accept);
    }

    public void forEach(@Nonnull BiConsumer<? super K, ? super V> consumer) {
        values.forEach((k, vh) -> consumer.accept(k, vh.getValue()));
    }

    public void pryForEach(@Nonnull BiConsumer<? super K, ? super V> consumer) {
        values.forEach((k, vh) -> consumer.accept(k, vh.pryValue()));
    }

    //smart methods
    //---------------------------------------------------------------------------------------------

    @Nullable
    public ValueHolder<V> getValueHolderIfPresent(@Nonnull K key) {
        return values.get(key);
    }

    @Nonnull
    public NonnullResult<ValueHolder<V>> getOrLoadValueHolder(
        @Nonnull Pipe pipe,
        @Nonnull K key,
        boolean timeoutIfAlreadyLoading,
        boolean forceReloadExisting
    ) {
        ValueHolder<V> vh = values.get(key);
        if (vh != null) {
            return NonnullImmutableResult.ofPresent(vh);
        }
        return loadValueHolder(pipe, key, timeoutIfAlreadyLoading, forceReloadExisting);
    }

    @Nonnull
    public NonnullResult<ValueHolder<V>> loadValueHolder(
        @Nonnull Pipe pipe,
        @Nonnull K key,
        boolean timeoutIfAlreadyLoading,
        boolean forceReloadExisting
    ) {
        Object lock = timeoutIfAlreadyLoading ?
            loadingLocks.createLockIfNotExist(key) :
            loadingLocks.getOrCreateLock(key);
        if (lock == null) {
            //other thread already load this key, and we don't want to wait it, so timed out
            return NonnullImmutableResult.ofTimedOut();
        }
        return NonnullFuture.<ValueHolder<V>>of().ofSubmit2R(pipe, () -> {
            try {
                return doLoadAndPut(lock, key, forceReloadExisting);
            } finally {
                loadingLocks.returnLock(key, lock);
            }
        });
    }

    @Nonnull
    public NullableResult<ValueHolder<V>> put(
        @Nonnull Pipe pipe,
        @Nonnull K key,
        @Nonnull V value,
        boolean onlyIfAbsent
    ) {
        Object lock = loadingLocks.getOrCreateLock(key);
        return NullableFuture.<ValueHolder<V>>of().ofSubmit2NX(pipe, () -> {
            try {
                return doPut(lock, key, value, onlyIfAbsent);
            } finally {
                loadingLocks.returnLock(key, lock);
            }
        });
    }

    //---------------------------------------------------------------------------------------------

    @Nonnull
    private NonnullResult<ValueHolder<V>> doLoadAndPut(@Nonnull Object lock, @Nonnull K key, boolean forceReloadExisting) {
        //NOTE: method is sync, async result used only to propagate special values (error/cancellation/interruption)
        synchronized (lock) {
            ValueHolder<V> vh = values.get(key);
            if (vh == null || forceReloadExisting) {
                final ValueHolder<V> vhc = vh;
                NonnullReadableResult<V> vr = Handlers.handleSafe2R(() -> loader.load(key, vhc != null ? vhc.pryValue() : null));
                SyncStatus syncStatus = vr.sync(SyncQuery.infinite()).status();
                if (!syncStatus.ok) {
                    LOGGER.error("Sync failed while waiting loading future, cancel loading, ignore, syncStatus: " + syncStatus);
                    return NonnullImmutableResult.ofCancelled();
                }
                if (!vr.status().present) {
                    return checkNotNull(NonnullImmutableResult.ofResultIfNotPresent(vr));
                }
                if (vh != null) {
                    vh.setValue(vr.value());
                } else {
                    vh = new ValueHolder<>(vr.value());
                }
                values.put(key, vh);//always put to map, because value can be removed by expiration after getting but before update
            }
            return NonnullImmutableResult.ofPresent(checkNotNull(vh));
        }
    }

    @Nullable
    private ValueHolder<V> doPut(@Nonnull Object lock, @Nonnull K key, @Nonnull V value, boolean onlyIfAbsent) {
        synchronized (lock) {
            ValueHolder<V> vh = values.get(key);
            if (vh != null) {
                if (onlyIfAbsent) {
                    return null;
                }
                vh.setValue(value);
            } else {
                vh = new ValueHolder<>(value);
            }
            values.put(key, vh);//always put to map, because value can be removed by expiration after getting but before update
            return vh;
        }
    }

    private void expirationPass() {
        long now = CLOCK.millis();
        for (Iterator<Map.Entry<K, ValueHolder<V>>> i = values.entrySet().iterator(); i.hasNext(); ) {
            Map.Entry<K, ValueHolder<V>> entry = i.next();
            ValueHolder<V> valueHolder = entry.getValue();
            boolean alive = !timeoutMs.isPresent() || !valueHolder.isExpired(timeoutMs.getAsLong(), now);
            if (!alive) {
                i.remove();
            }
        }
    }
}
