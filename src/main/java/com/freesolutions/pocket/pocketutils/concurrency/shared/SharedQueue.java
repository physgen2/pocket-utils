package com.freesolutions.pocket.pocketutils.concurrency.shared;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;
import com.freesolutions.pocket.pocketutils.concurrency.loop.LoopConfig;
import com.freesolutions.pocket.pocketutils.concurrency.loop.ThreadLoop;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.time.InstantTs;
import com.freesolutions.pocket.pocketutils.utils.Pair;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class SharedQueue<ITEM_ID, OFFER_ITEM, FETCH_ITEM, FETCH_CONTEXT> implements Startable {

    @ThreadSafe
    public interface IdGenerator {

        IdGenerator NONE = () -> {
            throw new UnsupportedOperationException("none");
        };

        long generateId();
    }

    @ThreadSafe
    public interface IORoutines<ITEM_ID, OFFER_ITEM, FETCH_ITEM, FETCH_CONTEXT> {

        int size(@Nullable Long queueId);

        void offer(
            @Nonnull InstantTs nowTs,
            @Nullable Long queueId,
            @Nonnull IdGenerator idGenerator,
            @Nonnull Iterable<OFFER_ITEM> offerItems
        ) throws Exception;

        @Nonnull
        Pair<List<ITEM_ID>, List<FETCH_ITEM>> fetch(
            @Nonnull InstantTs nowTs,
            @Nullable Long queueId,
            long lockId,
            @Nullable FETCH_CONTEXT fetchContext
        ) throws Exception;

        void complete(@Nonnull Iterable<ITEM_ID> itemIds, @Nullable Integer itemIdsCount) throws Exception;

        void clean(@Nonnull InstantTs borderTs) throws Exception;
    }

    @Immutable
    public static class Batch<ITEM_ID, FETCH_ITEM> {

        @Nonnull
        public final List<ITEM_ID> itemIds; //unmodifiable
        @Nonnull
        public final List<FETCH_ITEM> items; //unmodifiable

        private Batch(
            @Nonnull List<ITEM_ID> itemIds,
            @Nonnull List<FETCH_ITEM> items
        ) {
            checkArgument(itemIds.size() == items.size());
            this.itemIds = Collections.unmodifiableList(itemIds);
            this.items = Collections.unmodifiableList(items);
        }
    }

    private static final long CLEAN_INTERVAL_MS = TimeUnit.MINUTES.toMillis(5L);
    private static final long CLEAN_ERROR_INTERVAL_MS = TimeUnit.SECONDS.toMillis(5L);

    private final String name;
    @Nullable
    private final Long queueId;
    private final long retentionMs;
    @Nonnull
    private final IdGenerator idGenerator;
    @Nonnull
    private final IORoutines<ITEM_ID, OFFER_ITEM, FETCH_ITEM, FETCH_CONTEXT> ioRoutines;

    @Nonnull
    private final ThreadLoop cleaner;

    public SharedQueue(
        @Nonnull String name,
        @Nullable Long queueId,
        long retentionMs,
        @Nonnull IdGenerator idGenerator,
        @Nonnull IORoutines<ITEM_ID, OFFER_ITEM, FETCH_ITEM, FETCH_CONTEXT> ioRoutines
    ) {
        checkArgument(retentionMs >= 0L, "retentionMs: %s", retentionMs);

        this.name = name;

        this.queueId = queueId;
        this.retentionMs = retentionMs;
        this.idGenerator = idGenerator;
        this.ioRoutines = ioRoutines;

        this.cleaner = new ThreadLoop(
            name + "-cleaner",
            LoopConfig.builder()
                .intervalMs(CLEAN_INTERVAL_MS)
                .defaultIntervalPerturbation()
                .errorDelayMs(CLEAN_ERROR_INTERVAL_MS)
                .loopAction(this::periodicalClean)
                .build()
        );
    }

    @Override
    public void start() throws Exception {
        cleaner.start();
    }

    @Override
    public void stop() {
        cleaner.stop();
    }

    public String name() {
        return name;
    }

    public int size() {
        return ioRoutines.size(queueId);
    }

    public void offer(@Nonnull Iterable<OFFER_ITEM> offerItems) {
        InstantTs nowTs = InstantTs.now(ClockUtils.UTC);
        Action.executeUnchecked(() ->
            ioRoutines.offer(
                nowTs,
                queueId,
                idGenerator,
                offerItems
            )
        );
    }

    @Nonnull
    public Batch<ITEM_ID, FETCH_ITEM> fetch(long lockId) {
        return fetch(lockId, null);
    }

    @Nonnull
    public Batch<ITEM_ID, FETCH_ITEM> fetch(long lockId, @Nullable FETCH_CONTEXT fetchContext) {
        InstantTs nowTs = InstantTs.now(ClockUtils.UTC);
        var r = NonnullHandler.executeUnchecked(() ->
            ioRoutines.fetch(
                nowTs,
                queueId,
                lockId,
                fetchContext
            )
        );
        return new Batch<>(r.left, r.right);
    }

    public void complete(@Nonnull Iterable<ITEM_ID> itemIds) {
        complete(itemIds, null);
    }

    public void complete(@Nonnull Collection<ITEM_ID> itemIds) {
        complete(itemIds, itemIds.size());
    }

    private void complete(@Nonnull Iterable<ITEM_ID> itemIds, @Nullable Integer itemIdsCount) {
        Action.executeUnchecked(() ->
            ioRoutines.complete(itemIds, itemIdsCount)
        );
    }

    private void periodicalClean() {
        InstantTs nowTs = InstantTs.now(ClockUtils.UTC);
        Action.executeUnchecked(() ->
            ioRoutines.clean(nowTs.minusMillis(retentionMs))
        );
    }
}
