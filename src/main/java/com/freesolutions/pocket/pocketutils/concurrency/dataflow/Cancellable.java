package com.freesolutions.pocket.pocketutils.concurrency.dataflow;

import javax.annotation.concurrent.ThreadSafe;

/**
 * Interface to represent abstraction with active internal logic, that can be cancelled. While working we
 * assume that silence is not present and some internal activity is always present. Once cancellation is
 * called internal activity for a while can remain present, but eventually silence will be achieved and
 * any new activity must not arise again.
 *
 * Client can do sync-waiting for silence by calling {@link #join()} method at any time, before or after
 * cancellation, but technically silence will happen only after cancellation.
 *
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Cancellable {

    boolean isSilent();

    boolean cancel();

    void join() throws InterruptedException;

    static void joinUninterruptibly(Cancellable cancellable) {
        boolean interrupted = Thread.interrupted();
        while (true) {
            try {
                cancellable.join();
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
                return;
            } catch (InterruptedException ignored) {
                //NOTE: assume that thread interruption flag is clear
                interrupted = true;
            } catch (Throwable t) {
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
                throw t;
            }
        }
    }
}
