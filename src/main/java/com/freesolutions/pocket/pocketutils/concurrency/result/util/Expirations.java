package com.freesolutions.pocket.pocketutils.concurrency.result.util;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.WritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullPresentableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullablePresentableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.GlobalTracker;
import com.freesolutions.pocket.pocketutils.concurrency.timing.tracker.Query;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.actions.Handler;
import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class Expirations {
    private static final Logger LOGGER = LoggerFactory.getLogger(Expirations.class);

    //---------------------------------------------------------------------------------------------

    public static <T> void presentAfterNX(Pipe pipe, NullablePresentableResult<T> target, long delayMs, @Nullable T value) {
        presentAfterNX(pipe, target, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, value);
    }

    public static <T> void presentAfterNX(Pipe pipe, NullablePresentableResult<T> target, long deadlineTs, Clock clock, @Nullable T value) {
        GlobalTracker.instance().query(Query.of(pipe, deadlineTs, clock, () -> target.present(value)));
    }

    public static <T> void presentAfterX(Pipe pipe, NonnullPresentableResult<T> target, long delayMs, T value) {
        presentAfterX(pipe, target, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, value);
    }

    public static <T> void presentAfterX(Pipe pipe, NonnullPresentableResult<T> target, long deadlineTs, Clock clock, T value) {
        GlobalTracker.instance().query(Query.of(pipe, deadlineTs, clock, () -> target.present(value)));
    }

    //---------------------------------------------------------------------------------------------

    public static <T> void presentAfterNX(Pipe pipe, NullableWritableResult<T> target, long delayMs, Handler<T> handler) {
        presentAfterNX(pipe, target, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, handler);
    }

    public static <T> void presentAfterNX(Pipe pipe, NullableWritableResult<T> target, long deadlineTs, Clock clock, Handler<T> handler) {
        GlobalTracker.instance().query(Query.of(pipe, deadlineTs, clock, () -> Handlers.handleAndDelegate2NX(pipe, target, handler)));
    }

    public static <T> void presentAfterX(Pipe pipe, NonnullWritableResult<T> target, long delayMs, NonnullHandler<T> handler) {
        presentAfterX(pipe, target, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK, handler);
    }

    public static <T> void presentAfterX(Pipe pipe, NonnullWritableResult<T> target, long deadlineTs, Clock clock, NonnullHandler<T> handler) {
        GlobalTracker.instance().query(Query.of(pipe, deadlineTs, clock, () -> Handlers.handleAndDelegate2X(pipe, target, handler)));
    }

    //---------------------------------------------------------------------------------------------

    public static void cancelAfter(Pipe pipe, WritableResult<?> target, long delayMs) {
        cancelAfter(pipe, target, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK);
    }

    public static void cancelAfter(Pipe pipe, WritableResult<?> target, long deadlineTs, Clock clock) {
        GlobalTracker.instance().query(Query.of(pipe, deadlineTs, clock, target::cancel));
    }

    public static void timeoutAfter(Pipe pipe, WritableResult<?> target, long delayMs) {
        timeoutAfter(pipe, target, ClockUtils.toDeadlineTs(delayMs, ClockUtils.UNI_CLOCK), ClockUtils.UNI_CLOCK);
    }

    public static void timeoutAfter(Pipe pipe, WritableResult<?> target, long deadlineTs, Clock clock) {
        GlobalTracker.instance().query(Query.of(pipe, deadlineTs, clock, target::timeout));
    }

    //---------------------------------------------------------------------------------------------
}
