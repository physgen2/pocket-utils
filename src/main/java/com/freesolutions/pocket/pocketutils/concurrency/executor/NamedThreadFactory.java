package com.freesolutions.pocket.pocketutils.concurrency.executor;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class NamedThreadFactory implements ThreadFactory {

    @Nonnull
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    @Nonnull
    private final String name;

    public NamedThreadFactory(@Nonnull String name) {
        this.name = name;
    }

    @Nonnull
    @Override
    public Thread newThread(@Nonnull Runnable runnable) {
        Thread result = new Thread(runnable, name + "-" + threadNumber.getAndIncrement());
        result.setDaemon(false);
        result.setPriority(Thread.NORM_PRIORITY);
        return result;
    }
}
