package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.Result;

import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 */
public interface NullableReadableResult<T> extends Result<T> {

    @Nullable
    T value();

    @Nullable
    T syncedValue() throws Exception;
}
