package com.freesolutions.pocket.pocketutils.concurrency.sync;

import com.freesolutions.pocket.pocketutils.concurrency.timing.ClockUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class SyncQuery {

    private static final SyncQuery INFINITE = new SyncQuery(null, false, ClockUtils.UNI_CLOCK);
    private static final SyncQuery IMMEDIATE = new SyncQuery(null, true, ClockUtils.UNI_CLOCK);

    @Nullable
    public final Long deadlineTs;
    public final boolean immediate;
    @Nonnull
    public final Clock clock;

    @Nonnull
    public static SyncQuery infinite() {
        return INFINITE;
    }

    @Nonnull
    public static SyncQuery immediate() {
        return IMMEDIATE;
    }

    @Nonnull
    public static SyncQuery timeout(long timeoutMs) {
        return timeout(timeoutMs, ClockUtils.UNI_CLOCK);
    }

    @Nonnull
    public static SyncQuery timeout(long timeoutMs, @Nonnull Clock clock) {
        return new SyncQuery(
            ClockUtils.toDeadlineTs(timeoutMs, clock),
            !ClockUtils.isTimeoutExist(timeoutMs),
            ClockUtils.UNI_CLOCK
        );
    }

    @Nonnull
    public static SyncQuery timeout(long timeoutMs, long baseTs, Clock clock) {
        return new SyncQuery(
            ClockUtils.toDeadlineTs(timeoutMs, baseTs),
            !ClockUtils.isTimeoutExist(timeoutMs),
            clock
        );
    }

    @Nonnull
    public static SyncQuery deadline(long deadlineTs, @Nonnull Clock clock) {
        return new SyncQuery(
            deadlineTs,
            ClockUtils.isTimedOut(deadlineTs, clock),
            clock
        );
    }

    @Nonnull
    public static SyncQuery deadline(long deadlineTs, long baseTs, Clock clock) {
        return new SyncQuery(
            deadlineTs,
            ClockUtils.isTimedOut(deadlineTs, baseTs),
            clock
        );
    }

    @Nullable
    public Long toTimeoutMs() {
        if (immediate) {
            return 0L;
        }
        if (deadlineTs == null) {
            return null;
        }
        return ClockUtils.toTimeout(deadlineTs, clock);
    }

    private SyncQuery(@Nullable Long deadlineTs, boolean immediate, @Nonnull Clock clock) {
        this.deadlineTs = deadlineTs;
        this.immediate = immediate;
        this.clock = clock;
    }
}
