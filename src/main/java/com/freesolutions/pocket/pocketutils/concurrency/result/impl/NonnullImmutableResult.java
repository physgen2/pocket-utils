package com.freesolutions.pocket.pocketutils.concurrency.result.impl;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableReadableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class NonnullImmutableResult<T> extends ImmutableResult<T> implements NonnullResult<T> {

    private static final NonnullImmutableResult<?> PENDING = new NonnullImmutableResult<>(Status.PENDING, null);
    private static final NonnullImmutableResult<?> CANCELLED = new NonnullImmutableResult<>(Status.CANCELLED, null);
    private static final NonnullImmutableResult<?> TIMED_OUT = new NonnullImmutableResult<>(Status.TIMED_OUT, null);
    private static final NonnullImmutableResult<?> INTERRUPTED = new NonnullImmutableResult<>(Status.INTERRUPTED, null);

    @Nonnull
    public static <T> NonnullImmutableResult<T> ofResult(@Nonnull NonnullReadableResult<T> source) {
        Status status = source.status();
        return new NonnullImmutableResult<>(status, status.present ? source.value() : status.error ? source.error() : null);
    }

    @Nullable
    public static <T> NonnullImmutableResult<T> ofResultIfPresent(@Nonnull NonnullReadableResult<T> source) {
        Status status = source.status();
        return status.present ? new NonnullImmutableResult<>(status, source.value()) : null;
    }

    @Nullable
    public static <T> NonnullImmutableResult<T> ofResultIfNotPresent(@Nonnull NullableReadableResult<?> source) {//source is nullable, ok
        Status status = source.status();
        return status.done ? (!status.present ? new NonnullImmutableResult<>(status, status.error ? source.error() : null) : null) : null;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullImmutableResult<T> ofPending() {
        return (NonnullImmutableResult<T>) PENDING;
    }

    @Nonnull
    public static <T> NonnullImmutableResult<T> ofPresent(@Nonnull T value) {
        return new NonnullImmutableResult<>(Status.PRESENT, value);
    }

    @Nonnull
    public static <T> NonnullImmutableResult<T> ofError(@Nonnull Throwable t) {
        return new NonnullImmutableResult<>(Status.ERROR, t);
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullImmutableResult<T> ofCancelled() {
        return (NonnullImmutableResult<T>) CANCELLED;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullImmutableResult<T> ofTimedOut() {
        return (NonnullImmutableResult<T>) TIMED_OUT;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NonnullImmutableResult<T> ofInterrupted() {
        return (NonnullImmutableResult<T>) INTERRUPTED;
    }

    @Nonnull
    public T value() {
        return super.value();//delegate to super, just for nullability marker
    }

    @Nonnull
    public T syncedValue() throws Exception {
        return super.syncedValue();//delegate to super, just for nullability marker
    }

    @Override
    public boolean present(@Nonnull T value) {
        return false;//immutable, just for nullability marker, ok
    }

    @Override
    public boolean assignFrom(@Nonnull NonnullReadableResult<T> source) {
        return false;//immutable, ok
    }

    @Override
    public boolean assignFromIfPresent(@Nonnull NonnullReadableResult<T> source) {
        return false;//immutable, ok
    }

    @Override
    public boolean assignFromIfNotPresent(@Nonnull NullableReadableResult<?> source) {
        return false;//immutable, ok
    }

    private NonnullImmutableResult(@Nonnull Status status, Object value) {
        super(status, value);
    }
}
