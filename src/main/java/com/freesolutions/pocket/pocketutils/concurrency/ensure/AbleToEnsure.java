package com.freesolutions.pocket.pocketutils.concurrency.ensure;

import javax.annotation.Nonnull;

/**
 * Interface to represent such functionality when some event, related to implementer, can be ensured.
 * Implementers must provide immediate access to {@link Ensure} object by method {@link #ensured()},
 * which became present after event occurred.
 *
 * Note if event never occur then {@link Ensure} will stay never present.
 * Callers, if necessary, can perform sync or async waiting of returned {@link Ensure}.
 *
 * @author Stanislau Mirzayeu
 */
public interface AbleToEnsure {

    @Nonnull
    Ensure ensured();
}
