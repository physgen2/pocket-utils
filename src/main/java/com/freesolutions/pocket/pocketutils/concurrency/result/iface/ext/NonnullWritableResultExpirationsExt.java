package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NonnullWritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Expirations;
import com.freesolutions.pocket.pocketutils.actions.NonnullHandler;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface NonnullWritableResultExpirationsExt<T, RESULT extends NonnullWritableResult<T>> extends NonnullWritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofPresentAfterX(Pipe pipe, long delayMs, T value) {
        Expirations.presentAfterX(pipe, this, delayMs, value);
        return (RESULT) this;
    }

    default RESULT ofPresentAfterX(Pipe pipe, long deadlineTs, Clock clock, T value) {
        Expirations.presentAfterX(pipe, this, deadlineTs, clock, value);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------

    default RESULT ofPresentAfterX(Pipe pipe, long delayMs, NonnullHandler<T> handler) {
        Expirations.presentAfterX(pipe, this, delayMs, handler);
        return (RESULT) this;
    }

    default RESULT ofPresentAfterX(Pipe pipe, long deadlineTs, Clock clock, NonnullHandler<T> handler) {
        Expirations.presentAfterX(pipe, this, deadlineTs, clock, handler);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
