package com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext;

import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.WritableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.util.Expirations;

import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Clock;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public interface WritableResultExpirationsExt<T, RESULT extends WritableResult<T>> extends WritableResult<T> {

    //---------------------------------------------------------------------------------------------

    default RESULT ofCancelAfter(Pipe pipe, long delayMs) {
        Expirations.cancelAfter(pipe, this, delayMs);
        return (RESULT) this;
    }

    default RESULT ofCancelAfter(Pipe pipe, long deadlineTs, Clock clock) {
        Expirations.cancelAfter(pipe, this, deadlineTs, clock);
        return (RESULT) this;
    }

    default RESULT ofTimeoutAfter(Pipe pipe, long delayMs) {
        Expirations.timeoutAfter(pipe, this, delayMs);
        return (RESULT) this;
    }

    default RESULT ofTimeoutAfter(Pipe pipe, long deadlineTs, Clock clock) {
        Expirations.timeoutAfter(pipe, this, deadlineTs, clock);
        return (RESULT) this;
    }

    //---------------------------------------------------------------------------------------------
}
