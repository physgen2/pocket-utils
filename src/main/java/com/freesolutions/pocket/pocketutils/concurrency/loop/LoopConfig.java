package com.freesolutions.pocket.pocketutils.concurrency.loop;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.NonnullDataAction;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class LoopConfig {

    @NotThreadSafe
    public static class Builder {

        public static final double DEFAULT_PERTURBATION_FRACTION = 0.02;

        private boolean completed;

        @Nullable
        private Long intervalMs;
        @Nullable
        private Double intervalPerturbationMs;
        private boolean skipInitialInvoke;
        @Nullable
        private Long initialInvokeDelayMs;
        private boolean noRepeatOnError;
        @Nullable
        private Long errorDelayMs;
        @Nullable
        private Predicate<Void> stopCondition;
        private boolean ignoreInterruption;
        private boolean syncStart;
        @Nullable
        private Action loopAction;
        @Nullable
        private NonnullDataAction<Loop> loopActionWithLoopArg;
        @Nullable
        private Action startAction;
        @Nullable
        private Action stopAction;

        @Nonnull
        public Builder intervalMs(long intervalMs) {
            checkState(!completed);
            checkArgument(intervalMs >= 0L);
            checkArgument(this.intervalMs == null);
            this.intervalMs = intervalMs;
            return this;
        }

        @Nonnull
        public Builder intervalImmediateRepeat() {
            return intervalMs(0L);
        }

        @Nonnull
        public Builder intervalPerturbationMs(double intervalPerturbationMs) {
            checkState(!completed);
            checkArgument(intervalMs != null);
            checkArgument(intervalPerturbationMs >= 0.0 && intervalPerturbationMs <= intervalMs);
            this.intervalPerturbationMs = intervalPerturbationMs;
            return this;
        }

        @Nonnull
        public Builder intervalPerturbationFraction(double intervalPerturbationFraction) {
            checkState(!completed);
            checkArgument(intervalMs != null);
            checkArgument(intervalPerturbationFraction >= 0.0 && intervalPerturbationFraction <= 1.0);
            this.intervalPerturbationMs = intervalMs * intervalPerturbationFraction;
            return this;
        }

        @Nonnull
        public Builder defaultIntervalPerturbation() {
            checkState(!completed);
            return intervalPerturbationFraction(DEFAULT_PERTURBATION_FRACTION);
        }

        @Nonnull
        public Builder skipInitialInvoke() {
            checkState(!completed);
            this.skipInitialInvoke = true;
            return this;
        }

        @Nonnull
        public Builder initialInvokeDelayMs(long initialInvokeDelayMs) {
            checkState(!completed);
            checkArgument(initialInvokeDelayMs >= 0L);
            this.initialInvokeDelayMs = initialInvokeDelayMs;
            return this;
        }

        @Nonnull
        public Builder noRepeatOnError() {
            checkState(!completed);
            this.noRepeatOnError = true;
            return this;
        }

        @Nonnull
        public Builder errorDelayMs(long errorDelayMs) {
            checkState(!completed);
            checkArgument(errorDelayMs >= 0L);
            this.errorDelayMs = errorDelayMs;
            return this;
        }

        @Nonnull
        public Builder stopCondition(@Nonnull Predicate<Void> stopCondition) {
            checkState(!completed);
            this.stopCondition = stopCondition;
            return this;
        }

        @Nonnull
        public Builder ignoreInterruption() {
            checkState(!completed);
            this.ignoreInterruption = true;
            return this;
        }

        @Nonnull
        public Builder syncStart() {
            checkState(!completed);
            this.syncStart = true;
            return this;
        }

        @Nonnull
        public Builder loopAction(@Nonnull Action loopAction) {
            checkState(!completed);
            this.loopAction = loopAction;
            this.loopActionWithLoopArg = null;
            return this;
        }

        @Nonnull
        public Builder loopAction(@Nonnull NonnullDataAction<Loop> loopAction) {
            checkState(!completed);
            this.loopAction = null;
            this.loopActionWithLoopArg = loopAction;
            return this;
        }

        @Nonnull
        public Builder startAction(@Nonnull Action startAction) {
            checkState(!completed);
            this.startAction = startAction;
            return this;
        }

        @Nonnull
        public Builder stopAction(@Nonnull Action stopAction) {
            checkState(!completed);
            this.stopAction = stopAction;
            return this;
        }

        @Nonnull
        public LoopConfig build() {
            checkState(!completed);
            checkState((loopAction == null) != (loopActionWithLoopArg == null), "loopAction must be set");
            this.completed = true;
            return new LoopConfig(
                intervalMs,
                intervalPerturbationMs,
                skipInitialInvoke,
                initialInvokeDelayMs,
                noRepeatOnError,
                errorDelayMs,
                stopCondition,
                ignoreInterruption,
                syncStart,
                loopAction,
                loopActionWithLoopArg,
                startAction,
                stopAction
            );
        }

        private Builder() {
            //nothing
        }
    }

    @Nullable
    public final Long intervalMs;
    @Nullable
    public final Double intervalPerturbationMs;
    public final boolean skipInitialInvoke;
    @Nullable
    public final Long initialInvokeDelayMs;
    public final boolean noRepeatOnError;
    @Nullable
    public final Long errorDelayMs;
    @Nullable
    public final Predicate<Void> stopCondition;
    public final boolean ignoreInterruption;
    public final boolean syncStart;
    @Nullable
    public final Action loopAction;
    @Nullable
    public final NonnullDataAction<Loop> loopActionWithLoopArg;
    @Nullable
    public final Action startAction;
    @Nullable
    public final Action stopAction;

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public LoopConfig copy() {
        return new LoopConfig(
            intervalMs,
            intervalPerturbationMs,
            skipInitialInvoke,
            initialInvokeDelayMs,
            noRepeatOnError,
            errorDelayMs,
            stopCondition,
            ignoreInterruption,
            syncStart,
            loopAction,
            loopActionWithLoopArg,
            startAction,
            stopAction
        );
    }

    @Nullable
    public Long nextInvokeTs(long lastInvokeTs, boolean initial, boolean error) {
        if (error) {
            return lastInvokeTs + (errorDelayMs != null ? errorDelayMs : 0L);
        }
        if (initial) {
            //NOTE: initial invoke always happen, no matter intervals are present or not
            return lastInvokeTs + (initialInvokeDelayMs != null ? initialInvokeDelayMs : 0L);
        }
        if (intervalMs == null) {
            return null;
        }
        long resultTs = lastInvokeTs + intervalMs;
        if (intervalPerturbationMs != null && intervalMs > 0L) {
            resultTs -= (long) (intervalPerturbationMs * ThreadLocalRandom.current().nextDouble());
        }
        return resultTs;
    }

    private LoopConfig(
        @Nullable Long intervalMs,
        @Nullable Double intervalPerturbationMs,
        boolean skipInitialInvoke,
        @Nullable Long initialInvokeDelayMs,
        boolean noRepeatOnError,
        @Nullable Long errorDelayMs,
        @Nullable Predicate<Void> stopCondition,
        boolean ignoreInterruption,
        boolean syncStart,
        @Nullable Action loopAction,
        @Nullable NonnullDataAction<Loop> loopActionWithLoopArg,
        @Nullable Action startAction,
        @Nullable Action stopAction
    ) {
        checkArgument((loopAction == null) != (loopActionWithLoopArg == null));
        this.intervalMs = intervalMs;
        this.intervalPerturbationMs = intervalPerturbationMs;
        this.skipInitialInvoke = skipInitialInvoke;
        this.initialInvokeDelayMs = initialInvokeDelayMs;
        this.noRepeatOnError = noRepeatOnError;
        this.errorDelayMs = errorDelayMs;
        this.stopCondition = stopCondition;
        this.ignoreInterruption = ignoreInterruption;
        this.syncStart = syncStart;
        this.loopAction = loopAction;
        this.loopActionWithLoopArg = loopActionWithLoopArg;
        this.startAction = startAction;
        this.stopAction = stopAction;
    }
}
