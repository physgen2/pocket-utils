package com.freesolutions.pocket.pocketutils.concurrency.executor;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketutils.actions.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class StartableThreadPoolExecutor implements ExecutorService, Startable {
    private static final Logger LOGGER = LoggerFactory.getLogger(StartableThreadPoolExecutor.class);

    private static final long TERMINATION_TIMEOUT_MS = 10000L;//10 secs

    private final int coreThreadCount;
    private final int maxThreadCount;
    @Nonnull
    private final ThreadFactory threadFactory;
    @Nonnull
    private final BlockingQueue<Runnable> queue;
    private final boolean finishOnStop;

    @Nonnull
    private final AtomicReference<ExecutorService> delegate = new AtomicReference<>();

    public StartableThreadPoolExecutor(
        int coreThreadCount,
        int maxThreadCount,
        @Nonnull ThreadFactory threadFactory,
        @Nonnull BlockingQueue<Runnable> queue,
        boolean finishOnStop
    ) {
        this.coreThreadCount = coreThreadCount;
        this.maxThreadCount = maxThreadCount;
        this.threadFactory = threadFactory;
        this.queue = queue;
        this.finishOnStop = finishOnStop;
    }

    @Override
    public void start() throws Exception {
        ExecutorService executorService = new ThreadPoolExecutor(
            coreThreadCount, maxThreadCount,
            0L, TimeUnit.MILLISECONDS,
            queue,
            threadFactory
        );
        this.delegate.set(executorService);
    }

    @Override
    public void stop() {
        ExecutorService executorService = this.delegate.getAndSet(null);
        if (executorService != null) {
            shutdownAndAwaitTermination(executorService, TERMINATION_TIMEOUT_MS, finishOnStop);
        }
    }

    @Override
    public void shutdown() {
        delegate().shutdown();
    }

    @Nonnull
    @Override
    public List<Runnable> shutdownNow() {
        return delegate().shutdownNow();
    }

    @Override
    public boolean isShutdown() {
        return delegate().isShutdown();
    }

    @Override
    public boolean isTerminated() {
        return delegate().isTerminated();
    }

    @Override
    public boolean awaitTermination(long timeout, @Nonnull TimeUnit unit) throws InterruptedException {
        return delegate().awaitTermination(timeout, unit);
    }

    @Nonnull
    @Override
    public <T> Future<T> submit(@Nonnull Callable<T> task) {
        try {
            return delegate().submit(task);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable t) {
            return immediateFailedFuture(t);
        }
    }

    @Nonnull
    @Override
    public Future<?> submit(@Nonnull Runnable task) {
        try {
            return delegate().submit(task);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable t) {
            return immediateFailedFuture(t);
        }
    }

    @Nonnull
    @Override
    public <T> Future<T> submit(@Nonnull Runnable task, T result) {
        try {
            return delegate().submit(task, result);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable t) {
            return immediateFailedFuture(t);
        }
    }

    @Nonnull
    @Override
    public <T> List<Future<T>> invokeAll(@Nonnull Collection<? extends Callable<T>> tasks) throws InterruptedException {
        try {
            return delegate().invokeAll(tasks);
        } catch (InterruptedException | NullPointerException e) {
            throw e;
        } catch (Throwable t) {
            return immediateFailedFutures(tasks.size(), t);
        }
    }

    @Nonnull
    @Override
    public <T> List<Future<T>> invokeAll(@Nonnull Collection<? extends Callable<T>> tasks, long timeout, @Nonnull TimeUnit unit) throws InterruptedException {
        try {
            return delegate().invokeAll(tasks, timeout, unit);
        } catch (InterruptedException | NullPointerException e) {
            throw e;
        } catch (Throwable t) {
            return immediateFailedFutures(tasks.size(), t);
        }
    }

    @Nonnull
    @Override
    public <T> T invokeAny(@Nonnull Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return delegate().invokeAny(tasks);
    }

    //NOTE: nullability unknown
    @Override
    public <T> T invokeAny(@Nonnull Collection<? extends Callable<T>> tasks, long timeout, @Nonnull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return delegate().invokeAny(tasks, timeout, unit);
    }

    @Override
    public void execute(@Nonnull Runnable command) {
        delegate().execute(command);
    }

    @Nonnull
    private ExecutorService delegate() {
        return checkNotNull(delegate.get());
    }

    @Nonnull
    private static <T> List<Future<T>> immediateFailedFutures(int size, @Nonnull Throwable t) {
        List<Future<T>> result = new ArrayList<>(size);
        for (; size > 0; size--) {
            result.add(immediateFailedFuture(t));
        }
        return result;
    }

    @Nonnull
    private static <T> Future<T> immediateFailedFuture(@Nonnull Throwable t) {
        CompletableFuture<T> result = new CompletableFuture<>();
        boolean ok = result.completeExceptionally(t);
        checkState(ok);
        return result;
    }

    private static void shutdownAndAwaitTermination(
        @Nonnull ExecutorService service,
        long timeoutMs,
        boolean finish
    ) {
        Action.executeSafe(() -> {
            service.shutdown();
            try {
                if (!service.awaitTermination(timeoutMs / 2, TimeUnit.MILLISECONDS)) {
                    List<Runnable> unfinished = service.shutdownNow();
                    service.awaitTermination(timeoutMs / 2, TimeUnit.MILLISECONDS);
                    if (finish) {
                        for (Runnable r : unfinished) {
                            Action.executeSafe(r::run);
                        }
                    }
                }
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
                service.shutdownNow();
            }
        });
    }
}
