package com.freesolutions.pocket.pocketutils.concurrency.ensure;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Invokable;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketutils.concurrency.result.Status;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullablePresentableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability.NullableResult;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NullableFuture;
import com.freesolutions.pocket.pocketutils.concurrency.result.impl.NullableImmutableResult;
import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncQuery;
import com.freesolutions.pocket.pocketutils.concurrency.sync.result.NullableSyncResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkExpectNull;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class PresentableEnsure implements
        Ensure,
        Invokable,
        NullablePresentableResult<Void>,
        PresentableEnsureExt<PresentableEnsure> {
    public static final PresentableEnsure ENSURED = new PresentableEnsure(NullableImmutableResult.ofPresentNull());

    private final NullableResult<Void> delegate;

    @Nonnull
    public static PresentableEnsure initial() {
        return new PresentableEnsure(NullableFuture.of());
    }

    @Nonnull
    public static PresentableEnsure ensured() {
        return ENSURED;
    }

    @Nonnull
    @Override
    public NullableSyncResult<Void> sync(@Nonnull SyncQuery syncQuery) {
        return delegate.sync(syncQuery);
    }

    @Nonnull
    @Override
    public Status status() {
        Status status = delegate.status();
        checkState(!status.done || status.present);
        return status;
    }

    @Nullable
    @Override
    public Void value() {
        Void v = delegate.value();
        checkExpectNull(v);
        return null;
    }

    @Nullable
    @Override
    public Void syncedValue() throws Exception {
        Void v = delegate.syncedValue();
        checkExpectNull(v);
        return null;
    }

    @Nullable
    @Override
    public Throwable error() {
        Throwable t = delegate.error();
        checkState(t == null);
        return null;
    }

    @Override
    public void whenDone(@Nonnull Pipe pipe, @Nonnull Action action) {
        delegate.whenDone(pipe, action);
    }

    @Override
    public void purgeCallbacks() {
        delegate.purgeCallbacks();
    }

    @Override
    public boolean presentNull() {
        return delegate.presentNull();
    }

    @Override
    public boolean present(@Nullable Void value) {
        checkExpectNull(value);
        return presentNull();
    }

    @Override
    public void invoke() {
        presentNull();
    }

    private PresentableEnsure(@Nonnull NullableResult<Void> delegate) {
        this.delegate = delegate;
    }
}
