package com.freesolutions.pocket.pocketutils.concurrency.pipes;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.queue.LinkedNLQueue;
import com.freesolutions.pocket.pocketutils.concurrency.queue.NLQueue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicInteger;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class QuotaPipe implements Pipe {

    @Nonnull
    private final Pipe delegate;
    private final int quota;

    @Nullable
    private final NLQueue<Action> deferred;
    @Nullable
    private final AtomicInteger actual;

    @Nonnull
    public static QuotaPipe of(@Nonnull Pipe delegate, int quota) {
        checkArgument(quota > 0);
        if (delegate instanceof QuotaPipe) {
            QuotaPipe quotaDelegate = (QuotaPipe) delegate;
            if (quotaDelegate.quota == quota) {
                return quotaDelegate;//reuse existing without additional wrapping
            }
        }
        if (quota == 1) {
            return UnitPipe.of(delegate);
        }
        return new QuotaPipe(delegate, quota);
    }

    public int quota() {
        return quota;
    }

    @Nonnull
    public Pipe delegate() {
        return delegate;
    }

    public boolean isThroughMode() {
        return actual == null;
    }

    @Override
    public void add(@Nonnull Action action) {
        if (actual == null) {
            delegate.add(action);
            return;
        }
        int size = actual.incrementAndGet();
        if (size > quota) {
            checkNotNull(deferred).add(action);
        } else {
            delegate.add(wrapAction(action));
        }
    }

    protected QuotaPipe(@Nonnull Pipe delegate, int quota) {
        checkArgument(quota > 0);
        this.delegate = delegate;
        this.quota = quota;
        if (delegate instanceof QuotaPipe && ((QuotaPipe) delegate).quota <= quota) {
            //through-mode
            this.deferred = null;
            this.actual = null;
        } else {
            //normal mode
            this.deferred = new LinkedNLQueue<>();
            this.actual = new AtomicInteger(0);
        }
    }

    @Nonnull
    private Action wrapAction(@Nonnull Action action) {
        return () -> {
            try {
                action.execute();
            } finally {
                int size = checkNotNull(actual).decrementAndGet();
                if (size >= quota) {
                    while (true) {
                        Action deferredAction = checkNotNull(deferred).poll();
                        if (deferredAction != null) {
                            delegate.add(wrapAction(deferredAction));
                            break;
                        }
                        Thread.yield();
                    }
                }
            }
        };
    }
}
