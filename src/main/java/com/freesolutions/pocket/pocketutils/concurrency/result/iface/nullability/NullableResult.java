package com.freesolutions.pocket.pocketutils.concurrency.result.iface.nullability;

import com.freesolutions.pocket.pocketutils.concurrency.result.iface.ext.*;

/**
 * @author Stanislau Mirzayeu
 */
public interface NullableResult<T> extends
        NullableReadableResult<T>, NullableWritableResult<T>,
        NullableReadableResultDelegatesExt<T, NullableResult<T>>,
        NullableReadableResultSubmitsExt<T, NullableResult<T>>, NullableWritableResultSubmitsExt<T, NullableResult<T>>,
        NullableWritableResultCompletionsExt<T, NullableResult<T>>,
        NullableWritableResultTransformsExt<T, NullableResult<T>>,
        NullableWritableResultExpirationsExt<T, NullableResult<T>>, WritableResultExpirationsExt<T, NullableResult<T>>
{
    //nothing
}
