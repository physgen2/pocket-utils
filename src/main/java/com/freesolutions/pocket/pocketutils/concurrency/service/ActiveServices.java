package com.freesolutions.pocket.pocketutils.concurrency.service;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.actions.FromNonnullDataHandler;
import com.freesolutions.pocket.pocketutils.concurrency.loop.AsyncLoop;
import com.freesolutions.pocket.pocketutils.concurrency.loop.LoopConfig;
import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import com.freesolutions.pocket.pocketlifecycle.annotation.IdempotentLifecycle;
import com.freesolutions.pocket.pocketlifecycle.annotation.RestartableLifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Stanislau Mirzayau
 */
@IdempotentLifecycle
@RestartableLifecycle
@ThreadSafe
public class ActiveServices<KEY, SERVICE> implements Startable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveServices.class);

    public interface Listener<SERVICE> {

        default void onAfterStart(SERVICE service) throws Exception {
            //nothing
        }

        default void onBeforeStop(SERVICE service) throws Exception {
            //nothing
        }
    }

    @Nonnull
    private final String serviceTypeName;
    @Nonnull
    private final FromNonnullDataHandler<KEY, SERVICE> serviceConstructor;
    @Nullable
    private final Listener<SERVICE> listener;

    @Nonnull
    private final AsyncLoop updating;

    @Nonnull
    private volatile Set<KEY> activeKeys = Collections.emptySet();//immutable set
    @Nonnull
    private final ConcurrentMap<KEY, SERVICE> services = new ConcurrentHashMap<>();

    public ActiveServices(
        @Nonnull String serviceTypeName,
        @Nonnull Pipe pipe,
        @Nonnull FromNonnullDataHandler<KEY, SERVICE> serviceConstructor
    ) {
        this(
            serviceTypeName,
            pipe,
            null, //periodicUpdateIntervalMs
            serviceConstructor,
            null //listener
        );
    }

    public ActiveServices(
        @Nonnull String serviceTypeName,
        @Nonnull Pipe pipe,
        @Nullable Long periodicUpdateIntervalMs,
        @Nonnull FromNonnullDataHandler<KEY, SERVICE> serviceConstructor
    ) {
        this(
            serviceTypeName,
            pipe,
            periodicUpdateIntervalMs,
            serviceConstructor,
            null //listener
        );
    }

    public ActiveServices(
        @Nonnull String serviceTypeName,
        @Nonnull Pipe pipe,
        @Nonnull FromNonnullDataHandler<KEY, SERVICE> serviceConstructor,
        @Nullable Listener<SERVICE> listener
    ) {
        this(
            serviceTypeName,
            pipe,
            null, //periodicUpdateIntervalMs
            serviceConstructor,
            listener
        );
    }

    public ActiveServices(
        @Nonnull String serviceTypeName,
        @Nonnull Pipe pipe,
        @Nullable Long periodicUpdateIntervalMs,
        @Nonnull FromNonnullDataHandler<KEY, SERVICE> serviceConstructor,
        @Nullable Listener<SERVICE> listener
    ) {
        this.serviceTypeName = serviceTypeName;
        this.serviceConstructor = serviceConstructor;
        this.listener = listener;

        LoopConfig.Builder loopBuilder = LoopConfig.builder()
            .startAction(this::clearServices)
            .loopAction(this::updateServices)
            .stopAction(this::removeServices);
        if (periodicUpdateIntervalMs != null) {
            loopBuilder.intervalMs(periodicUpdateIntervalMs);
            loopBuilder.defaultIntervalPerturbation();
        }

        this.updating = new AsyncLoop(
            pipe,
            "active-services-" + serviceTypeName,
            loopBuilder.build()
        );
    }

    @Override
    public void start() throws Exception {
        updating.start();
    }

    @Override
    public void stop() {
        updating.stop();
    }

    public void update() {
        updating.invoke();
    }

    public void updateActiveKeys(@Nonnull Set<KEY> activeKeys) {
        this.activeKeys = new HashSet<>(activeKeys);//volatile
        updating.invoke();
    }

    @Nonnull
    public Map<KEY, SERVICE> services() {
        return Collections.unmodifiableMap(services);//concurrent map
    }

    private void clearServices() {
        this.activeKeys = Collections.emptySet();
        services.clear();
    }

    private void updateServices() {

        Set<KEY> activeKeys = this.activeKeys;//volatile

        //determine old
        Map<KEY, SERVICE> oldServices = new HashMap<>();
        services.forEach(((key, service) -> {
            if (!activeKeys.contains(key)) {
                oldServices.put(key, service);
            }
        }));

        //determine new
        Set<KEY> newKeys = new HashSet<>();
        activeKeys.forEach(key -> {
            if (!services.containsKey(key)) {
                newKeys.add(key);
            }
        });

        //create & start new
        for (KEY key : newKeys) {
            SERVICE service = FromNonnullDataHandler.executeSafe(serviceConstructor, key, null);
            if (service != null) {
                if (doStartSafe(key, service)) {
                    services.put(key, service);
                }
            }
        }

        //stop old
        oldServices.forEach((key, service) -> {
            services.remove(key, service);
            doStopSafe(key, service);
        });
    }

    private void removeServices() {

        //clear
        Map<KEY, SERVICE> oldServices = new HashMap<>(services);
        clearServices();

        //stop
        oldServices.forEach(this::doStopSafe);
    }

    private boolean doStartSafe(@Nonnull KEY key, @Nonnull SERVICE service) {
        if (service instanceof Startable) {
            try {
                LOGGER.info("Start \"{}\" for {} ...", serviceTypeName, key);
                ((Startable) service).start();
                if (listener != null) {
                    Action.executeSafe(() -> listener.onAfterStart(service));
                }
                LOGGER.info("\"{}\" for {} is started", serviceTypeName, key);
            } catch (Throwable t) {
                if (t instanceof InterruptedException) {
                    Thread.currentThread().interrupt();
                } else {
                    LOGGER.error("Failed to start \"" + serviceTypeName + "\" for " + key + ", ignore", t);
                }
                return false;
            }
        }
        return true;
    }

    private void doStopSafe(@Nonnull KEY key, @Nonnull SERVICE service) {
        if (service instanceof Startable) {
            try {
                LOGGER.info("Stop \"{}\" for {} ...", serviceTypeName, key);
                if (listener != null) {
                    Action.executeSafe(() -> listener.onBeforeStop(service));
                }
                ((Startable) service).stop();
                LOGGER.info("\"{}\" for {} is stopped", serviceTypeName, key);
            } catch (Throwable t) {
                if (t instanceof InterruptedException) {
                    Thread.currentThread().interrupt();
                } else {
                    LOGGER.error("Failed to stop \"" + serviceTypeName + "\" for " + key + ", ignore", t);
                }
            }
        }
    }
}
