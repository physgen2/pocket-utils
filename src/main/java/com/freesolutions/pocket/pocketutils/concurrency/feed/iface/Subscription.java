package com.freesolutions.pocket.pocketutils.concurrency.feed.iface;

import com.freesolutions.pocket.pocketutils.concurrency.pipes.Pipe;
import com.freesolutions.pocket.pocketlifecycle.startable.AutoStartable;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.HasLifecycleState;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface Subscription<G, P, L> extends AutoStartable, HasLifecycleState {

    @Nonnull
    Feed<G, P, L> feed();

    @Nonnull
    Pipe pipe();

    @Nonnull
    G group();

    @Nonnull
    P context();

    @Nonnull
    L listener();

    default void applyAction(@Nonnull ListenerAction<G, P, L> listenerAction) {
        pipe().add(() -> listenerAction.execute(group(), context(), listener()));
    }
}
