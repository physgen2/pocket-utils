package com.freesolutions.pocket.pocketutils.concurrency.queue;

import com.freesolutions.pocket.pocketutils.concurrency.dataflow.QueueStore;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.ResultTakeable;

import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface SyncableNLQueue<T> extends NLQueue<T>, ResultTakeable<T>, QueueStore<T> {
    //empty
}
