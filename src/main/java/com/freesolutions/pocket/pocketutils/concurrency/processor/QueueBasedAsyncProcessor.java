package com.freesolutions.pocket.pocketutils.concurrency.processor;

import com.freesolutions.pocket.pocketutils.actions.Action;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.QueueStore;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public interface QueueBasedAsyncProcessor<Q extends QueueStore<Action>> extends AsyncProcessor {

    @Nonnull
    Q actionQueue();
}
