package com.freesolutions.pocket.pocketutils.concurrency.sync.result;

import com.freesolutions.pocket.pocketutils.concurrency.sync.SyncStatus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeoutException;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class NullableSyncResult<T> implements SyncResult<T> {

    private static final NullableSyncResult<?> OK_NULL = new NullableSyncResult<>(SyncStatus.OK, null);
    private static final NullableSyncResult<?> TIMED_OUT = new NullableSyncResult<>(SyncStatus.TIMED_OUT, null);
    private static final NullableSyncResult<?> INTERRUPTED = new NullableSyncResult<>(SyncStatus.INTERRUPTED, null);
    private static final NullableSyncResult<?> REJECTED = new NullableSyncResult<>(SyncStatus.REJECTED, null);

    @Nonnull
    private final SyncStatus status;
    @Nullable
    private final T value;

    @Nullable
    public static <T> NullableSyncResult<T> ofOkIfOk(@Nonnull NullableSyncResult<T> source) {
        return source.status.ok ? source : null;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static <T> NullableSyncResult<T> ofNotOkIfNotOk(@Nonnull NullableSyncResult<?> source) {
        if (source.status.ok) {
            return null;
        }
        return (NullableSyncResult<T>) source;//direct cast due to value is null
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableSyncResult<T> ofOkNull() {
        return (NullableSyncResult<T>) OK_NULL;
    }

    @Nonnull
    public static <T> NullableSyncResult<T> ofOk(@Nullable T value) {
        return new NullableSyncResult<>(SyncStatus.OK, value);
    }

    @Nonnull
    public static <T> NullableSyncResult<T> ofNotOk(@Nonnull SyncStatus status) {
        checkArgument(!status.ok);
        return new NullableSyncResult<>(status, null);
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableSyncResult<T> ofTimedOut() {
        return (NullableSyncResult<T>) TIMED_OUT;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableSyncResult<T> ofInterrupted() {
        return (NullableSyncResult<T>) INTERRUPTED;
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <T> NullableSyncResult<T> ofRejected() {
        return (NullableSyncResult<T>) REJECTED;
    }

    @Nonnull
    @Override
    public SyncStatus status() {
        return status;
    }

    @Nullable
    @Override
    public T value() {
        checkState(status.ok);
        return value;
    }

    @Nonnull
    @Override
    public NullableSyncResult<T> notRejected() throws RejectedExecutionException {
        if (status.rejected) {
            throw new RejectedExecutionException();
        }
        return this;
    }

    @Nonnull
    @Override
    public NullableSyncResult<T> notInterrupted() throws InterruptedException {
        if (status.interrupted) {
            throw new InterruptedException("Interrupted by sync");
        }
        return this;
    }

    @Nonnull
    @Override
    public NullableSyncResult<T> noProblems() throws RejectedExecutionException, InterruptedException {
        return notRejected().notInterrupted();
    }

    @Nonnull
    @Override
    public NullableSyncResult<T> notTimedOut() throws TimeoutException {
        if (status.timedOut) {
            throw new TimeoutException();
        }
        return this;
    }

    @Nonnull
    @Override
    public NullableSyncResult<T> mustOk() throws RejectedExecutionException, InterruptedException, TimeoutException {
        notRejected().notInterrupted().notTimedOut();
        checkState(status.ok);
        return this;
    }

    protected NullableSyncResult(@Nonnull SyncStatus status, @Nullable T value) {
        checkArgument(status.ok || (value == null));
        this.status = status;
        this.value = value;
    }
}
