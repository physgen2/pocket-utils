package com.freesolutions.pocket.pocketutils.concurrency.topic.impl.topic;

import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Consumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.ListenableConsumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.ManualConsumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.iface.Topic;
import com.freesolutions.pocket.pocketutils.concurrency.topic.impl.consumer.DelegatingListenableConsumer;
import com.freesolutions.pocket.pocketutils.concurrency.topic.impl.consumer.SyncableQueueManualConsumer;
import com.freesolutions.pocket.pocketutils.concurrency.dataflow.Addable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class DistributingTopic<T> implements Topic<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DistributingTopic.class);

    private final ConcurrentMap<Consumer<T>, Boolean> consumers = new ConcurrentHashMap<>();

    @Nonnull
    @Override
    public ManualConsumer<T> manualConsumer(@Nonnull Predicate<T> filter) {
        return new SyncableQueueManualConsumer<>(
                this,
                filter,
                x -> consumers.put(x, Boolean.TRUE),
                x -> consumers.remove(x, Boolean.TRUE)
        );
    }

    @Nonnull
    @Override
    public ListenableConsumer<T> listenableConsumer(@Nonnull Predicate<T> filter, @Nonnull Addable<T> output) {
        return new DelegatingListenableConsumer<>(
                this,
                filter,
                output,
                x -> consumers.put(x, Boolean.TRUE),
                x -> consumers.remove(x, Boolean.TRUE)
        );
    }

    @Override
    public int size() {
        return consumers.size();
    }

    @Override
    public void add(@Nonnull T item) {
        for (Consumer<T> consumer : consumers.keySet()) {
            boolean accepted = false;
            try {
                accepted = consumer.filter().test(item);
            } catch (Throwable t) {
                LOGGER.error("Consumer acceptance filter failed, ignore, assume not accepted", t);
            }
            if (accepted) {
                consumer.add(item);
            }
        }
    }
}
