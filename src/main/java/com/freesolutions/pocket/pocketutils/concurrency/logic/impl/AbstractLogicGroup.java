package com.freesolutions.pocket.pocketutils.concurrency.logic.impl;

import com.freesolutions.pocket.pocketutils.concurrency.logic.iface.Logic;
import com.freesolutions.pocket.pocketutils.concurrency.logic.iface.LogicGroup;
import com.freesolutions.pocket.pocketlifecycle.lifecycle.LifecycleStartable;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public abstract class AbstractLogicGroup<CONTEXT, CONTROL> implements LogicGroup<CONTEXT, CONTROL> {

    @ThreadSafe
    public abstract class AbstractLogic extends LifecycleStartable implements Logic<CONTEXT, CONTROL> {

        @Nonnull
        protected final CONTEXT context;

        public AbstractLogic(@Nonnull CONTEXT context) {
            this.context = context;
        }

        @Override
        public void start() throws Exception {
            logics.add(this);
            try {
                super.start();
            } catch (Throwable t) {
                logics.removeIf(x -> x == this);
                throw t;
            }
        }

        @Override
        public void stop() {
            try {
                super.stop();
            } finally {
                logics.removeIf(x -> x == this);
            }
        }

        @Nonnull
        @Override
        public CONTEXT context() {
            return context;
        }
    }

    private final List<Logic<CONTEXT, CONTROL>> logics = new CopyOnWriteArrayList<>();

    @Override
    public int size() {
        return logics.size();
    }

    @Nonnull
    @Override
    public List<Logic<CONTEXT, CONTROL>> all() {
        return Collections.unmodifiableList(logics);
    }
}
