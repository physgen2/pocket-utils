package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.*;
import java.time.temporal.ChronoField;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class DaySecondId implements Comparable<DaySecondId> {

    public static final DaySecondId MIN = ofLocalTime(LocalTime.MIN);
    public static final DaySecondId MAX = ofLocalTime(LocalTime.MAX);
    public static final DaySecondId MIDNIGHT = ofLocalTime(LocalTime.MIDNIGHT);
    public static final DaySecondId NOON = ofLocalTime(LocalTime.NOON);

    private static final long MIN_INC_SECOND_OF_DAY = ChronoField.SECOND_OF_DAY.range().getMinimum();
    private static final long COUNT_OF_SECONDS_OF_DAY = ChronoField.SECOND_OF_DAY.range().getMaximum() - MIN_INC_SECOND_OF_DAY + 1L;

    public final long secondOfDay;

    @Nonnull
    public static DaySecondId ofSecondOfDay(long secondOfDay) {
        return new DaySecondId(secondOfDay);
    }

    @Nonnull
    public static DaySecondId ofInsideTs(@Nonnull LocalTs insideTs) {
        return ofLocalTime(insideTs.toLocalDateTime().toLocalTime());
    }

    @Nonnull
    public static DaySecondId ofDayMillisId(@Nonnull DayMillisId dayMillisId) {
        return new DaySecondId(TimeUnit.MILLISECONDS.toSeconds(dayMillisId.millisOfDay));
    }

    @Nonnull
    public static DaySecondId ofLocalTime(@Nonnull LocalTime localTime) {
        return ofSecondOfDay(localTime.toSecondOfDay());
    }

    @Nonnull
    public static DaySecondId now(@Nonnull Clock zonedClock) {
        return now(zonedClock, zonedClock.getZone());
    }

    @Nonnull
    public static DaySecondId now(@Nonnull Clock utcClock, @Nonnull ZoneId zoneId) {
        return DaySecondId.ofLocalTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(utcClock.millis()), zoneId).toLocalTime());
    }

    @Nonnull
    public DaySecondId next() {
        return plusAmount(1L);
    }

    @Nonnull
    public DaySecondId prev() {
        return minusAmount(1L);
    }

    @Nonnull
    public DaySecondId plusAmount(long signedCount) {
        return new DaySecondId(rotateSecondOfDay(secondOfDay + signedCount));
    }

    @Nonnull
    public DaySecondId minusAmount(long signedCount) {
        return new DaySecondId(rotateSecondOfDay(secondOfDay - signedCount));
    }

    @Nonnull
    public DayMillisId toDayMillisId() {
        return DayMillisId.ofMillisOfDay(TimeUnit.SECONDS.toMillis(secondOfDay));
    }

    @Nonnull
    public LocalTime toLocalTime() {
        return LocalTime.ofSecondOfDay(secondOfDay);
    }

    public boolean isBefore(@Nonnull DaySecondId daySecondId) {
        return secondOfDay < daySecondId.secondOfDay;
    }

    public boolean isBeforeOrEqual(@Nonnull DaySecondId daySecondId) {
        return secondOfDay <= daySecondId.secondOfDay;
    }

    public boolean isAfter(@Nonnull DaySecondId daySecondId) {
        return secondOfDay > daySecondId.secondOfDay;
    }

    public boolean isAfterOrEqual(@Nonnull DaySecondId daySecondId) {
        return secondOfDay >= daySecondId.secondOfDay;
    }

    public boolean isEqual(@Nonnull DaySecondId daySecondId) {
        return secondOfDay == daySecondId.secondOfDay;
    }

    public long diffAmount(@Nonnull DaySecondId daySecondId) {
        return secondOfDay - daySecondId.secondOfDay;
    }

    @Override
    public int compareTo(@Nullable DaySecondId rhs) {
        return rhs != null ? Long.compare(secondOfDay, rhs.secondOfDay) : 1;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof DaySecondId)) return false;
        DaySecondId that = (DaySecondId) o;
        return secondOfDay == that.secondOfDay;
    }

    @Override
    public int hashCode() {
        return (int) (secondOfDay ^ (secondOfDay >>> 32));
    }

    @Nonnull
    @Override
    public String toString() {
        return "DaySecondId[" + secondOfDay + " | " + toLocalTime() + "]";
    }

    private DaySecondId(long secondOfDay) {
        checkArgument(isValidSecondOfDay(secondOfDay));
        this.secondOfDay = secondOfDay;
    }

    private static boolean isValidSecondOfDay(long secondOfDay) {
        return ChronoField.SECOND_OF_DAY.range().isValidValue(secondOfDay);
    }

    private static long rotateSecondOfDay(long secondOfDayExceeded) {
        return secondOfDayExceeded < MIN_INC_SECOND_OF_DAY ?
            COUNT_OF_SECONDS_OF_DAY + 2L * MIN_INC_SECOND_OF_DAY - rotateSecondOfDay(2L * MIN_INC_SECOND_OF_DAY - secondOfDayExceeded) :
            MIN_INC_SECOND_OF_DAY + (secondOfDayExceeded - MIN_INC_SECOND_OF_DAY) % COUNT_OF_SECONDS_OF_DAY;
    }
}
