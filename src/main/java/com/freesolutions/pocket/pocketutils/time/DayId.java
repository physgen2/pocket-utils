package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class DayId implements Comparable<DayId> {

    public static final DateTimeFormatter ISO_CODE_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE;

    public static final DayId EPOCH = DayId.ofEpochDay(0L);
    public static final DayId MIN = DayId.ofInsideTs(LocalTs.MIN).minusAmount(1L);
    public static final DayId MAX = DayId.ofInsideTs(LocalTs.MAX).plusAmount(1L);

    public final long epochDay;

    private String isoCode;//private, mutable, non-volatile, ok

    @Nonnull
    public static DayId ofEpochDay(long epochDay) {
        return new DayId(epochDay);
    }

    @Nonnull
    public static DayId ofIsoCode(@Nonnull String isoCode) {
        return new DayId(LocalDate.parse(isoCode, ISO_CODE_FORMATTER).toEpochDay());
    }

    @Nonnull
    public static DayId ofInsideTs(@Nonnull LocalTs insideTs) {
        return new DayId(insideTs.toEpochDay());
    }

    @Nonnull
    public static DayId ofLocalDate(@Nonnull LocalDate localDate) {
        return new DayId(localDate.toEpochDay());
    }

    @Nonnull
    public static DayId now(@Nonnull Clock zonedClock) {
        return now(zonedClock, zonedClock.getZone());
    }

    @Nonnull
    public static DayId now(@Nonnull Clock utcClock, @Nonnull ZoneId zoneId) {
        return DayId.ofInsideTs(LocalTs.now(utcClock, zoneId));
    }

    @Nonnull
    public static String toIsoCode(long epochDay) {
        return ISO_CODE_FORMATTER.format(LocalDate.ofEpochDay(epochDay));
    }

    @Nullable
    public static DayId min(@Nullable DayId left, @Nullable DayId right) {
        if (left == null || right == null) {
            return null;
        }
        return left.isBefore(right) ? left : right;
    }

    @Nullable
    public static DayId max(@Nullable DayId left, @Nullable DayId right) {
        if (left == null) {
            return right;
        }
        if (right == null) {
            return left;
        }
        return left.isAfter(right) ? left : right;
    }

    public int dayOfWeekInt() {
        //[0 .. 6] <--> [monday ... sunday]
        return (int) Math.floorMod(epochDay + 3, 7);
    }

    @Nonnull
    public DayOfWeek dayOfWeek() {
        return DayOfWeek.of(dayOfWeekInt() + 1);
    }

    @Nonnull
    public DayId next() {
        return plusAmount(1L);
    }

    @Nonnull
    public DayId prev() {
        return minusAmount(1L);
    }

    @Nonnull
    public DayId plusAmount(long signedCount) {
        return new DayId(epochDay + signedCount);
    }

    @Nonnull
    public DayId minusAmount(long signedCount) {
        return new DayId(epochDay - signedCount);
    }

    @Nonnull
    public LocalTs toLocalTs() {
        return LocalTs.ofTs(TimeUnit.DAYS.toMillis(epochDay));
    }

    @Nonnull
    public LocalTs toLocalTs(@Nonnull DayMillisId dayMillisId) {
        return LocalTs.ofTs(TimeUnit.DAYS.toMillis(epochDay) + dayMillisId.millisOfDay);
    }

    @Nonnull
    public LocalDate toLocalDate() {
        return LocalDate.ofEpochDay(epochDay);
    }

    public boolean isBefore(@Nonnull DayId dayId) {
        return epochDay < dayId.epochDay;
    }

    public boolean isBeforeOrEqual(@Nonnull DayId dayId) {
        return epochDay <= dayId.epochDay;
    }

    public boolean isAfter(@Nonnull DayId dayId) {
        return epochDay > dayId.epochDay;
    }

    public boolean isAfterOrEqual(@Nonnull DayId dayId) {
        return epochDay >= dayId.epochDay;
    }

    public boolean isEqual(@Nonnull DayId dayId) {
        return epochDay == dayId.epochDay;
    }

    public long diffDays(@Nonnull DayId dayId) {
        return epochDay - dayId.epochDay;
    }

    @Nonnull
    public String isoCode() {
        if (isoCode == null) {
            this.isoCode = toIsoCode(epochDay);
        }
        return isoCode;
    }

    @Override
    public int compareTo(@Nullable DayId rhs) {
        return rhs != null ? Long.compare(epochDay, rhs.epochDay) : 1;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof DayId)) return false;
        DayId that = (DayId) o;
        return epochDay == that.epochDay;
    }

    @Override
    public int hashCode() {
        return (int) (epochDay ^ (epochDay >>> 32));
    }

    @Nonnull
    @Override
    public String toString() {
        return "DayId[" + isoCode() + " | " + epochDay + "]";
    }

    private DayId(long epochDay) {
        this.epochDay = epochDay;
    }
}
