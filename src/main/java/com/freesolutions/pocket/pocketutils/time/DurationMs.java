package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class DurationMs implements Comparable<DurationMs> {

    public static final DurationMs ZERO = new DurationMs(0L);
    public static final DurationMs MIN = new DurationMs(Long.MIN_VALUE);
    public static final DurationMs MAX = new DurationMs(Long.MAX_VALUE);
    public static final DurationMs DAY = new DurationMs(TimeUnit.DAYS.toMillis(1L));
    public static final DurationMs HOUR = new DurationMs(TimeUnit.HOURS.toMillis(1L));
    public static final DurationMs MINUTE = new DurationMs(TimeUnit.MINUTES.toMillis(1L));
    public static final DurationMs SECOND = new DurationMs(TimeUnit.SECONDS.toMillis(1L));
    public static final DurationMs MILLI = new DurationMs(1L);

    //NOTE: length of directed duration, measured in milliseconds, can be negative
    public final long durationMs;

    private String code;//private, mutable, non-volatile, ok

    @Nonnull
    public static DurationMs ofMillis(long value) {
        return new DurationMs(value);
    }

    @Nonnull
    public static DurationMs ofCode(String code) {
        int l = code.length();
        checkArgument(l > 0, "code must be not empty");

        //zero case
        if (l == 4
            && code.charAt(0) == 'z'
            && code.charAt(1) == 'e'
            && code.charAt(2) == 'r'
            && code.charAt(3) == 'o') {
            return ZERO;
        }

        int p = 0;

        //optional sign
        boolean negative = false;
        if (code.charAt(p) == '-') {
            p++;
            negative = true;
        }

        long days = 0L;
        long hours = 0L;
        long minutes = 0L;
        long seconds = 0L;
        long millis = 0L;

        boolean daysFilled = false;
        boolean hoursFilled = false;
        boolean minutesFilled = false;
        boolean secondsFilled = false;
        boolean millisFilled = false;

        //parse section by section
        boolean first = true;
        do {
            //parse not first colon
            if (!first) {
                checkArgument(p < l && code.charAt(p) == ':', "colon expected, code: %s, position: %s", code, p);
                p++;
            }
            first = false;

            //parse value
            checkArgument(p < l, "value expected, code: %s, position: %s", code, p);
            long v = 0L;
            boolean valuePresent = false;
            do {
                char ch = code.charAt(p);
                if (ch < '0' || ch > '9') {
                    break;
                }
                p++;
                valuePresent = true;
                v = v * 10 + (ch - '0');
            } while (p < l);
            checkArgument(valuePresent, "value expected, code: %s, position: %s", code, p);

            //parse unit
            if (p < l) {
                char ch = code.charAt(p);
                if (ch == 'd') {
                    checkArgument(!daysFilled, "days was already used, code: %s, position: %s", code, p);
                    p++;
                    days = v;
                    daysFilled = true;
                } else if (ch == 'h') {
                    checkArgument(!hoursFilled, "hours was already used, code: %s, position: %s", code, p);
                    p++;
                    hours = v;
                    hoursFilled = true;
                } else if (ch == 'm') {
                    p++;
                    if (p < l && code.charAt(p) == 's') {
                        checkArgument(!millisFilled, "millis was already used, code: %s, position: %s", code, p);
                        p++;
                        millis = v;
                        millisFilled = true;
                    } else {
                        checkArgument(!minutesFilled, "minutes was already used, code: %s, position: %s", code, p);
                        minutes = v;
                        minutesFilled = true;
                    }
                } else if (ch == 's') {
                    checkArgument(!secondsFilled, "seconds was already used, code: %s, position: %s", code, p);
                    p++;
                    seconds = v;
                    secondsFilled = true;
                } else {
                    checkArgument(false, "unit expected, code: %s, position: %s", code, p);
                }
            } else {
                //millis by default
                checkArgument(!millisFilled, "millis was already used, code: %s, position: %s", code, p);
                millis = v;
                millisFilled = true;
            }
        } while (p < l);

        //result
        long b = days * DAY.durationMs +
            hours * HOUR.durationMs +
            minutes * MINUTE.durationMs +
            seconds * SECOND.durationMs +
            millis;
        return new DurationMs(negative ? -b : b);
    }

    public static String toCode(long durationMs) {
        long v = durationMs;
        if (v == 0L) {
            return "zero";
        }
        StringBuilder b = new StringBuilder();
        if (v < 0L) {
            v = -v;
            b.append('-');
        }
        boolean millisOnly = true;
        if (v >= DAY.durationMs) {
            long d = v / DAY.durationMs;
            b.append(d).append('d');
            v -= d * DAY.durationMs;
            millisOnly = false;
        }
        if (v >= HOUR.durationMs) {
            if (!millisOnly) {
                b.append(':');
            }
            long h = v / HOUR.durationMs;
            b.append(h).append('h');
            v -= h * HOUR.durationMs;
            millisOnly = false;
        }
        if (v >= MINUTE.durationMs) {
            if (!millisOnly) {
                b.append(':');
            }
            long m = v / MINUTE.durationMs;
            b.append(m).append('m');
            v -= m * MINUTE.durationMs;
            millisOnly = false;
        }
        if (v >= SECOND.durationMs) {
            if (!millisOnly) {
                b.append(':');
            }
            long s = v / SECOND.durationMs;
            b.append(s).append('s');
            v -= s * SECOND.durationMs;
            millisOnly = false;
        }
        if (v > 0L) {
            if (!millisOnly) {
                b.append(':');
                if (v < 100L) {
                    b.append('0');
                    if (v < 10L) {
                        b.append('0');
                    }
                }
            }
            b.append(v);
            b.append("ms");
        }
        return b.toString();
    }

    @Nullable
    public static DurationMs min(@Nullable DurationMs left, @Nullable DurationMs right) {
        if (left == null || right == null) {
            return null;
        }
        return left.isLess(right) ? left : right;
    }

    @Nullable
    public static DurationMs max(@Nullable DurationMs left, @Nullable DurationMs right) {
        if (left == null) {
            return right;
        }
        if (right == null) {
            return left;
        }
        return left.isGreater(right) ? left : right;
    }

    public static boolean isEqual(@Nullable DurationMs left, @Nullable DurationMs right) {
        return left == right || (left != null && right != null && left.durationMs == right.durationMs);
    }

    @Nonnull
    public static DurationMs between(@Nonnull LocalTs fromTs, @Nonnull LocalTs toTs) {
        return new DurationMs(toTs.localTs - fromTs.localTs);
    }

    @Nonnull
    public static DurationMs between(@Nonnull InstantTs fromTs, @Nonnull InstantTs toTs) {
        return new DurationMs(toTs.instantTs - fromTs.instantTs);
    }

    public long lengthMs() {
        return durationMs >= 0L ? durationMs : -durationMs;
    }

    public boolean isZero() {
        return durationMs == 0L;
    }

    public boolean isPositive() {
        return durationMs > 0L;
    }

    public boolean isPositiveOrZero() {
        return durationMs >= 0L;
    }

    public boolean isNegative() {
        return durationMs < 0L;
    }

    public boolean isNegativeOrZero() {
        return durationMs <= 0L;
    }

    @Nonnull
    public DurationMs plus(@Nonnull DurationMs value) {
        return new DurationMs(this.durationMs + value.durationMs);
    }

    @Nonnull
    public DurationMs plusMillis(long value) {
        return new DurationMs(durationMs + value);
    }

    @Nonnull
    public DurationMs minus(@Nonnull DurationMs value) {
        return new DurationMs(durationMs - value.durationMs);
    }

    @Nonnull
    public DurationMs minusMillis(long value) {
        return new DurationMs(durationMs - value);
    }

    @Nonnull
    public DurationMs plusLengthMillis(long value) {
        return durationMs >= 0L ?
            new DurationMs(Math.max(0L, durationMs + value)) :
            new DurationMs(Math.min(0L, durationMs - value));
    }

    @Nonnull
    public DurationMs minusLengthMillis(long value) {
        return durationMs >= 0L ?
            new DurationMs(Math.max(0L, durationMs - value)) :
            new DurationMs(Math.min(0L, durationMs + value));
    }

    @Nonnull
    public DurationMs multiply(long value) {
        return new DurationMs(durationMs * value);
    }

    @Nonnull
    public DurationMs multiply(double value) {
        return new DurationMs((long) (durationMs * value));
    }

    @Nonnull
    public DurationMs divide(long value) {
        if (value == 0L) {
            throw new ArithmeticException("division by zero");
        }
        return new DurationMs(durationMs / value);
    }

    @Nonnull
    public DurationMs divide(double value) {
        if (value == 0.0) {
            throw new ArithmeticException("division by zero");
        }
        return new DurationMs((long) (durationMs / value));
    }

    public double divide(@Nonnull DurationMs value) {
        if (value.isZero()) {
            throw new ArithmeticException("division by zero");
        }
        long iResult = durationMs / value.durationMs;
        long remainder = durationMs - iResult * value.durationMs;
        return (double) iResult + ((double) remainder / (double) value.durationMs);
    }

    @Nonnull
    public DurationMs negate() {
        return new DurationMs(-durationMs);
    }

    @Nonnull
    public DurationMs abs() {
        return durationMs >= 0L ? this : negate();
    }

    @Nonnull
    public Duration toDuration() {
        return Duration.ofMillis(durationMs);
    }

    public boolean isLess(@Nonnull DurationMs value) {
        return durationMs < value.durationMs;
    }

    public boolean isLessOrEqual(@Nonnull DurationMs value) {
        return durationMs <= value.durationMs;
    }

    public boolean isGreater(@Nonnull DurationMs value) {
        return durationMs > value.durationMs;
    }

    public boolean isGreaterOrEqual(@Nonnull DurationMs value) {
        return durationMs >= value.durationMs;
    }

    public boolean isEqual(@Nonnull DurationMs value) {
        return durationMs == value.durationMs;
    }

    @Nonnull
    public String code() {
        if (code == null) {
            this.code = toCode(durationMs);
        }
        return code;
    }

    @Override
    public int compareTo(@Nullable DurationMs rhs) {
        return rhs != null ? Long.compare(durationMs, rhs.durationMs) : 1;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof DurationMs)) return false;
        DurationMs that = (DurationMs) o;
        return durationMs == that.durationMs;
    }

    @Override
    public int hashCode() {
        return (int) (durationMs ^ (durationMs >>> 32));
    }

    @Nonnull
    @Override
    public String toString() {
        return "<D " + durationMs + " | " + code() + ">";
    }

    private DurationMs(long durationMs) {
        this.durationMs = durationMs;
    }
}
