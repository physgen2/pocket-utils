package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.*;
import java.time.temporal.ChronoField;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class DayMillisId implements Comparable<DayMillisId> {

    public static final DayMillisId MIN = ofLocalTime(LocalTime.MIN);
    public static final DayMillisId MAX = ofLocalTime(LocalTime.MAX);
    public static final DayMillisId MIDNIGHT = ofLocalTime(LocalTime.MIDNIGHT);
    public static final DayMillisId NOON = ofLocalTime(LocalTime.NOON);

    private static final long MIN_INC_MILLIS_OF_DAY = ChronoField.MILLI_OF_DAY.range().getMinimum();
    private static final long COUNT_OF_MILLIS_OF_DAY = ChronoField.MILLI_OF_DAY.range().getMaximum() - MIN_INC_MILLIS_OF_DAY + 1L;

    public final long millisOfDay;

    @Nonnull
    public static DayMillisId ofLocalTs(@Nonnull LocalTs localTs) {
        return ofLocalTime(localTs.toLocalDateTime().toLocalTime());
    }

    @Nonnull
    public static DayMillisId ofMillisOfDay(long millisOfDay) {
        return new DayMillisId(millisOfDay);
    }

    @Nonnull
    public static DayMillisId ofLocalTime(@Nonnull LocalTime localTime) {
        return ofMillisOfDay(TimeUnit.NANOSECONDS.toMillis(localTime.toNanoOfDay()));
    }

    @Nonnull
    public static DayMillisId now(@Nonnull Clock zonedClock) {
        return now(zonedClock, zonedClock.getZone());
    }

    @Nonnull
    public static DayMillisId now(@Nonnull Clock utcClock, @Nonnull ZoneId zoneId) {
        return DayMillisId.ofLocalTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(utcClock.millis()), zoneId).toLocalTime());
    }

    @Nonnull
    public DayMillisId next() {
        return plusAmount(1L);
    }

    @Nonnull
    public DayMillisId prev() {
        return minusAmount(1L);
    }

    @Nonnull
    public DayMillisId plusAmount(long signedCount) {
        return new DayMillisId(rotateMillisOfDay(millisOfDay + signedCount));
    }

    @Nonnull
    public DayMillisId minusAmount(long signedCount) {
        return new DayMillisId(rotateMillisOfDay(millisOfDay - signedCount));
    }

    @Nonnull
    public LocalTime toLocalTime() {
        return LocalTime.ofNanoOfDay(TimeUnit.MILLISECONDS.toNanos(millisOfDay));
    }

    public boolean isBefore(@Nonnull DayMillisId dayMillisId) {
        return millisOfDay < dayMillisId.millisOfDay;
    }

    public boolean isBeforeOrEqual(@Nonnull DayMillisId dayMillisId) {
        return millisOfDay <= dayMillisId.millisOfDay;
    }

    public boolean isAfter(@Nonnull DayMillisId dayMillisId) {
        return millisOfDay > dayMillisId.millisOfDay;
    }

    public boolean isAfterOrEqual(@Nonnull DayMillisId dayMillisId) {
        return millisOfDay >= dayMillisId.millisOfDay;
    }

    public boolean isEqual(@Nonnull DayMillisId dayMillisId) {
        return millisOfDay == dayMillisId.millisOfDay;
    }

    public long diffAmount(@Nonnull DayMillisId dayMillisId) {
        return millisOfDay - dayMillisId.millisOfDay;
    }

    @Override
    public int compareTo(@Nullable DayMillisId rhs) {
        return rhs != null ? Long.compare(millisOfDay, rhs.millisOfDay) : 1;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof DayMillisId)) return false;
        DayMillisId that = (DayMillisId) o;
        return millisOfDay == that.millisOfDay;
    }

    @Override
    public int hashCode() {
        return (int) (millisOfDay ^ (millisOfDay >>> 32));
    }

    @Nonnull
    @Override
    public String toString() {
        return "DayMillisId[" + millisOfDay + " | " + toLocalTime() + "]";
    }

    private DayMillisId(long millisOfDay) {
        checkArgument(isValidMillisOfDay(millisOfDay));
        this.millisOfDay = millisOfDay;
    }

    private static boolean isValidMillisOfDay(long millisOfDay) {
        return ChronoField.MILLI_OF_DAY.range().isValidValue(millisOfDay);
    }

    private static long rotateMillisOfDay(long millisOfDayExceeded) {
        return millisOfDayExceeded < MIN_INC_MILLIS_OF_DAY ?
            COUNT_OF_MILLIS_OF_DAY + 2L * MIN_INC_MILLIS_OF_DAY - rotateMillisOfDay(2L * MIN_INC_MILLIS_OF_DAY - millisOfDayExceeded) :
            MIN_INC_MILLIS_OF_DAY + (millisOfDayExceeded - MIN_INC_MILLIS_OF_DAY) % COUNT_OF_MILLIS_OF_DAY;
    }
}
