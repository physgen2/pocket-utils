package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.*;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketutils.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class Space<T> {

    private static final Space<?> UNKNOWN = new Space<>(entireSpace(null));

    public final NavigableMap<TimeSpan, Optional<T>> space;

    @Nullable
    private List<T> existing;//private, mutable, non-volatile, ok

    @Nonnull
    public static <T> Space<T> unknown() {
        @SuppressWarnings("unchecked")
        Space<T> result = (Space<T>) UNKNOWN;
        return result;
    }

    @Nonnull
    public static <T> Space<T> of(@Nonnull Map<TimeSpan, Optional<T>> space) {
        return new Space<>(space);
    }

    @Nonnull
    public static <T> Space<T> of(@Nullable T value) {
        return new Space<>(entireSpace(value));
    }

    @Nonnull
    public List<T> existing() {
        if (existing == null) {
            this.existing = space.values()
                .stream()
                .map(optValue -> optValue.orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toUnmodifiableList());
        }
        return existing;
    }

    @Nonnull
    public Map.Entry<TimeSpan, Optional<T>> activeEntry(@Nullable LocalTs momentTs) {
        return checkNotNull(space.floorEntry(TimeSpan.ofBorders(momentTs, null)));
    }

    @Nonnull
    public TimeSpan activeTimeSpan(@Nullable LocalTs momentTs) {
        return checkNotNull(space.floorKey(TimeSpan.ofBorders(momentTs, null)));
    }

    @Nullable
    public Map.Entry<TimeSpan, Optional<T>> prevEntry(@Nonnull TimeSpan key) {
        return space.headMap(key, false).lastEntry();//not inclusive, ok
    }

    @Nullable
    public TimeSpan prevTimeSpan(@Nonnull TimeSpan key) {
        var entry = prevEntry(key);
        return entry != null ? entry.getKey() : null;
    }

    @Nullable
    public Map.Entry<TimeSpan, Optional<T>> nextEntry(@Nonnull TimeSpan key) {
        return space.tailMap(key, false).firstEntry();//not inclusive, ok
    }

    @Nullable
    public TimeSpan nextTimeSpan(@Nonnull TimeSpan key) {
        var entry = nextEntry(key);
        return entry != null ? entry.getKey() : null;
    }

    @Nullable
    public T find(@Nonnull TimeSpan key) {
        Optional<T> optValue = space.get(key);
        return optValue != null ? optValue.orElse(null) : null;
    }

    @Nullable
    public T find(@Nullable LocalTs momentTs) {
        return activeEntry(momentTs).getValue().orElse(null);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof Space)) return false;
        Space<?> that = (Space<?>) o;
        return space.equals(that.space);
    }

    @Override
    public int hashCode() {
        return space.hashCode();
    }

    @Nonnull
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("[");
        boolean notFirst = false;
        for (Map.Entry<TimeSpan, Optional<T>> entry : space.entrySet()) {
            TimeSpan timeSpan = entry.getKey();
            Optional<T> optValue = entry.getValue();
            if (!notFirst) {
                notFirst = true;
                LocalTs fromInc = timeSpan.fromInc;
                b.append(fromInc != null ? fromInc : "-INF");
            }
            b.append(" -> ");
            b.append(optValue.isPresent() ? optValue.get() : "<EMPTY>");
            b.append(" -> ");
            b.append(timeSpan.toExc != null ? timeSpan.toExc : "+INF");
        }
        return b.append("]").toString();
    }

    protected Space(@Nonnull Map<TimeSpan, Optional<T>> space) {
        TreeMap<TimeSpan, Optional<T>> lSpace = new TreeMap<>(space);//copy with natural ordering
        checkIntegrity(lSpace);
        this.space = Collections.unmodifiableNavigableMap(lSpace);
    }

    protected static <T> void checkIntegrity(@Nonnull NavigableMap<TimeSpan, Optional<T>> space) {
        checkArgument(
            !space.isEmpty(),
            "Integrity violation: at least one entire time span must be present, space: %s",
            space
        );
        LocalTs ts = null;
        boolean noMoreExpected = false;
        for (TimeSpan timeSpan : space.keySet()) {
            checkArgument(
                !noMoreExpected,
                "Integrity violation: no more time spans expected, because previous time span ends with +infinity, space: %s",
                space
            );
            checkArgument(
                !timeSpan.isEmpty(),
                "Integrity violation: time span must be not empty, timeSpan: %s, space: %s",
                timeSpan, space
            );
            checkArgument(
                LocalTs.isEqual(ts, timeSpan.fromInc),
                "Integrity violation: incorrect fromTs of time span, expected fromTs: %s, actual timeSpan: %s, space: %s",
                ts, timeSpan, space
            );
            ts = timeSpan.toExc;
            noMoreExpected = (ts == null);//ends with +infinity
        }
        checkArgument(
            noMoreExpected,
            "Integrity violation: unexpected finish of time spans, expected timeSpan.fromTs: %s, space: %s",
            ts, space
        );
    }

    @Nonnull
    protected static <V> Map<TimeSpan, Optional<V>> entireSpace(@Nullable V value) {
        return Collections.singletonMap(TimeSpan.ENTIRETY, Optional.ofNullable(value));
    }
}
