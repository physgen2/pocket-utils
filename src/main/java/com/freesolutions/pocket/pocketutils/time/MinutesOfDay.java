package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class MinutesOfDay {

    @NotThreadSafe
    public static class Builder {

        @Nonnull
        private final byte[] data = new byte[DATA_SIZE];
        private boolean finished;

        public Builder() {
            //empty
        }

        public Builder(@Nonnull byte[] initial) {
            checkArgument(
                initial.length == DATA_SIZE,
                "length != expected, expected: %s, actual: %s",
                DATA_SIZE, initial.length
            );
            System.arraycopy(initial, 0, data, 0, DATA_SIZE);
        }

        @Nonnull
        public Builder setInterval(int fromMinuteOfDayInc, int toMinuteOfDayExc, boolean setNReset) {
            checkArgument(!finished, "Builder was finished before");
            checkArgument(
                fromMinuteOfDayInc >= 0 && fromMinuteOfDayInc <= MINUTES_IN_DAY,
                "fromMinute >= 0 && fromMinute <= MINUTES_IN_DAY, actual: %s",
                fromMinuteOfDayInc
            );
            checkArgument(
                toMinuteOfDayExc >= 0 && toMinuteOfDayExc <= MINUTES_IN_DAY,
                "toMinute >= 0 && toMinute <= MINUTES_IN_DAY, actual: %s",
                toMinuteOfDayExc
            );
            checkArgument(
                fromMinuteOfDayInc <= toMinuteOfDayExc,
                "fromMinuteOfDayInc <= toMinuteOfDayExc, actual: fromMinuteOfDayInc: %s, toMinuteOfDayExc: %s",
                fromMinuteOfDayInc, toMinuteOfDayExc
            );
            for (int minuteOfDay = fromMinuteOfDayInc; minuteOfDay < toMinuteOfDayExc; minuteOfDay++) {
                int bi = minuteOfDay >>> 3;
                int bm = (byte) (1 << (minuteOfDay - (bi << 3)));
                if (setNReset) {
                    data[bi] |= bm;
                } else {
                    data[bi] &= ~bm;
                }
            }
            return this;
        }

        @Nonnull
        public Builder setAll() {
            return setInterval(0, MINUTES_IN_DAY, true);
        }

        @Nonnull
        public MinutesOfDay build() {
            checkArgument(!finished, "Builder was finished before");
            this.finished = true;
            return new MinutesOfDay(data);
        }
    }

    public static final int MINUTES_IN_DAY = (int) TimeUnit.DAYS.toMinutes(1L);
    public static final int DATA_SIZE = MINUTES_IN_DAY / Byte.SIZE;

    private static final int MILLIS_IN_MINUTE = (int) TimeUnit.MINUTES.toMillis(1L);
    private static final int MINUTES_IN_HOUR = (int) TimeUnit.HOURS.toMinutes(1L);

    private static final MinutesOfDay EMPTY = builder().build();
    private static final MinutesOfDay ALL = builder().setAll().build();

    @Nonnull
    private final byte[] data;//immutable
    private int filledCount = -1;//private, not final, not volatile, ok
    @Nullable
    private String cachedToString;//private, not final, not volatile, ok

    @Nonnull
    public static MinutesOfDay empty() {
        return EMPTY;
    }

    @Nonnull
    public static MinutesOfDay all() {
        return ALL;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public static Builder builder(@Nonnull byte[] initial) {
        return new Builder(initial);
    }

    public boolean contains(int minuteOfDay) {
        int bi = minuteOfDay >>> 3;
        return (data[bi] & (1 << ((~(minuteOfDay ^ (bi << 3))) & 0x7))) != 0;
    }

    public boolean contains(@Nonnull LocalTs minuteInsideTs) {
        long epochMinute = Math.floorDiv(minuteInsideTs.localTs, MILLIS_IN_MINUTE);
        int minuteOfDay = Math.floorMod(epochMinute, MINUTES_IN_DAY);
        return contains(minuteOfDay);
    }

    @Nonnull
    public Iterator<Integer> flipIterator(boolean inNOut) {
        return new Iterator<>() {
            private int i = 0;
            private boolean s = !inNOut;

            {
                nextFlip();
            }

            @Override
            public boolean hasNext() {
                return i < MINUTES_IN_DAY;
            }

            @Override
            public Integer next() {
                int r = i;
                if (r >= MINUTES_IN_DAY) {
                    throw new NoSuchElementException();
                }
                nextFlip();
                return r;
            }

            private void nextFlip() {
                while (i < MINUTES_IN_DAY && (contains(i) ^ !s)) {
                    i++;
                }
                this.s = !s;
            }
        };
    }

    public void retrieveData(@Nonnull byte[] out, int outOffset) {
        checkArgument(out.length >= outOffset + DATA_SIZE);
        System.arraycopy(data, 0, out, outOffset, DATA_SIZE);
    }

    public byte[] retrieveData() {
        byte[] result = new byte[DATA_SIZE];
        retrieveData(result, 0);
        return result;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof MinutesOfDay)) return false;
        MinutesOfDay that = (MinutesOfDay) o;
        return Arrays.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }

    @Nonnull
    @Override
    public String toString() {
        if (cachedToString != null) {
            return cachedToString;
        }
        int fc = getFilledCount();
        if (fc == 0) {
            this.cachedToString = "MinutesOfDay[empty]";
            return cachedToString;
        }
        if (fc == MINUTES_IN_DAY) {
            this.cachedToString = "MinutesOfDay[all]";
            return cachedToString;
        }
        StringBuilder b = new StringBuilder();
        b.append("MinutesOfDay[").append(fc).append("/").append(MINUTES_IN_DAY).append(": ");
        boolean first = true;
        Iterator<Integer> i = flipIterator(true);
        while (i.hasNext()) {
            int fromInc = i.next();
            int toExc = i.hasNext() ? i.next() : MINUTES_IN_DAY;
            if (!first) {
                b.append(", ");
            } else {
                first = false;
            }
            b.append(fromInc / MINUTES_IN_HOUR).append(':').append(toStringMinuteOfHour(fromInc % MINUTES_IN_HOUR));
            b.append(" -> ");
            b.append(toExc / MINUTES_IN_HOUR).append(':').append(toStringMinuteOfHour(toExc % MINUTES_IN_HOUR));
        }
        b.append("]");
        this.cachedToString = b.toString();
        return cachedToString;
    }

    private MinutesOfDay(@Nonnull byte[] data) {
        this.data = data;
    }

    private int getFilledCount() {
        if (filledCount < 0) {
            int c = 0;
            for (int b : data) {
                b = b & 0xff;
                while (b != 0) {
                    c += b & 1;
                    b >>>= 1;
                }
            }
            this.filledCount = c;
        }
        return filledCount;
    }

    @Nonnull
    private static String toStringMinuteOfHour(int minuteOfHour) {
        if (minuteOfHour == 0) {
            return "00";
        }
        if (minuteOfHour < 10) {
            return "0" + minuteOfHour;
        }
        return Integer.toString(minuteOfHour);
    }
}
