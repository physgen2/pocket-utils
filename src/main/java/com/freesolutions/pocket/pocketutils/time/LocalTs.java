package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class LocalTs implements Comparable<LocalTs> {

    public static final LocalTs EPOCH = ofTs(0L);
    public static final LocalTs MIN = ofTs(Long.MIN_VALUE);
    public static final LocalTs MAX = ofTs(Long.MAX_VALUE);

    //package-private
    static final long MILLIS_PER_DAY = TimeUnit.DAYS.toMillis(1L);
    static final long MILLIS_PER_SECOND = TimeUnit.SECONDS.toMillis(1L);
    static final long NANOS_PER_MILLI = TimeUnit.MILLISECONDS.toNanos(1L);

    //NOTE: this is not instant-like millis, this is short form of <LocalDateTime>,
    //represented as amount of millis after start of epoch in some local timezone.
    //NOTE: negative values are correct, corresponding time moments before start of epoch.
    public final long localTs;

    @Nonnull
    public static LocalTs ofTs(long localTs) {
        return new LocalTs(localTs);
    }

    @Nonnull
    public static LocalTs ofLocalDateTime(@Nonnull LocalDateTime localDateTime) {
        return new LocalTs(
            localDateTime.toLocalDate().toEpochDay() * MILLIS_PER_DAY +
                localDateTime.toLocalTime().toNanoOfDay() / NANOS_PER_MILLI //positive, avoid floorDiv()
        );
    }

    @Nonnull
    public static LocalTs ofInstantTs(@Nonnull InstantTs instantTs, @Nonnull ZoneId zoneId) {
        return ofTs(instantTs.instantTs + zoneOffsetMs(instantTs.toInstant(), zoneId));
    }

    @Nonnull
    public static LocalTs ofInstant(@Nonnull Instant instant, @Nonnull ZoneId zoneId) {
        return ofTs(instant.toEpochMilli() + zoneOffsetMs(instant, zoneId));
    }

    @Nonnull
    public static LocalTs nowUTC() {
        return ofTs(System.currentTimeMillis()); //fastest implementation
    }

    @Nonnull
    public static LocalTs now(@Nonnull Clock zonedClock) {
        return ofInstant(zonedClock.instant(), zonedClock.getZone());
    }

    @Nonnull
    public static LocalTs now(@Nonnull Clock utcClock, @Nonnull ZoneId zoneId) {
        return ofInstant(utcClock.instant(), zoneId);
    }

    @Nullable
    public static LocalTs min(@Nullable LocalTs left, @Nullable LocalTs right) {
        if (left == null || right == null) {
            return null;
        }
        return left.isBefore(right) ? left : right;
    }

    @Nullable
    public static LocalTs max(@Nullable LocalTs left, @Nullable LocalTs right) {
        if (left == null) {
            return right;
        }
        if (right == null) {
            return left;
        }
        return left.isAfter(right) ? left : right;
    }

    public static boolean isEqual(@Nullable LocalTs left, @Nullable LocalTs right) {
        return left == right || (left != null && right != null && left.localTs == right.localTs);
    }

    @Nonnull
    public LocalTs next() {
        return plusMillis(1L);
    }

    @Nonnull
    public LocalTs prev() {
        return minusMillis(1L);
    }

    @Nonnull
    public LocalTs plusMillis(long signedCount) {
        return new LocalTs(localTs + signedCount);
    }

    @Nonnull
    public LocalTs minusMillis(long signedCount) {
        return new LocalTs(localTs - signedCount);
    }

    @Nonnull
    public LocalTs atStartOfDay() {
        return new LocalTs(toEpochDay() * MILLIS_PER_DAY);
    }

    @Nonnull
    public LocalDateTime toLocalDateTime() {
        return LocalDateTime.of(toLocalDate(), toLocalTime());
    }

    @Nonnull
    public LocalDate toLocalDate() {
        return LocalDate.ofEpochDay(toEpochDay());
    }

    @Nonnull
    public LocalTime toLocalTime() {
        return LocalTime.ofNanoOfDay(toNanoOfDay());
    }

    public long toEpochDay() {
        return Math.floorDiv(localTs, MILLIS_PER_DAY);
    }

    public long toMilliOfDay() {
        return Math.floorMod(localTs, MILLIS_PER_DAY);
    }

    public long toNanoOfDay() {
        return toMilliOfDay() * NANOS_PER_MILLI;
    }

    @Nonnull
    public InstantTs toInstantTs(@Nonnull ZoneId zoneId) {
        return InstantTs.ofLocalTs(this, zoneId);
    }

    @Nonnull
    public Instant toInstant(@Nonnull ZoneId zoneId) {
        return Instant.ofEpochMilli(epochMilli(toLocalDateTime(), zoneId));
    }

    public boolean isBefore(@Nonnull LocalTs localTs) {
        return this.localTs < localTs.localTs;
    }

    public boolean isBeforeOrEqual(@Nonnull LocalTs localTs) {
        return this.localTs <= localTs.localTs;
    }

    public boolean isAfter(@Nonnull LocalTs localTs) {
        return this.localTs > localTs.localTs;
    }

    public boolean isAfterOrEqual(@Nonnull LocalTs localTs) {
        return this.localTs >= localTs.localTs;
    }

    public boolean isEqual(@Nonnull LocalTs localTs) {
        return this.localTs == localTs.localTs;
    }

    public long diffAmount(@Nonnull LocalTs localTs) {
        return this.localTs - localTs.localTs;
    }

    @Nonnull
    public LocalTs switchZone(@Nullable ZoneId sourceZoneId, @Nullable ZoneId targetZoneId) {
        return sourceZoneId != null && targetZoneId != null && !sourceZoneId.equals(targetZoneId) ?
            ofInstant(toInstant(sourceZoneId), targetZoneId) :
            this;
    }

    @Override
    public int compareTo(@Nullable LocalTs rhs) {
        return rhs != null ? Long.compare(localTs, rhs.localTs) : 1;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof LocalTs)) return false;
        LocalTs that = (LocalTs) o;
        return localTs == that.localTs;
    }

    @Override
    public int hashCode() {
        return (int) (localTs ^ (localTs >>> 32));
    }

    @Nonnull
    @Override
    public String toString() {
        return "<L " + localTs + " | " + toLocalDateTime() + ">";
    }

    private LocalTs(long localTs) {
        this.localTs = localTs;
    }

    //package-private
    static long zoneOffsetMs(@Nonnull Instant instant, @Nonnull ZoneId zoneId) {
        //NOTE: can't avoid usage of Instant here
        return zoneId.getRules().getOffset(instant).getTotalSeconds() * MILLIS_PER_SECOND;
    }

    //package-private
    static long epochMilli(@Nonnull LocalDateTime localDateTime, @Nonnull ZoneId zoneId) {
        //NOTE: can't avoid usage of LocalDateTime & ZonedDateTime here
        return ZonedDateTime.of(localDateTime, zoneId).toEpochSecond() * MILLIS_PER_SECOND;
    }
}
