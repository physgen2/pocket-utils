package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.*;

import static com.freesolutions.pocket.pocketutils.time.LocalTs.*;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class InstantTs implements Comparable<InstantTs> {

    public static final InstantTs EPOCH = InstantTs.ofTs(0L);
    public static final InstantTs MIN = InstantTs.ofTs(Long.MIN_VALUE);
    public static final InstantTs MAX = InstantTs.ofTs(Long.MAX_VALUE);

    //NOTE: this is short form of <Instant>, represented as amount of millis after start of epoch.
    //NOTE: negative values are correct, corresponding time moments before start of epoch.
    public final long instantTs;

    @Nonnull
    public static InstantTs ofTs(long instantTs) {
        return new InstantTs(instantTs);
    }

    @Nonnull
    public static InstantTs ofInstant(@Nonnull Instant instant) {
        return ofTs(instant.toEpochMilli());
    }

    @Nonnull
    public static InstantTs ofLocalTs(@Nonnull LocalTs localTs, @Nonnull ZoneId zoneId) {
        return ofTs(epochMilli(localTs.toLocalDateTime(), zoneId));
    }

    @Nonnull
    public static InstantTs ofLocalDateTime(@Nonnull LocalDateTime localDateTime, @Nonnull ZoneId zoneId) {
        return ofTs(epochMilli(localDateTime, zoneId));
    }

    @Nonnull
    public static InstantTs nowUTC() {
        return ofTs(System.currentTimeMillis()); //fastest implementation
    }

    @Nonnull
    public static InstantTs now(@Nonnull Clock clock) {
        return ofTs(clock.millis());
    }

    @Nullable
    public static InstantTs min(@Nullable InstantTs left, @Nullable InstantTs right) {
        if (left == null || right == null) {
            return null;
        }
        return left.isBefore(right) ? left : right;
    }

    @Nullable
    public static InstantTs max(@Nullable InstantTs left, @Nullable InstantTs right) {
        if (left == null) {
            return right;
        }
        if (right == null) {
            return left;
        }
        return left.isAfter(right) ? left : right;
    }

    public static boolean isEqual(@Nullable InstantTs left, @Nullable InstantTs right) {
        return left == right || (left != null && right != null && left.instantTs == right.instantTs);
    }

    @Nonnull
    public InstantTs next() {
        return plusMillis(1L);
    }

    @Nonnull
    public InstantTs prev() {
        return minusMillis(1L);
    }

    @Nonnull
    public InstantTs plusMillis(long signedCount) {
        return new InstantTs(instantTs + signedCount);
    }

    @Nonnull
    public InstantTs minusMillis(long signedCount) {
        return new InstantTs(instantTs - signedCount);
    }

    @Nonnull
    public Instant toInstant() {
        return Instant.ofEpochMilli(instantTs);
    }

    @Nonnull
    public LocalTs toLocalTs(@Nonnull ZoneId zoneId) {
        return LocalTs.ofInstantTs(this, zoneId);
    }

    @Nonnull
    public LocalDateTime toLocalDateTime(@Nonnull ZoneId zoneId) {
        long localTs = instantTs + zoneOffsetMs(toInstant(), zoneId);
        long epochDay = Math.floorDiv(localTs, MILLIS_PER_DAY);
        long milliOfDay = Math.floorMod(localTs, MILLIS_PER_DAY);
        return LocalDateTime.of(
            LocalDate.ofEpochDay(epochDay),
            LocalTime.ofNanoOfDay(milliOfDay * NANOS_PER_MILLI)
        );
    }

    @Nonnull
    public LocalDate toLocalDate(@Nonnull ZoneId zoneId) {
        long localTs = instantTs + zoneOffsetMs(toInstant(), zoneId);
        long epochDay = Math.floorDiv(localTs, MILLIS_PER_DAY);
        return LocalDate.ofEpochDay(epochDay);
    }

    @Nonnull
    public LocalTime toLocalTime(@Nonnull ZoneId zoneId) {
        long localTs = instantTs + zoneOffsetMs(toInstant(), zoneId);
        long milliOfDay = Math.floorMod(localTs, MILLIS_PER_DAY);
        return LocalTime.ofNanoOfDay(milliOfDay * NANOS_PER_MILLI);
    }

    public boolean isBefore(@Nonnull InstantTs instantTs) {
        return this.instantTs < instantTs.instantTs;
    }

    public boolean isBeforeOrEqual(@Nonnull InstantTs instantTs) {
        return this.instantTs <= instantTs.instantTs;
    }

    public boolean isAfter(@Nonnull InstantTs instantTs) {
        return this.instantTs > instantTs.instantTs;
    }

    public boolean isAfterOrEqual(@Nonnull InstantTs instantTs) {
        return this.instantTs >= instantTs.instantTs;
    }

    public boolean isEqual(@Nonnull InstantTs instantTs) {
        return this.instantTs == instantTs.instantTs;
    }

    public long diffAmount(@Nonnull InstantTs instantTs) {
        return this.instantTs - instantTs.instantTs;
    }

    @Override
    public int compareTo(@Nullable InstantTs rhs) {
        return rhs != null ? Long.compare(instantTs, rhs.instantTs) : 1;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof InstantTs)) return false;
        InstantTs that = (InstantTs) o;
        return instantTs == that.instantTs;
    }

    @Override
    public int hashCode() {
        return (int) (instantTs ^ (instantTs >>> 32));
    }

    @Nonnull
    @Override
    public String toString() {
        return "<I " + instantTs + " | " + toInstant() + ">";
    }

    private InstantTs(long instantTs) {
        this.instantTs = instantTs;
    }
}
