package com.freesolutions.pocket.pocketutils.time;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class TimeSpan implements Comparable<TimeSpan> {

    public static final TimeSpan ENTIRETY = new TimeSpan(null, null);

    @Nullable
    public final LocalTs fromInc;
    @Nullable
    public final LocalTs toExc;

    @Nullable
    public static TimeSpan ofBordersOrNull(@Nullable LocalTs fromInc, @Nullable LocalTs toExc) {
        return (fromInc == null || toExc == null || fromInc.localTs <= toExc.localTs) ?
            new TimeSpan(fromInc, toExc) :
            null;
    }

    @Nonnull
    public static TimeSpan ofBorders(@Nullable LocalTs fromInc, @Nullable LocalTs toExc) {
        checkArgument(fromInc == null || toExc == null || fromInc.localTs <= toExc.localTs);
        return new TimeSpan(fromInc, toExc);
    }

    public boolean isEmpty() {
        return fromInc != null && toExc != null && fromInc.isEqual(toExc);
    }

    public boolean isEntirety() {
        return fromInc == null && toExc == null;
    }

    public boolean isInfinite() {
        return fromInc == null || toExc == null;
    }

    public boolean isInfiniteLeft() {
        return fromInc == null;
    }

    public boolean isInfiniteRight() {
        return toExc == null;
    }

    public boolean isBelong(@Nonnull LocalTs localTs) {
        return (fromInc == null || localTs.isAfterOrEqual(fromInc)) && (toExc == null || localTs.isBefore(toExc));
    }

    public boolean isIntersect(@Nonnull TimeSpan timeSpan) {
        return (fromInc == null || timeSpan.toExc == null || fromInc.isBefore(timeSpan.toExc)) &&
            (timeSpan.fromInc == null || toExc == null || timeSpan.fromInc.isBefore(toExc));
    }

    public boolean isBefore(@Nonnull TimeSpan timeSpan) {
        return compareTo(timeSpan) < 0;
    }

    public boolean isBeforeOrEqual(@Nonnull TimeSpan timeSpan) {
        return compareTo(timeSpan) <= 0;
    }

    public boolean isAfter(@Nonnull TimeSpan timeSpan) {
        return compareTo(timeSpan) > 0;
    }

    public boolean isAfterOrEqual(@Nonnull TimeSpan timeSpan) {
        return compareTo(timeSpan) >= 0;
    }

    public boolean isEqual(@Nonnull TimeSpan timeSpan) {
        return compareTo(timeSpan) == 0;
    }

    @Nullable
    public DurationMs duration() {
        return (fromInc != null && toExc != null) ?
            DurationMs.between(fromInc, toExc) :
            null;
    }

    @Nullable
    public TimeSpan intersect(@Nonnull TimeSpan timeSpan) {
        return ofBordersOrNull(
            LocalTs.max(fromInc, timeSpan.fromInc),
            LocalTs.min(toExc, timeSpan.toExc)
        );
    }

    @Override
    public int compareTo(@Nullable TimeSpan rhs) {
        if (this == rhs) {
            return 0;
        }
        if (rhs == null) {
            return 1;
        }
        int r;
        if (fromInc == null && rhs.fromInc == null) {
            r = 0;
        } else if (fromInc == null) {
            r = -1;
        } else if (rhs.fromInc == null) {
            r = 1;
        } else {
            r = fromInc.compareTo(rhs.fromInc);
        }
        if (r == 0) {
            if (toExc == null && rhs.toExc == null) {
                r = 0;
            } else if (toExc == null) {
                r = 1;
            } else if (rhs.toExc == null) {
                r = -1;
            } else {
                r = toExc.compareTo(rhs.toExc);
            }
        }
        return r;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof TimeSpan)) return false;
        TimeSpan timeSpan = (TimeSpan) o;
        if (fromInc != null ? !fromInc.equals(timeSpan.fromInc) : timeSpan.fromInc != null) return false;
        return toExc != null ? toExc.equals(timeSpan.toExc) : timeSpan.toExc == null;
    }

    @Override
    public int hashCode() {
        int result = fromInc != null ? fromInc.hashCode() : 0;
        result = 31 * result + (toExc != null ? toExc.hashCode() : 0);
        return result;
    }

    @Nonnull
    @Override
    public String toString() {
        return "/" + (fromInc != null ? fromInc : "-INF") + " .. " + (toExc != null ? toExc : "+INF") + "/";
    }

    private TimeSpan(@Nullable LocalTs fromInc, @Nullable LocalTs toExc) {
        //no checks, see builder methods
        this.fromInc = fromInc;
        this.toExc = toExc;
    }
}
