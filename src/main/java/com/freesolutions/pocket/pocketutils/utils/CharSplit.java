package com.freesolutions.pocket.pocketutils.utils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.freesolutions.pocket.pocketutils.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@NotThreadSafe
public class CharSplit {

    @Nonnull
    public static Stream<String> splitS(@Nonnull String v, char ch) {
        return splitS(v, ch, false);
    }

    @Nonnull
    public static List<String> splitL(@Nonnull String v, char ch) {
        return splitL(v, ch, false);
    }

    @Nonnull
    public static String[] split(@Nonnull String v, char ch) {
        return split(v, ch, false);
    }

    @Nonnull
    public static Stream<String> splitSkipEmptyS(@Nonnull String v, char ch) {
        return splitS(v, ch, true);
    }

    @Nonnull
    public static List<String> splitSkipEmptyL(@Nonnull String v, char ch) {
        return splitL(v, ch, true);
    }

    @Nonnull
    public static String[] splitSkipEmpty(@Nonnull String v, char ch) {
        return split(v, ch, true);
    }

    @Nonnull
    public static Stream<String> splitS(@Nonnull String v, char ch, boolean skipEmpty) {
        return Arrays.stream(split(v, ch, skipEmpty));
    }

    @Nonnull
    public static List<String> splitL(@Nonnull String v, char ch, boolean skipEmpty) {
        return Arrays.asList(split(v, ch, skipEmpty));
    }

    @Nonnull
    public static String[] split(@Nonnull String v, char ch, boolean skipEmpty) {

        int l = v.length();
        if (l == 0) {
            return skipEmpty ?
                new String[0] :
                new String[]{v};
        }

        //calculate amount of parts
        int c = 0;
        int s = 0;
        for (int i = 0; i < l; i++) {
            if (v.charAt(i) == ch) {
                if (i > s || !skipEmpty) {
                    c++;
                }
                s = i + 1;
            }
        }
        if (c == 0) {
            return (l > s || !skipEmpty) ?
                new String[]{s == 0 ? v : v.substring(s)} :
                new String[0];
        }
        if (l > s || !skipEmpty) {
            c++;
        }

        //do divide
        String[] result = new String[c];
        c = 0;
        s = 0;
        for (int i = s; i < l; i++) {
            if (v.charAt(i) == ch) {
                if (i > s || !skipEmpty) {
                    result[c++] = v.substring(s, i);
                }
                s = i + 1;
            }
        }
        if (l > s || !skipEmpty) {
            result[c++] = v.substring(s);//last
        }
        checkState(c == result.length);

        return result;
    }
}
