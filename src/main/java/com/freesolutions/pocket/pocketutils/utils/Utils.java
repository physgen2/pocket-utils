package com.freesolutions.pocket.pocketutils.utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Stanislau Mirzayeu
 */
public class Utils {

    @Nullable
    public static <V, R> R nn(@Nullable V v, Function<V, R> f) {
        return v != null ? f.apply(v) : null;
    }

    @Nonnull
    public static <V> V or(@Nullable V v, Supplier<V> d) {
        return v != null ? v : d.get();
    }

    @Nonnull
    public static <T> T checkNotNull(@Nullable T value) {
        if (value == null) {
            throw new NullPointerException();
        }
        return value;
    }

    @Nonnull
    public static <T> T checkNotNull(@Nullable T value, @Nonnull String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
        return value;
    }

    @Nonnull
    public static <T> T checkNotNull(@Nullable T value, @Nonnull String message, @Nonnull Object... args) {
        if (value == null) {
            throw new NullPointerException(String.format(message, args));
        }
        return value;
    }

    public static <T> void checkExpectNull(@Nullable T value) {
        if (value != null) {
            throw new IllegalStateException("!null");
        }
    }

    public static <T> void checkExpectNull(@Nullable T value, @Nonnull String message) {
        if (value != null) {
            throw new IllegalStateException(message);
        }
    }

    public static <T> void checkExpectNull(@Nullable T value, @Nonnull String message, @Nonnull Object... args) {
        if (value != null) {
            throw new IllegalStateException(String.format(message, args));
        }
    }

    public static void checkArgument(boolean condition) {
        if (!condition) {
            throw new IllegalArgumentException();
        }
    }

    public static void checkArgument(boolean condition, @Nonnull String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkArgument(boolean condition, @Nonnull String message, @Nonnull Object... args) {
        if (!condition) {
            throw new IllegalArgumentException(String.format(message, args));
        }
    }

    public static void checkState(boolean state) {
        if (!state) {
            throw new IllegalStateException();
        }
    }

    public static void checkState(boolean state, @Nonnull String message) {
        if (!state) {
            throw new IllegalStateException(message);
        }
    }

    public static void checkState(boolean state, @Nonnull String message, @Nonnull Object... args) {
        if (!state) {
            throw new IllegalStateException(String.format(message, args));
        }
    }

    public static double checkFinite(double value) {
        if (!Double.isFinite(value)) {
            throw new IllegalArgumentException();
        }
        return value;
    }

    public static double checkFinite(double value, @Nonnull String message) {
        if (!Double.isFinite(value)) {
            throw new IllegalArgumentException(message);
        }
        return value;
    }

    public static double checkFinite(double value, @Nonnull String message, @Nonnull Object... args) {
        if (!Double.isFinite(value)) {
            throw new IllegalArgumentException(String.format(message, args));
        }
        return value;
    }

    @Nonnull
    public static RuntimeException propagate(@Nonnull Throwable t) {
        if (t instanceof RuntimeException) {
            throw (RuntimeException) t;
        }
        throw new RuntimeException(t);
    }

    @Nonnull
    public static RuntimeException interruptionAwarePropagate(@Nonnull Throwable t) {
        if (t instanceof InterruptedException) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("interrupted", t);
        }
        throw propagate(t);
    }
}
